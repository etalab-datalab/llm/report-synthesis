import pandas as pd


import platform

system_info = platform.system()
if system_info != "Darwin":
    print("Le système d'exploitation est linux.")
    # Ajoutez ici le code que vous souhaitez exécuter si le système est macOS
    from vllm import LLM, SamplingParams
    sampling_params = SamplingParams(temperature=0.2, top_p=0.95, max_tokens=1000)



prompt1 = """<|im_start|>system \n Tu es un agent de l'administration qui sait résumer des textes. Tu dois articuler le résumé autour du titre du texte<|im_end|> \n
<|im_start|>user \n Le texte que tu dois résumé est un chapitre d'un texte plus long. Le titre du chapitre est : """

prompt2 = """. Fais un court résumé du texte qui porte sur le titre. Soit concis et synthétique. Le résumé doit faire entre 4 et 6 phrases.  Voici le texte à résumer : \n"""  

prompt3 = """<|im_end|> \n <|im_start|>assistant"""



def prompt_generator(titres, textes, prompt1, prompt2, prompt3):
    prompt = []
    for i in range(len(titres)):
        prompt.append(prompt1+titres[i]+prompt2+textes[i]+prompt3)
    return prompt


def generate_summary(prompt, llm, sampling_params):

    summaries = []
    outputs = llm.generate(prompt,sampling_params)

    for output in outputs:
        generated_text = output.outputs[0].text
        summaries.append(generated_text)
    return summaries

 
def one_level_summary(df, level, llm, sampling_params, prompt1 =prompt1, prompt2 = prompt2, prompt3 = prompt3, hf=False, max_length = 300):

    df_copie = df.copy()

    df_id = df['niveau'] == level
    
    textes_niveau = df[df_id]['texte_associé'].tolist()
    titres_niveau = df[df_id]['titre'].tolist()

    prompt = prompt_generator(titres_niveau, textes_niveau, prompt1, prompt2, prompt3)
    hf_prompt = titres_niveau + ['/n' for i in range(len(titres_niveau))] + textes_niveau
    if hf==False:
        summaries = generate_summary(prompt, llm, sampling_params)

    else : 
        summaries = generate_HF_summary(hf_prompt, llm, max_length=max_length)
    df_copie.loc[df_id,'texte_associé'] = summaries

    return df_copie

def long_chapter_summary(df, level, llm, sampling_params ,summ_size=2000, prompt1 =prompt1, prompt2 = prompt2, prompt3 = prompt3, hf=False, max_length = 300):

    df_copie = df.copy()
    df_filtre = (df_copie['texte_associé'].str.len() > summ_size) & (df_copie['niveau'] == level)
    
    textes_niveau = df_copie[df_filtre]['texte_associé'].tolist()
    titres_niveau = df_copie[df_filtre]['titre'].tolist()

    prompt = prompt_generator(titres_niveau, textes_niveau, prompt1, prompt2, prompt3)
    hf_prompt = titres_niveau + ['/n' for i in range(len(titres_niveau))] + textes_niveau
    if hf==False:
        summaries = generate_summary(prompt, llm, sampling_params)

    else : 
        summaries = generate_HF_summary(hf_prompt, llm, max_length=max_length)
    df_copie.loc[df_filtre,'texte_associé'] = summaries

    return df_copie

def iterative_summary_reduction(df, level, llm, sampling_params ,summ_size=2000, max_iteration = 5):
    i=0
    df_copie = df.copy()
    while any(len(text) > summ_size for text in df_copie['texte_associé']) and i <=max_iteration:
        i+=1

        df_long = long_chapter_summary(df_copie, level, llm, sampling_params = sampling_params,summ_size=summ_size)
        df_copie = df_long

    return df_copie




def generate_HF_summary(prompt, summarizer, max_length): 
    summaries = []
    sum = summarizer(prompt, max_length=max_length, min_length=30, do_sample=False)
    summaries = [dictionnaire['summary_text'] for dictionnaire in sum]
    print(summaries)
    return summaries




