# MAI 2020

LES INTERNALISATEURS SYSTEMATIQUES: LEUR ROLE DANS LA STRUCTURE DU MARCHE ACTIONS ET DANS LA FORMATION DU PRIX

# IRIS LUCAS

__amf\-france\.org__



Cette étude a été coordonnée par la Division Surveillance des marchés\. Elle repose sur l’utilisation de sources considérées comme fiables mais dont l’exhaustivité et l’exactitude ne peuvent être garanties\.

Toute copie, diffusion et reproduction de cette étude, en totalité ou partie, sont soumises à l’accord exprès, préalable et écrit de l’AMF\.

__SYNTHESE__

Alors que le nouveau cadre règlementaire des marchés d’Instruments financiers \(MIF2\) entre dans sa troisième année d’application, la Commission européenne a entrepris de mener à bien sa réforme technique dans le cadre de son programme Refit\. Dans son rapport rédigé dans cette perspective, l’ESMA envisage de ne plus inclure les internalisateurs systématiques \(SI\) dans l’obligation dite de négociation des actions\. Avec l’objectif de permettre à un plus grand volume d’ordres de contribuer au processus de formation des prix, MIF2 impose en effet l’obligation de négocier les actions sur des marchés réglementés \(MR\), des systèmes multilatéraux de négociation \(SMN\) ou des SI, entreprises d’investissement exécutant les ordres de leurs clients en dehors des plateformes

« traditionnelles » face à leur compte propre\. L’Autorité des marchés financiers \(AMF\) a donc souhaité étudier le rôle des SI dans le marché des actions françaises sous trois axes : leur poids dans la structure actuelle du marché, leur apport en termes de transparence pré\-négociation et de formation des prix et, enfin, la qualité de leurs prix par rapport à ceux d’Euronext Paris\.

### POIDS DES SI DANS LE MARCHE

Dans les premiers mois ayant suivi l’entrée en vigueur de MIF2 \(3 janvier 2018\), la part de marché des SI atteignait alors plus de 30% des volumes échangés, soulevant de nombreuses interrogations autour du rôle de ces acteurs dans la structure du marché, et donc dans la formation des prix \(cf\. graphique 1\)\. Deux ans plus tard, après clarifications du *reporting *de transactions venues confirmer les suspicions de surestimation du poids de ces acteurs, les SI affichent désormais une part de marché évaluée, selon les mois, entre 15% et 20% des montants totaux échangés sur actions, un pourcentage restant malgré tout plus élevé qu’attendu\.

Pour mieux comprendre le rôle des SI dans la structure de marché, il convient alors de différencier la part de leur activité participant au processus de formation des prix sur le marché \(dite « price forming »\) de celle relevant de transactions techniques\. Le format du *reporting *réglementaire prévoit à cet effet, au moment de la déclaration des transactions, une mention particulière\. En considérant cette distinction et en excluant les transactions intragroupes des SI, il ressort de l’étude que la part des montants issus des transactions de SI participant au processus de formation des prix et accessibles aux clients ne représente pas plus de 8% à 10% des volumes globaux échangés sur les actions françaises au premier trimestre 2020 \(cf\. graphique 4\)\. C’est sur ce périmètre plus représentatif pour le marché que se fonde la suite de cette étude\.

### PANORAMA DES ACTEURS

Au cours du premier trimestre 2020, 36 internalisateurs systématiques actifs ont été observés sur le marché des actions françaises via le *reporting *des transactions, dont 28 SI de banques d’investissement \(appelés par la suite Bank SI\) et 8 SI de traders à haute fréquence apporteurs de liquidité \(Electronic Liquidity Provider SI ou ELP SI\)\. Les premiers représentent 76% des volumes et les seconds 24%\. En revanche, parmi les 65% des volumes sur SI exécutés durant la phase continue de trading d’Euronext \(i\.e\. avant le fixing de clôture\), la part des ELP SI monte à 37% \(cf\. graphique 5\)\.

L’analyse de la répartition des montants échangés par les deux catégories de SI montre que les ELP SI concentrent les opérations de petite taille \(pour la plupart inférieures à 50\.000 euros\), tandis que les Bank SI sont préférés pour des transactions de plus grande taille, supérieures à 200\.000 euros \(cf\. graphique 7\)\.

### APPORT EN TERMES DE TRANSPARENCE PRE\-NEGOCIATION

MIF2 impose aux internalisateurs systématiques de publier des prix à l’achat et à la vente pour une taille au moins égale à 10% de ce qu’on appelle la taille standard de marché \(Standard Market Size, SMS\) pour les actions liquides et instruments liquides assimilés \(comme les ETF et les certificats par exemple\)\. La SMS s’élève à 10\.000 euros pour la quasi\-totalité des 150 valeurs françaises entrant dans le périmètre de l’étude mais les SI sont libres de proposer à leurs clients, en complément de leurs cotations publiques, des cotations bilatérales non soumises à la transparence pré\-négociation dès lors qu’elles sont supérieures à la SMS\.

Sur le mois de septembre 2019, en moyenne, la plupart des internalisateurs systématiques ont proposé une cotation pour une taille représentant entre 15% et 40% de la taille standard de marché, soit entre 1\.500 et 4\.000 euros, ce qui peut paraître dérisoire en comparaison de la liquidité potentiellement accessible au meilleur prix sur Euronext à cette période, en moyenne 40\.000 euros\.

Toutefois, cette liquidité offerte publiquement ne reflète pas la réalité des montants échangés : une part importante des transactions des SI échappe aux exigences de la transparence pré\-négociation\. En croisant les données des cotations publiques et du *reporting *des transactions, il ressort que les transactions faisant l’objet de transparence pré\-négociation ne représentent que 22% des montants échangés sur SI durant la phase continue des échanges, soit seulement 1,4% des montants totaux échangés sur le marché durant cette période\. L’apport des SI en matière de transparence est donc très limité\.

### AMELIORATION DES PRIX OFFERTS PAR RAPPORT A EURONEXT

Pour étudier la qualité des prix proposés, sur la base des données du premier trimestre 2020, l’étude compare la fourchette proposée sur Euronext aux prix exécutés sur les SI au même moment\. La quasi\-totalité des montants déclarés par les ELP SI s’insèrent dans la fourchette proposée sur Euronext Paris, à l’exception des opérations au\- dessus du seuil des opérations dites de grande taille \(Large In Scale\)\. Les Bank SI négociant davantage à la voix avec leurs clients, et traitant majoritairement des gros montants, leurs prix s’écartent plus fréquemment de la cotation en continu du marché de référence\.

Parmi les montants exécutés sur SI dont le prix s’insère dans la fourchette Euronext \(soit la moitié des volumes

« price forming »\), 54% sont réalisés au même prix que celui coté au même moment dans le carnet français\. Les 46% restant sont opérés à un prix amélioré \(au regard du périmètre de l’étude un tick, ou « pas de cotation », soit l’écart minimal entre deux prix offerts successifs sur un marché, fait ici principalement référence à un pas de 0,05€ ou 0,01€\) pouvant aller d’un dixième de tick à quelques ticks, dont 12% avec une amélioration significative \(c’est\- à\-dire supérieure à un tick, cf\. graphiques 16 & 17\)\.

Ce sont les ELP SI qui cumulent, en proportion, la majorité des montants échangés avec amélioration de prix \(cf\. graphique 20\)\.

### RESPECT DU REGIME TICK SIZE A VENIR

MIF2 a instauré un régime de tick size, dont le calcul est fondé sur la liquidité et le prix des actions, ayant permis d’harmoniser les cotations sur plate\-formes « traditionnelles » et dont l’extension aux SI est prévue pour fin juin 2020\. L’étude permet de constater que près de 40% des volumes échangés sur les SI sont traités à un prix qui ne sera plus possible après l’application du régime de tick size aux SI \(cf\. graphique 11\)\. Par ailleurs, avec l’extension du régime aux internalisateurs systématiques, une majorité des améliorations de prix évoquées ci\-dessus, car considérées comme étant non significatives, ne sera plus possible : sur le premier trimestre, 43% des prix améliorés étaient inférieurs à un tick et n’auraient pas respecté le régime des pas de cotation\.

### QUALITE DES PRIX OFFERTS DU POINT DE VUE DU CLIENT

Afin d’apprécier la qualité des SI du point de vue du client, il est proposé d’étudier l’évolution des prix observés sur le marché de référence des actions françaises \(Euronext Paris\) autour d’une transaction sur un SI\. L’étude, qui a été réalisée sur la base des transactions des SI quelle que soit leur contrepartie, a mis en avant des niveaux de

« réversion » des prix assez hétérogènes parmi les différents acteurs \(cf\. graphique 21\)\. Or, comme cela a été évoqué, les prix offerts par les SI peuvent varier selon les clients\. Cette analyse, qui constitue un élément essentiel du processus MIF2 de meilleure exécution / meilleure sélection, est donc à conduire par chaque participant sur la base des propres prix auxquels il traite ou qu’il reçoit des SI, le cas échéant\.

# STRUCTURE DE MARCHÉ

### COMBIEN PÈSENT VERITABLEMENT LES SI DANS LE MARCHÉ ?

Quelques temps après l’entrée en vigueur de MIF2 le 3 janvier 2018, la part de marché importante des « internalisateurs systématiques » \(SI\) avait soulevé de nombreuses interrogations autour de ces entreprises d’investissement qui exécutent les ordres de leurs clients en dehors des plateformes de négociation face à leur compte propre\. Pour rappel, sur le marché actions français, __la part des SI publiée par les fournisseurs de données affichait plus de 30% sur les premiers mois après la prise d’effet de la directive européenne\. Deux ans après sur le premier trimestre 2020, le poids des SI est désormais évalué entre 15% et 20% des volumes échangés, __toujours selon les fournisseurs de données \(cf\. graphique 1 ci\-dessous\)\.

__Graphique 1 : Evolution des parts de marché des SI sur le marché actions français__

Source : Refinitiv, AMF

Note de lecture : Dans le graphique 1 ci\-dessus, « Plateformes – LIT » adressent les volumes des platefomes à carnet continu incluant marchés réglementés et MTF, « OTC reporté sur plateformes » désigne les transaction pré\-arrangées reportées au marché1 \(aussi connu sous le terme de « off\-book on exchange »\) et « Dark » englobe les volumes négociés sur les plateformes exemptées de transparence pré\-trade au titre des exemptions « Large In Scale » \(LIS\) ou « Reference Price » \(RP\) \(communément appelées « dark pool »\)\.

A la suite d’une clarification2 du *reporting *des transactions sous le régime SI, cette réévaluation à la baisse est venue confirmer les suspicions de surestimation à l’égard des chiffres observés peu de temps après l’application de la nouvelle règlementation\. Toutefois, la part des SI telle que publiée par les fournisseurs de données semble toujours plus élevée que celle attendue\.

Par ailleurs, sur 2019 et début 2020, le référentiel de l’ESMA a enregistré une augmentation de 48% du nombre de ces acteurs \(225 SI en mars 2020 contre 152 au troisième trimestre 20183\)\. Au premier trimestre 2020, sur les actions françaises, une partie de ces nouveaux acteurs \(5 sur un total de 36 SI actifs, pour un cumul de 7,7% des montants échangés sur SI\) sont les entités des acteurs originaires du Royaume\-Uni affichant désormais un SI opérant en Grande\-Bretagne et un second sur le Vieux Continent \(Brexit\)\.

1 Autrement dit les volumes usant du *waiver *« *negotiated transaction *» à la transparence pré\-*trade\.*

2 [https://www\.esma\.europa\.eu/sites/default/files/library/esma70\-872942901\-35\_qas\_transparency\_issues\.pdf](https://www.esma.europa.eu/sites/default/files/library/esma70-872942901-35_qas_transparency_issues.pdf)

3 Tous les SI référencés dans le registre ESMA ne sont pas actifs sur les actions\.

Afin d’analyser la liquidité apportée par les SI à l’ensemble du marché, il est tout d’abord utile de comparer leurs volumes à ceux affichés sur les plateformes *LIT*\. En effet, comme ces dernières, les SI sont soumis à un régime de transparence pré\-négociation4 et sont censés participer également à la formation du prix*\. *Pour cela, il convient de différencier parmi l’activité des SI celle participant à la formation du prix de celle relevant de transactions techniques : à défaut de déclarer ces dernières transactions en OTC, les SI doivent distinguer grâce à un *flag *\(repris par les fournisseurs de données\) la part de leurs opérations ne participant pas à la formation des prix\.

D’après Refinitiv, près de 30% des montants échangés sur les SI sur le premier trimestre 2020 ne participaient pas à la formation des prix\. Sur cette même période, la part des transactions sur SI participant à la formation des prix s’élèverait donc entre 11% et 14% des montants totaux échangés sur les actions françaises \(cf\. graphique 2 ci\- dessous\)\.

__Graphique 2 : Poids des SI dans les volumes totaux incluant les montants ne participant pas à la formation des prix__



Source : Refinitiv, AMF

L’AMF, via les déclarations de transactions imposées par MIF2 aux entreprises d’investissement \(*transaction reporting*\) dispose de toutes les transactions réalisées par les SI sur les instruments français\. Le graphe ci\-après compare les volumes issus de cette source, désignée dans cette étude par l’abréviation RDT\-TREM5, avec ceux de Refinitiv\.

__Graphique 3 : Comparaison des montants échangés sur les SI entre les données Refinitiv et RDT\-TREM__



Source : Refinitiv , RDT\-TREM, AMF

4 Un focus sur le régime de transparence pré\-trade des SI est proposé plus loin dans le document\.

5 Reporting Des Transactions \- Trade Reporting Exchange Mechanism\.

__Les montants déclarés dans le *reporting *au régulateur sont quasiment identiques à ceux publiés par Refinitiv__, à l’exception de la proportion des volumes déclarés comme ne participant pas à la formation du prix, qui, sur le mois de mars, semblent être sous\-estimés dans RDT\-TREM\. En effet, une étude de l’utilisation du *flag *« *non price forming *»6 dans le *reporting *au régulateur montre que certains déclarants n’utilisent jamais le *flag*, conduisant à considérer l’intégralité de leur flux comme « *price forming *» et à surestimer la part réelle des opérations « *price forming *», en dépit de cas présumés injustifiés\.

Afin d’estimer de façon plus précise ce que serait le flux « *price forming » *des SI accessible au marché, il est proposé, en plus du flag « *non price forming *», de soustraire aux volumes non déclarés comme « *non price forming *» les opérations intragroupes7, via l’analyse des contreparties des SI à partir des données RDT\-TREM\.

A partir de ce raisonnement, le graphique ci\-après illustre les résultats obtenus sur l’estimation de la vraie part des montants « *price forming *» à partir des données RDT\-TREM\. Par souci de clarté, ces volumes \(volumes non déclarés comme « *non price forming *» \- hors intragroupe\) seront dans la suite de la note désignés comme les volumes

« maximum *addressable *» des SI\.

__Graphique 4 : Poids des SI dans les volumes totaux incluant les montants ne participant pas à la formation des prix__

Source : Refinitiv , RDT\-TREM, AMF

Sur la base de ces observations, la présente étude réévalue la part des transactions des SI participant à la formation du prix accessibles par le marché entre 8% et 10% des montants totaux échangés sur les actions françaises sur le premier trimestre 2020 \(contre 12% en moyenne affichés par Refinitiv, qui intègre les transactions des SI\)\.

6 Pour rappel, ce flag est utilisé pour caractériser les opérations ne participant pas à la formation des prix\.

7 Pour cette étude, sur la base d’observations répétées, l’ensemble des opérations intragroupes sont considérées « *non price forming *» en dépit de pouvoir identifier au cas par cas celles qui contribueraient bien à la formation des prix\.

__Encadré 1: Focus période de crise Covid19, quel impact pour la part de marché des SI ?__

La pandémie que le monde connaît au début de l’année 2020 est à l’origine d’une crise sur les marchés financiers d’une ampleur exceptionnelle, comparable à celle de 2008\. En particulier sur la fin du mois de février et le mois de mars 2020, les volumes et la volatilité ont atteint leurs plus hauts depuis 10 ans\. Le graphique ci\-dessous présente en histogramme \(axe de gauche\), pour les actions françaises, les montants quotidiens échangés sur __tout le marché __\(gris\) et sur les __plateformes LIT __\(bleu foncé\)\. La part de marché journalière des __plateformes LIT __est représentée par la courbe bleue foncée et celle des __SI __est tracée en turquoise \(axe de droite\)\. Enfin, la __variation *close\-to\-close *de l’indice CAC 40 __est illustrée par la courbe rouge \(axe de droite\) et les séances marquées par les plus fortes baisses identifiées par des marqueurs ronds\.

Alors que __la part de marché des SI reste relativement stable __au cours de la période, __celle des plateformes *LIT *connait une hausse sensible, __atteignant en particulier ses extrêmes au cours des séances correspondant aux plus fortes baisses de l’indice\. A l’inverse, les séances de forte baisse sont également marquées par de parts de marché des plateformes LIT particulièrement élevées, à l’exception du 18 mars8\.

Au cours de ces journées de volatilité extrême, les plateformes *LIT *sont privilégiées aux SI car elle constituent l’origine de la formation des prix face aux autres lieux d’exécution susceptibles d’offrir des prix fréquemment obsolètes et décalés\.

__Evolution de la part de marché des SI et des plateformes LIT pendant la période de crise__

Source : Refinitiv, RDT\-TREM, AMF

8 La raison de ce comportement différent n’a pas été identifiée ; on observe toutefois, sans pouvoir expliquer une relation de cause à effet, que le 18/03/2020 est le premier jour de l’interdiction de création ou augmentation de positions courtes nettes \(*short ban*\) qui a suivi l’interdiction des ventes à découvert en France\.

### PANORAMA DES ACTEURS

Au cours du premier trimestre 2020, 36 SI actifs ont été observés sur le marché des actions françaises dans le *reporting *RDT\-TREM\. Parmi ceux\-ci, une distinction systématique sera proposée dans la suite de cette étude entre les SI des banques d’investissement \(28 entités9\) et ceux des *market\-makers *HFT \(8 entités10\), désignés communément respectivement par Bank SI et ELP SI \(pour Electronic Liquidity Provider SI\)\.

### Bank et ELP SI : quelle part de marché par acteur ?

Les ELP et Bank SI pèsent respectivement 24% et 76% des volumes « maximum *addressable *» sur SI \(cf\. graphique 5\.a\)\.

__Graphique 5 : Part de marché des acteurs ELP SI et des Bank SI sur T1 2020 dans les volumes « maximum *addressable *»__

__Graphique 5\.a Part de marché globale__

__Graphique 5\.b Part de marché en phase continue de trading__

Les montants en phase continue de *trading *représentent 65% des volumes

« maximum *adressable *» des SI\. Lors de cette phase, les ELP SI gagnent en part de marché\.

Source : RDT\-TREM, AMF

Par ailleurs, parmi les volumes « maximum *addressable *» __65% des volumes sont exécutés pendant la phase continue de *trading *d’Euronext Paris __\(i\.e\. avant la clôture\)\. Au cours de cette phase de *trading *continu, les __ELP SI représentent alors 37% des montants totaux échangés sur SI __\(cf\. graphique 5\.b\)\. A titre de comparaison, sur les actions du CAC 40, les HFT présentent une part de marché estimée à 58% en termes de montants négociés sur Euronext Paris avant le fixing11\.

Sur le premier trimestre 2020, durant la phase continue de *trading *de la bourse parisienne, la part de marché individuelle des acteurs a pu varier notablement : le premier acteur des Bank SI \(en termes de volumes traités\), qui représente près de 19% des volumes en moyenne sur la période, a vu sa part de marché décroître de 26% en décembre 2019 à 16% en mars 2020 \(cf\. graphique 6\.b\)\. A ce stade, les services ne sont pas en mesure d’identifier si cette baisse résulte d’une correction dans le *reporting *ou d’une réalité concurrentielle qui aurait amené l’acteur à perdre une partie de ses flux au profit d’un autre\. En même temps, celle du premier ELP SI, qui représente un peu plus de 11% de l’activité, a progressé de 8% à 13% \(cf\. graphique 6\.a\)\.

9 Dont 4 Bank SI avec une part de marché inférieur à 0,01%\.

10 Dont 1 ELP SI avec une part de marché inférieure à 0,01%\.

11 Cette part, qui croît traditionnellement avec la volatilité, est montée à 68% le 11 mars 2020\.

__Graphique 6 : Evolution de la part de marché SI pendant la phase de trading continu__

__Graphique 6\.a Evolution des parts de marchés des ELP SI__

__Graphique 6\.b Evolution des parts de marchés__

__des Bank SI__

Source : RDT\-TREM, AMF – sur le premier trimestre 2020

### Pour quelles tailles de transaction les SI sont\-ils privilégiés ?

Afin d’apprécier le niveau de liquidité apportée par les SI aux participants de marché, il est proposé de comparer la répartition des montants par transaction sur ces derniers avec celle observée sur le marché de référence pour les actions françaises \(Euronext Paris\)\. Pour cela, à partir de cette section, __les mentions faites aux volumes considérés sur les SI ne concernent que ceux issus des opérations « maximum *addressable *» et réalisées avant la clôture12__\. Un focus sur l’activité post\-*fixing *des SI est proposé en annexe\.

La répartition des montants échangés entre les Bank et les ELP SI en fonction de la taille des transactions met en avant un partage du flux entre les deux familles d’acteurs : __les ELP SI concentrent les opérations de petite taille tandis que les Bank SI sont préférés pour des transactions de bloc supérieures au seuil dit *Large In Scale *\(LIS\) __– allant de 500k€ à 650k€ pour les actions du CAC 40 \(cf\. graphique 7\)\.

__Graphique 7 : Répartition des montants traités entre SI Bank et SI ELP par taille de transaction__



Source : RDT\-TREM, AMF – sur le premier trimestre 2020

Note de lecture : Dans le graphique 7, comme dans tous ceux du même type qui suivront, la largeur des colonnes est proportionnelle au montant cumulé correspondant\. La colonne « Total » à droite du graphique donne la répartition entre les Bank et les ELP SI, toutes tailles de transaction confondues\.

### Par ailleurs, alors que les ELP SI affichent des tailles de transactions très similaires à celles observées sur la bourse parisienne, pour la plupart inférieures à 50k€, l’activité des Bank SI diffère fortement, en concentrant majoritairement des opérations de taille importante, supérieures à 200k€ \(cf\. graphique 8\)\.

12 Pour rappel, ces volumes représentent 65% des volumes « maximum *addressable *» et 47% des montants totaux échangés sur SI, soit 8% des volumes totaux du marché \(en considérant une part de marché moyenne des SI de 17% sur le premier trimestre 2020\)\.

__Graphique 8 : Répartition des montants traités sur Euronext et sur SI par taille de transaction__

Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

Il convient, dans cette analyse, de tenir compte de la différence structurante entre les SI, qui négocient une opération en bilatéral, et le carnet continu d’Euronext Paris dans lequel la taille des transactions est fragmentée par les ordres passifs\. Les tailles des transactions SI sont donc comparées à celles des ordres dits agressifs13 exécutés sur la bourse parisienne\. On constate que, dans ce cas, __les montants réalisés en\-dessous de 10k€ cumulent 30% des échanges en euros sur Euronext Paris __\(dans le cas – usuel – où c’est la taille des transactions qui est considérée, ces montants représentent alors 58% des volumes échangés\)\.

La taille médiane des transactions14 calculée sur les SI est légèrement inférieure à celle d’Euronext Paris \(6k€\) : 5,9k€ sur les Bank SI et 5,2k€ sur les ELP SI \(cf\. graphique 9\)\.

__Graphique 9 : Taille médiane des transactions sur Euronext et sur SI__

Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

13 Un ordre agressif est celui qui va rencontrer un ou plusieurs ordres passifs, déclenchant ainsi une ou plusieurs transactions\.

14 Bien que ces métriques soient généralement exprimées en terme de moyenne, les volumes traités sur les Bank SI étant très dispersés, la médiane apparait comme plus représentative pour pouvoir comparer les systèmes de négociation entre eux\. Pour information, la taille moyenne des transactions sur Bank SI s’élève à 62k€ contre 12k€ pour Euronext et 10k€ pour les ELP SI\.

# ENJEU LIMITÉ DE LA TRANSPARENCE PRÉ\-TRADE SUR LES SI

Les exigences en matière de transparence pré\-négociation pour le régime SI ne s’appliquent qu’aux actions liquides15 \(ainsi qu’aux instruments liquides assimilés aux actions comme les ETF et certificats\), soit, pour les actions qui constituent le périmètre de cette étude, environ 150 valeurs françaises\. Elles ont été définies autour du concept de *Standard Market Size *\(SMS\) – qui s’élève à 10k€ pour la quasi\-totalité des actions16\. En vertu de ces exigences de transparence pré\-négociation, les SI sont tenus de publier des prix fermes et de façon permanente, à l’achat et à la vente, pour une taille au moins égale à 10% de la SMS pour toute action sur laquelle ils ont le statut de SI\. Toutefois, les SI demeurent libres de proposer, en complément de leurs cotations publiques, des cotations bilatérales à leurs clients non soumises aux obligations de transparence pré\-négociation dès lors que leur taille est supérieure à la SMS\.

### QUELLE EST LA LIQUIDITE AFFICHEE PAR LES SI ?

Les SI disposent de trois options pour assurer la publication de leurs cotations : au travers de leur site, d’une plateforme ou d’un APA17 \(*Approved Publication Arrangement*\)\. Ceux\-ci doivent garantir un format spécifique, exploitable par les machines et identifiant le SI en question afin de permettre aux participants d’accéder aux prix de ce SI\. En pratique, la plupart des SI ont opté pour le canal de publication offert par l’APA de CBOE Global Markets et celui du London Stock Exchange Group, TRADEcho, tous deux relayés par les fournisseurs de données\.

Pour cette partie de l’analyse, l’AMF a étudié les données de cotation des SI sur le mois de septembre 2019 sur les actions françaises \(collectées via Refinitiv\)\. Il ressort, qu’en moyenne, __la plupart des SI proposent une cotation publique pour une taille allant de 15% et 40% de la SMS \(1,5k€\-4k€\)\. La majorité propose néanmoins un prix public pour une taille juste au\-dessus du seuil minimal requis \(1k€\)__\. Seuls deux ELP SI affichent des tailles avoisinant, ou dépassant, le seuil de la SMS \(cf\. graphique 10\)\.

__Graphique 10 : Quantité moyenne offerte par les SI à la cotation publique__

Classement des acteurs selon leur part de marché

Source : Refinitiv, AMF – sur septembre 2019

Note de lecture : les niveaux de cotation offert par le 2ème et le 6ème Bank SI n’étant pas disponibles dans Refinitiv, ceux\-ci ayant choisi un autre canal que celui d’un APA, ils ne sont donc pas représentés\.

15 Aux fins de l’article Article 2\(1\)\(17\)\(b\) du règlement \(EU\) No 600/2014, une action est considérée comme liquide si elle est traitée quotidiennement, si sa capitalisation flottante est au moins égale à 100 M€ pour les valeurs admises au *trading *sur un marché réglementé \(200 M€ pour les valeurs traitées sur MTF uniquement\), si son nombre moyen quotidien de transactions est supérieur à 250 et si son volume quotidien moyen échangé en euros est supérieur à 1M€\.

16 Toutes les actions liquides françaises ont une SMS égale à 10k€, à une exception près, LVMH, dont la SMS est égale à 30k€\.

17 Entités régulées sous MIF 2 en charge de la publication des transactions OTC \(incluant les SI\)\.

En comparaison avec la meilleure limite d’Euronext Paris, dont la profondeur s’élève en moyenne à près de 40k€ en septembre 2019 sur les actions liquides \(calculé hors phases de *fixing*\), __cette liquidité basée sur les cotations publiques des SI __paraît dérisoire\. Toutefois, cette liquidité __ne reflète pas la réalité des montants échangés __par les SI : si le 1er et le 3ème ELP SI en termes de part de marché se distinguent par une cotation publique moyenne \(respectivement 13,3k€ et 8,0k€\) supérieure aux volumes médians de leurs opérations, __pour la plupart des acteurs les tailles moyennes proposées à la cotation publique sont bien en\-dessous des tailles médianes des transactions__\. Avec un tel écart entre la quantité offerte à la cotation et les volumes exécutés, __il semblerait qu’une part importante des transactions provienne de flux différents de ceux publiés par les SI via les APA, flux qui échapperaient donc aux exigences de la transparence pré\-négociation du régime SI__\. Ce résultat est également confirmé par l’analyse des prix \(cf\. *infra*\)\.

### QUELLE EST LA PART DES MONTANTS EXECUTES SUR LES SI SOUMISE A LA TRANSPARENCE PRE\-TRADE ?

En croisant les données de cotations publiques des SI \(Refinitiv\) sur le mois de septembre 2019 avec les transactions reçues dans le *reporting *au régulateur \(RDT\-TREM\) sur la même période, la part des montants ayant bénéficié de la transparence pré\-négociation du régime SI est estimée en cumulant les volumes des transactions dont :

 le prix coté au même moment correspond à celui traité, et,

 la taille exécutée est inférieure ou égale à la quantité offerte publiée par le SI\.

Sur le mois de septembre 2019, __la part des montants __ayant bénéficié de la transparence pré\-négociation __s’élève à 22% des montants échangés sur SI en phase continue de *trading*, soit 1,4%__18 __des montants totaux échangés sur le marché sur cette période__\. L’apport des SI en matière de transparence apparaît donc très limité et s’explique par les différentes pratiques mises en place par l’industrie\. En effet, il ressort de discussions entre l’AMF et certains SI que :

- 
	- 
		1. les Bank SI, privilégiés pour les opérations de tailles importantes \(supérieures à 200k€\), négocient une part significative de leur activité à la voix avec leur clients \(« high touch »\)\. Compte tenu de leur taille, ces transactions échappent aux exigences du régime SI de la transparence pré\-négociation ;
		2. les principaux ELP SI ont mis en place des flux bilatéraux de cotations automatiques avec leurs clients pour des tailles au\-delà de la SMS \(10k€\)\. Ces flux ne sont pas publics, un même SI pouvant parfois envoyer des flux différents selon ses clients\. Un SI propose, par exemple, à ses clients de choisir entre un flux privilégiant des tailles importantes et un flux plus compétitif sur les prix mais pour des tailles moindres\. En septembre 2019, les montants cumulés sur les ELP SI pour les transactions en\-dessous de la SMS représentaient 25% de leurs montants totaux, autrement dit 75% de leur activité n’était pas soumise au régime SI de la transparence pré\-négociation\.

# FOCUS SUR LA QUALITÉ DES PRIX OFFERTS PART LES SI

Afin d’étudier la qualité des prix proposés par les SI, il pourrait paraître opportun de comparer les prix publiés par les SI dans le cadre des exigences de transparence pré\-négociation auxquelles ils sont soumis \(source Refinitiv pour le mois de septembre 2019\) avec la meilleure fourchette affichée sur Euronext Paris\. Toutefois, après analyse des transactions reportées au régulateur par les SI, il ressort que __seuls 31% des montants déclarés sur cette période sont réellement exécutés au prix publié au même moment par le SI__, indépendamment de la taille offerte\. __Il apparaît donc que les cotations publiques des SI ne sont pas représentatives des prix auxquels ces acteurs traitent *in fine*__\. Afin de mener cette analyse, Il est donc plus pertinent de comparer les prix exécutés sur les SI \(source RDT\-TREM\) à la meilleure fourchette affichée sur Euronext Paris \(les résultats de l’étude *infra *se basent sur les données du premier trimestre 2020\)\.

18 En septembre 2019, les SI représentaient 19% du marché total des actions françaises, 13% en ne retenant que les volumes « price forming » selon le reporting et 10% pour les volumes « maximum *addressable *»\. Les montants en phase continue de *trading *sur cette période réprésentaient 63%\. Ainsi, la part correspondant aux cotations transparentes de 22% s’applique à 6,3%, soit 1,4%\.

La qualité des prix étant à apprécier avec le respect du régime du *tick size *– auquel les SI ne sont pas encore soumis au premier trimestre 2020 – la première section de cette partie propose de mettre en évidence la part de l’activité des SI qui ne pourra plus être pratiquée après l’extension du régime *tick size *aux SI\. Pour rappel, le régime *tick size*, qui est une des dispositions de MIF 2, est venu harmoniser les pas de cotation minimums entre les plateformes *LIT19*\.

Pour rappel, les résultats de la partie *infra *sont basés sur les volumes « maximum *addressable *» des SI, c’est\-à\-dire les transactions ayant le plus vraisemblablement contribué à la formation des prix\.

### RESPECT DU REGIME TICK SIZE

L’extension du régime *tick size *aux SI, initialement prévue pour le 26 mars 2020 est reportée à fin juin 2020 – elle concernera les prix cotés, ajustés et traités\.

Le graphique 11 ci\-dessous introduit la part des montants échangés sur les SI dont le prix ne respecte pas le régime du *tick size*, selon que la transaction soit opérée avant ou après la clôture d’Euronext\. En somme, __40% des volumes totaux échangés sur les SI sont traités à un prix qui ne sera plus possible après l’application du régime du pas de cotation aux SI__\.

__Graphique 11 : Respect du régime tick size dans les prix traités par les SI__



Source : Refinitiv, RDT\-TREM, AMF – sur le premier trimestre 2020

En réalité il est probable qu’une partie des montants traités ne respectant pas le régime du *tick size *post\-*fixing *ne participe pas à la formation du prix \(même si le *reporting *en état les déclare comme tel à défaut de la présence du flag « non *price forming *»\)\. Ainsi, dès lors que les prix auxquels ces volumes sont échangés ne seront plus permis sur les SI, ces opérations seront certainement basculées en OTC\.

En revanche, bien que les prix publics cotés par les SI ne soient pas représentatifs de ceux qui sont finalement traités par ces acteurs, l’ensemble des SI semble déjà respecter le régime du *tick size *dans les prix offerts à la cotation publique \(cf\. graphique 12\) – à l’exception d’un acteur, dont la part de marché est inférieure à 2%\.

19 Dans la disposition MIF 2, le calcul du pas de cotation minimum est fondé sur le prix et la liquidité des instruments financiers concernés \(à savoir les actions, certificats de dépôts et ETF négociés sur plates\-formes européennes\)\.

__Graphique 12 : Proportion du temps dans lequel le régime du tick size est respecté par les prix de cotation__

Source : Refinitiv, AMF – sur septembre 2019

### QUELLE PART DE MARCHÉ POUR LE FLUX DES PRIX AMELIORÉS SUR LES SI ?

Afin de palier d’éventuels problèmes de qualité de la donnée concernant l’horodatage des transactions déclarées dans RDT\-TREM, qui pourraient venir compromettre le croisement avec la donnée des ordres d’Euronext Paris, les améliorations de prix des SI n’ont été étudiées que dans le cas où le prix exécuté est contenu dans la fourchette Euronext\.

Les graphiques 13 et 14 ci\-dessous présentent la répartition des montants exécutés sur les ELP et Bank SI dont le prix pratiqué est dans la fourchette du marché de référence\.

__Graphique 13 : Part des montants traités sur les ELP SI__

__dont le prix exécuté est contenu dans la fourchette Euronext par taille de transaction__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

__Graphique 14 : Part des montants traités sur les Bank SI__

__dont le prix exécuté est contenu dans la fourchette Euronext par taille de transaction__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

La part des montants exécutés dans la fourchette Euronext décroît pour des tailles de transactions de plus en plus importantes\. A l’exception des opérations au\-dessus du LIS, la quasi\-totalité des montants déclarés par les ELP SI s’insèrent dans la fourchette, ce qui n’est pas étonnant dans la mesure où les systèmes de ces derniers reposent sur des processus entièrement automatisés similaires à ceux des plateformes \(« low touch »\) impliquant généralement une bonne qualité de *reporting *au niveau de l’horodatage, tandis que les SI Bank sont susceptibles de pratiquer davantage de négociations à la voix avec leurs clients \(« high touch »\)\. Par ailleurs, les Bank SI traitant majoritairement des gros montants, il peut être normal que leurs prix s’écartent plus fréquemment de la cotation en continu dans le marché de référence\.

Pour les opérations dont le prix est en\-dehors de la fourchette, aucune tendance ne se dessine : les prix sont très dispersés et peuvent s’éloigner d’un seul *tick *comme de plusieurs centaines\. Une étude au cas par cas serait nécessaire pour ces transactions\. Le graphique 15, ci\-après, indique une répartition approximative des montants dès lors que le prix des transactions sur SI est en\-dehors de la fourchette établie au même moment sur la bourse parisienne, selon que celui\-ci soit dégradé ou amélioré, et en nombre de *ticks *\(par rapport aux prix d’Euronext\)\. Parmi les transactions concernées, les valeurs du CAC 40 cumulent plus de 90% des volumes bénéficiant d’un prix amélioré \(un *tick *faisant donc ici principalement référence à un pas de 0,05€ ou 0,01€\)\.

__Graphique 15 : Répartition des montants en nombre de *ticks*__

__lorsque le prix est en\-dehors de la fourchette affichée sur Euronext Paris__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

En réalité, il est peu probable que les SI proposent des améliorations au\-delà de la fourchette d’Euronext\. Les volumes échangés à des prix en\-dehors de la fourchette tels que présentés ici \(graphique 15\) pourraient donc résulter d’une erreur de déclaration dans le *reporting *RDT\-TREM : soit au niveau de l’horodatage des transactions, ce qui entraînerait alors la comparaison du prix du SI avec une fourchette d’Euronext affichée à autre instant ou soit du fait d’une absence du flag « *non price forming *», qui, dans le cas avéré, s’il avait bien été utilisé, aurait sorti l’opération du périmètre de l’étude\.

Il est donc préféré pour le reste de l’analyse de ne retenir que les montants exécutés dans la fourchette d’Euronext, soit la moitié des montants traités sur les SI, « maximum *addressable *» et exécutés pendant la phase continue de *trading*\.

Le graphique 16 met en évidence la répartition des parts des montants en fonction de l’amélioration de prix des SI \(toujours par rapport à ceux d’Euronext\) en nombre de *ticks*\.

__Graphique 16 : Répartition des montants par niveau d'amélioration du prix en nombre de *ticks*__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

Parmi les montants exécutés sur SI dont le prix s’insère dans la fourchette Euronext, 54% sont réalisés au même prix que celui coté au même moment dans le carnet parisien\. Le reste, __soit près de la moitié des montants négociés, est opéré à un prix amélioré __qui peut aller d’un dixième de *tick *à quelques pas de cotation \(cf\. graphique 17\)\.

__Graphique 17 : Répartition des montants__

__pour les niveaux de prix amélioré concentrant le plus de volume__

38% des montants échangés à un « meilleur prix » sont améliorés entre ½ et 1 *tick*\.

Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

Pour rappel, l’analyse de la qualité des prix est rendue difficile de par le *reporting *parfois incertain et l’étude n’a donc pu être réalisée que sur la moitié des montants \(i\.e*\. *en ne retenant que les opérations dont les prix des SI sont traités dans la fourchette Euronext\)\. __Ainsi, sur la moitié des volumes « maximum *addressable *» dont les prix ont pu être étudiés sur le premier trimestre 2020, seuls 46% sont échangés avec une amélioration de prix__, dont 12% avec une amélioration significative \(i\.e\. supérieure à un *tick*\)\. Toutefois, avec l’extension du régime des pas de cotation aux internalisateurs systématiques, une majorité des améliorations de prix évoquées ci\-dessus, car considérées comme étant non significatives, ne sera plus possible : sur le premier trimestre, 43% des prix améliorés étaient inférieurs à un *tick *et n’auraient pas respecté le régime des pas de cotation\.

Par ailleurs, alors que les ELP SI représentent la majorité des améliorations de prix jusqu’à un *tick*, au\-delà \(pour un *tick *et plus\), ce sont les Bank SI qui concentrent la plus grande part des volumes de ces opérations \(près des deux tiers\)\.

### BANK ET ELP SI : QUI PROPOSE LES MEILLEURES AMELIORATIONS DE PRIX ?

Le graphique 18 donne une répartition des montants avec amélioration de prix entre les Bank et ELP SI \(ce graphique est à lire en comparaison avec le graphique 6 : Répartition des montants traités entre Bank et ELP SI par taille de transaction\)\.

__Graphique 18 : Répartition des montants avec un prix amélioré entre les Bank et ELP SI par taille de transaction__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

__En proportion, les ELP SI cumulent la majorité des montants proposant une amélioration du prix __mais, au niveau de chaque acteur, le flux améliorant les prix \(par rapport à ceux d’Euronext\) __représente généralement 50% de l’ensemble des volumes traités par SI parmi les Bank SI __contre entre 17% et 60% pour les ELP SI\. En revanche, le reste des montants exécutés par les Bank SI le sont à un prix en\-dehors de la fourchette du marché de référence, tandis que les ELP SI, hormis leur flux « amélioré », réalisent le reste de leurs opérations aux mêmes prix que ceux affichés sur Euronext Paris \(cf\. graphique 19\)\.

__Graphique 19 : Proportion du flux « amélioration de prix » par acteur__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

Avec l’extension du régime *tick size *aux SI prévu pour fin juin 2020, une partie du flux d’amélioration des prix des acteurs à un dixième ou une moitié de *tick *ne sera plus possible : __sur le premier trimestre 2020, 57% des « prix améliorés » ne respectent pas le régime du pas de cotation __\(et plus largement 36% des volumes traités en phase continue de *trading*\)\. En particulier, le troisième ELP SI \(en termes de part de marché\), dont l’amélioration des prix est très concentrée autour d’un dixième de *tick *devra revoir son offre client \(cf\. graphique 20\)\.

__Graphique 20 : Focus sur les « améliorations de prix » des 6 plus gros acteurs__



Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

### REVERSION DES PRIX

Afin d’apprécier la qualité de ces lieux d’exécution du point de vue du client, il est proposé d’étudier l’évolution des prix observés sur le marché de référence des actions françaises \(Euronext Paris\) autour d’une transaction sur un SI\. Dans le graphique 21 ci\-après, les graphiques 21\.a & 21\.b présentent cette évolution des prix observés pour les 12 premiers ELP et Bank SI \(cumulant 88% des volumes « maximum *addressable *» en phase de *trading *continu\)\.

Chacune des courbes représente, pour un SI donné, la moyenne des prix observés sur Euronext Paris à divers horizons de temps autour d’une transaction sur ce SI : entre 5 minutes et 1 seconde avant et entre 1 seconde et 30 minutes après\. Les données ont été uniformisées dans le sens des achats pour les SI \(resp\. le sens des ventes pour les clients des SI\) afin que les courbes puissent représenter l’évolution des prix quel que soit le sens des transactions des SI \(i\.e\. à l’achat ou à la vente\)\.

__Graphique 21 : Evolution des prix observés sur Euronext après une transaction sur les SI Graphique 21\.a Pour les 6 premiers ELP SI__

__Graphique 21\.b Pour les 6 premiers Bank SI__

Source : Euronext, RDT\-TREM, AMF – sur janvier et février 2020

Note de lecture : Sens de la transaction \- Achat sur le SI \(i\.e\. vente du client\)

La partie du graphique située à gauche de la droite verticale orange, qui représente l’instant où la transaction du SI a eu lieu, permet de mettre en évidence dans quelle configuration chacun des SI effectue habituellement une transaction \(i\.e\. quels sont les prix pratiqués sur Euronext au même moment\)\. Compte tenu de l’obligation imposée

aux SI de coter des prix fermes et de façon permanente20, une transaction sur un SI donné traduit, outre la volonté du client de traiter sur ce SI, des conditions comparativement au moins aussi favorables sur ce SI que sur le reste du marché \(dont Euronext\) avec des prix et/ou tailles plus compétitifs offerts\.

La partie du graphique à droite met en avant l’évolution des prix traités sur Euronext après la transaction sur le SI\. Ces prix peuvent résulter notamment d’opérations en provenance \(i\) du SI \(dans le sens contraire à la transaction initiale du SI, en cas de débouclement ou de couverture sur le marché de la position acquise par le SI face au client par exemple, ou dans le même sens que la transaction initiale du SI\) ; \(ii\) du client \(qui poursuivrait sur le marché les opérations faites avec le SI, augmentant ainsi le risque de sélection adverse du SI\) ; \(iii\) ou d’autres participants \(opérations susceptibles de refléter une tendance de marché\)\.

Cette étude fait apparaître une grande variété de profils selon les acteurs\.

Tout d’abord, l’horizon de temps sur lequel une éventuelle réversion des prix s’observe \(i\.e\. lorsque les prix retournent au niveau initial de la transaction SI après un écart\) diffère selon le type du SI :

- les ELP SI traitant principalement de petites tailles \(de l’ordre de la SMS\), la réversion des prix s’observe majoritairement quelques minutes après la transaction ;
- les Bank SI concentrant les opérations au\-delà de 200k€, la réversion se manifeste sur une fenêtre plus lointaine, ici jusqu’à 30 minutes après la transaction\.

Au sein des deux grandes catégories d’acteurs, certains profils caractéristiques se distinguent plus particulièrement\.

Parmi les ELP SI :

- ELP\_1 et ELP\_4 offrent des prix compétitifs en absence de tendance marquée au préalable \(la partie gauche des courbes est relativement plate\)\. Après une transaction sur ces SI, on observe peu d’évolution sur les prix d’Euronext \(les prix ne baissent pas \(resp\. ne montent pas\) sensiblement après un achat du SI \(resp\. une vente\)\)\. Cette évolution des prix, particulièrement favorable après les transactions effectuées sur ELP\_1, semble montrer la capacité de certains SI à porter les positions issues de leur client, ou du moins à les gérer sans causer de *price impact *susceptible de compromettre la poursuite des exécutions de leur client\. Dans ces conditions, ces lieux d’exécution s’avèrent constituer des sources de liquidité intéressantes pour leurs clients\.
- ELP\_2 apparaît particulièrement compétitif en période de fort mouvement de marché \(à l’achat \(resp\. à la vente\) après une baisse \(resp\. une hausse\) significative des cours\) : la partie gauche de la courbe de l’acteur présente la pente la plus élevée de tous les SI représentés ici\. Un léger impact est observé, qui s’estompe à partir de 2 minutes après la transaction initiale\. La légère pente de la partie droite de la courbe pourrait s’expliquer par la poursuite de la tendance marquée qui semble s’être constituée en amont de la transaction sur le SI\.
- ELP\_3, ELP\_5 et ELP\_6 semblent en revanche être les acteurs \(parmi les SI ici représentés\) qui s’accompagnent de la plus grande baisse des prix consécutivement à la vente des clients sur un SI\. Alors qu’aucune tendance ne se dégage sur la partie gauche de leurs courbes, la pente de celles\-ci s’accroît significativement après la transaction sur le SI\. Cette baisse pourrait être causée par ces SI, suggérant que ces acteurs ne conservent pas longtemps la position acquise face au client et la débouclent rapidement sur le marché, reportant sur celui\-ci l’impact qu’aurait pu avoir le client s’il avait traité directement dessus\. On ne peut toutefois exclure que cette évolution des prix soit étrangère aux SI \(et soit imputable au client, voire d’autres participants\), mais cette évolution récurrente, synonyme de moins\-value pour le SI, pourrait être de nature à lui faire réviser sa politique commerciale sous peine de compromettre la profitabilité et la pérennité de son activité\.

20 Pour rappel, sur actions liquides\.

Pour les Bank SI, la plupart des acteurs ne montrent pas d’évolution défavorable pour la poursuite des exécutions du client \(sur la fenêtre des 30 minutes ici représentées\) à l’exception de Bank\_1 dont l’impact postérieur à une de ces transactions est particulièrement notable\. Cet acteur a par ailleurs connu une forte baisse de sa part de marché – près de 10% – depuis décembre 2019 \(voir graphique 6 ci\-dessus\)\.

Cette étude a été réalisée sur la base des transactions des SI quelle que soit leur contrepartie\. Or, comme cela a été évoqué, les prix offerts par les SI peuvent varier selon les clients\. Cette analyse, qui constitue un élément essentiel du processus MIF2 de meilleure exécution / meilleure sélection21, est donc à conduire par chaque participant sur la base des propres prix auxquels il traite ou qu’il reçoit des SI, le cas échéant\.

21 La meilleure exécution / meilleure sélection fait référence au devoir d'une entreprise de services d'investissement exécutant des ordres au nom des clients d'assurer la meilleure exécution possible pour les ordres de leurs clients, i\.e\. sélectionner le meilleur prix disponible sur le marché pouvant répondre aux caractéristiques de l’ordre ce ceux\-ci \(quantité, urgence, etc\.\)\.

__ANNEXE 1 : POST\-FIXING DE CLOTURE__

Parmi les montants traités sur SI après le *fixing *de clôture d’Euronext Paris \(seuls les Bank SI sont concernés\), qui représentent 35% des montants non déclarés comme non « *price forming » *\(hors intragroupe\), seuls 36% d’entre eux sont exécutés au prix de clôture importé depuis le marché de référence\. Pour les autres 64%, les prix sont très dispersés et nécessiteraient une étude au cas par cas pour en dégager d’éventuels enseignements

__Distribution des montants traités sur SI post\-*fixing *par niveaux de prix \(par rapport au prix du *fixing*\)__



## Source : Euronext, RDT\-TREM, AMF – sur le premier trimestre 2020

Dans le graphique ci\-dessus, un point représente la somme des montants échangés pour les transactions dont le prix traité se situe à une même distance, en pourcentage, du prix de référence\. Près de 77% des montants exécutés après clôture \(à un prix différent de celui du *fixing *d’Euronext\) ne respectent pas le régime du pas de cotation – autrement dit, 49% des volumes échangés post\-clôture sur les SI\.

## Il est important de noter qu’une transaction reportée après la clôture ne signifie pas nécessairement qu’elle n’était pas en risque : en milieu de journée, une entreprise d’investissement peut accepter en risque un ordre client en garantissant à ce dernier de l’exécuter au prix du *fixing *à venir \(« *fixing *garanti »\) ou au prix du VWAP jusqu’au *fixing*\. Les flags actuellement prévus par MIF2 ne permettent pas en revanche de préciser l’exacte nature de ces flux\.

