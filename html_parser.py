import markdown2
from bs4 import BeautifulSoup
import pandas as pd

def enlever_caracteres_indesirables(texte):
    # Remplace les caractères de saut de ligne par des espaces
    texte = texte.replace('\n', ' ')

    # Remplace le caractère '\xa0' par un espace
    texte = texte.replace('\xa0', ' ')

    return texte

def enlever_tableaux_et_balises(html):
    # Utilise lxml pour parser le HTML
    soup = BeautifulSoup(html, 'lxml')

    # Supprime tous les tableaux du HTML
    for tableau in soup.find_all('table'):
        tableau.decompose()

    # Retourne le texte sans balises ni caractères de saut de ligne
    return enlever_caracteres_indesirables(soup.get_text(strip=True, separator=' '))

def extraire_titres_sous_titres_texte_entier_paragraphe(chemin_fichier):
    # Lire le fichier Markdown et convertir le contenu en HTML
    with open(chemin_fichier, 'r', encoding='utf-8') as fichier:
        contenu_markdown = fichier.read()
        contenu_html = markdown2.markdown(contenu_markdown)

    # Utiliser BeautifulSoup pour extraire les titres, sous-titres et texte associé du HTML
    soup = BeautifulSoup(contenu_html, 'html.parser')
    donnees = []

    # Parcourir les balises de titre
    for tag in soup.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']):
        titre = tag.get_text(strip=True)
        niveau = int(tag.name[1])  # Récupère le niveau du titre (1 pour h1, 2 pour h2, etc.)

        # Trouver le texte associé au titre (y compris tous les éléments entre deux titres successifs)
        texte_associé = ""
        prochain_element = tag.find_next_sibling()
        while prochain_element and prochain_element.name and not prochain_element.name.startswith('h'):
            texte_associé += str(prochain_element)
            prochain_element = prochain_element.find_next_sibling()

        # Enleve les tableaux, supprime toutes les balises HTML et les caractères de saut de ligne du texte associé
        texte_associé = enlever_tableaux_et_balises(texte_associé)

        donnees.append({"niveau": niveau, "titre": titre, "texte_associé": texte_associé})

    # Créer un DataFrame pandas à partir des données
    df = pd.DataFrame(donnees)

    # Ajoute une colonne qui comprends une liste signifiant l'arborescence des chapitres
    df = ajouter_colonne_chapitre(df)

    return df

def extraire_titres_sous_titres_texte_entier(chemin_fichier):
    with open(chemin_fichier, 'r', encoding='utf-8') as fichier:
        contenu_markdown = fichier.read()
        contenu_html = markdown2.markdown(contenu_markdown)

    soup = BeautifulSoup(contenu_html, 'html.parser')
    donnees = []

    for tag in soup.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']):
        titre = tag.get_text(strip=True)
        niveau = int(tag.name[1])

        texte_associé = ""
        prochain_element = tag.find_next_sibling()

        while prochain_element and prochain_element.name and not prochain_element.name.startswith('h'):
            # Ajoutez cette boucle pour diviser le texte en paragraphes
            texte_associé += str(prochain_element.get_text()) + '\n'
            prochain_element = prochain_element.find_next_sibling()

        texte_associé = enlever_tableaux_et_balises(texte_associé)

        # Diviser le texte en paragraphes et créer une ligne par paragraphe
        paragraphes = texte_associé.strip().split('\n')
        for paragraphe in paragraphes:
            donnees.append({"niveau": niveau, "titre": titre, "texte_associé": paragraphe.strip()})

    df = pd.DataFrame(donnees)
    df = ajouter_colonne_chapitre(df)

    return df


def ajouter_colonne_chapitre(df):
    df['chapitre'] = df.apply(lambda row: [], axis=1)    
    max_level = df['niveau'].max()
    for level in range(max_level):
        chapter = 0
        for i in range(len(df)):
            if df.loc[i]['niveau'] <= level:
                chapter+=1
            df.loc[i]['chapitre'].append(chapter)

    return df
