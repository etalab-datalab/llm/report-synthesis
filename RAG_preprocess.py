import pandas as pd
from nltk.tokenize import sent_tokenize
import os
import mammoth
from html_parser import enlever_tableaux_et_balises, extraire_titres_sous_titres_texte_entier
import re
import pandas as pd


def split_text_into_chunks(title, text, chunk_size=3, overlap_ratio=0.2):
    sentences = sent_tokenize(text)

    overlap_sentences = int(chunk_size * overlap_ratio)
    chunks = []

    for i in range(0, len(sentences), chunk_size - overlap_sentences):
        chunk = sentences[i:i + chunk_size]
        if chunk:
            chunks.append(f"{title}: {' '.join(chunk)}")

    return chunks

def process_dataframe(df, chunk_size=3, overlap_ratio=0.2):
    result_data = {'titre': [], 'Chunk_Text': []}

    for index, row in df.iterrows():
        title = row['titre']
        text = row['texte_associé']

        chunks = split_text_into_chunks(title, text, chunk_size, overlap_ratio)

        result_data['titre'].extend([title] * len(chunks))
        result_data['Chunk_Text'].extend(chunks)

    result_df = pd.DataFrame(result_data)
    chunks_list = result_df['Chunk_Text'].tolist()

    return result_df



def process_docx_files(input_dir):
    # Create an empty list to store DataFrames
    dfs = []

    # Iterate over the files in the input directory
    for filename in os.listdir(input_dir):
        file_path = os.path.join(input_dir, filename)

        # Check if the file is of type DOCX
        if filename.lower().endswith('.md'):
            # Convert the DOCX file to a DataFrame
            df = extraire_titres_sous_titres_texte_entier(file_path)

            # Add a 'titre_document' column with the document title
            df['titre_document'] = os.path.splitext(filename)[0]

            # Append the DataFrame to the list
            dfs.append(df)

    # Concatenate all DataFrames in the list
    result_df = pd.concat(dfs, ignore_index=True)

    return result_df


def remove_image_tags(markdown_text):
    # Pattern for identifying Markdown image tags
    image_pattern = re.compile(r'!\[.*?\]\(.*?\)')

    # Replace all occurrences of Markdown image tags with an empty string
    text_without_images = re.sub(image_pattern, '', markdown_text)

    return text_without_images

def convert_docx_to_md(input_dir):
    # Create a new directory with "_md" at the end of the name
    new_directory = input_dir + '_md'
    os.makedirs(new_directory, exist_ok=True)

    # Iterate over the files in the original directory
    for fichier in os.listdir(input_dir):
        chemin_fichier = os.path.join(input_dir, fichier)

        # Check if the file is of type DOCX
        if fichier.lower().endswith('.docx'):
            # Convert the DOCX file to Markdown
            try:
                with open(chemin_fichier, "rb") as docx_file:
                    result = mammoth.convert_to_markdown(docx_file)
                    md_content = result.value

                # Remove image tags from the Markdown content
                md_content_without_images = remove_image_tags(md_content)

                # Create a new filename for the Markdown file in the output directory
                md_filename = os.path.splitext(fichier)[0] + ".md"
                md_output_path = os.path.join(new_directory, md_filename)

                # Save the Markdown content without images to the output file
                with open(md_output_path, "w", encoding="utf-8") as md_file:
                    md_file.write(md_content_without_images)

            except Exception as e:
                print(f"Error converting {fichier}: {e}")