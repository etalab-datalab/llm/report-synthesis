from docx import Document


# Nom du fichier DOCX de sortie
nom_fichier_docx = 'output/summary.docx'



def save_docx(df, path):

    # Initialiser un document Word
    doc = Document()

    # Itérer sur les lignes du DataFrame
    for index, row in df.iterrows():
        # Ajouter le titre
        doc.add_heading(row['titre'], level=1)

        # Ajouter le texte associé
        doc.add_paragraph(row['texte_associé'])

        # Ajouter un saut de page entre chaque titre-texte
        if index < len(df) - 1:
            doc.add_page_break()

    # Sauvegarder le document Word
    doc.save(path)

    return