__AVRIL 2021__

ANALYSE RÉTROSPECTIVE DE L'IMPACT DES CONTRATS DE LIQUIDITÉ SUR LE MARCHÉ FRANÇAIS \(DÉCEMBRE 2019 \- MAI 2020\)

ET PISTES D’ÉVOLUTION DE LA PRATIQUE DE MARCHÉ ADMISE

__KHEIRA BENHAMI ANNE DEMARTINI LAURENT SCHWAGER__

__amf\-france\.org__



Cette étude a été coordonnée par la Division Études, stabilité financière et risques et la Direction des Marchés\. Elle repose sur l’utilisation de sources considérées comme fiables mais dont l’exhaustivité et l’exactitude ne peuvent être garanties\. « Risques et tendances » reflète les vues personnelles de ses auteurs et n’exprime pas nécessairement la position de l’AMF\.

Toute copie, diffusion et reproduction de cette étude, en totalité ou partie, sont soumises à l’accord exprès, préalable et écrit de l’AMF\.

<a id="SYNTHÈSE"></a>__SYNTHÈSE__

__Cette étude analyse la pratique de marché admise des contrats de liquidité sur la Bourse de Paris \(Euronext et Euronext Growth\) entre décembre 2019 et mai 2020, une période instructive car comportant à la fois un fonctionnement normal du marché et des conditions de crise\. L’étude a été rendue possible par la mise en place d’un *reporting *permettant l’identification des interventions des apporteurs de liquidité dans le cadre des contrats de liquidité à partir du 2nd semestre 2019\. Les principales conclusions de l’étude sont les suivantes :__

Plus de 440 sociétés françaises ont bénéficié d’un contrat de liquidité sur la période d’étude, soit près de 70 % de la cote\. Une dizaine de prestataires de services d’investissement \(PSI\) ont été identifiés comme offrant ce service de liquidité\. Ce marché est du reste assez concentré en termes de parts de marché\. Ceci reflète une spécialisation des PSI selon la taille des entreprises clientes\.

 Bien que les interventions des PSI dans le cadre des contrats de liquidité soient fréquentes et régulières, les volumes de transactions sont limités : ils s’élèvent à 4,9 milliards d’euros sur 6 mois, ce qui représente moins de 1 % des montants échangés sur ces valeurs et moins de 0,5 % du total des montants échangés sur la Place de Paris \(i\.e\. l’ensemble des actions cotées\)\. Pour autant, les interventions peuvent être significatives pour les valeurs non liquides[1 ](#_bookmark0): pour un quart des séances de bourse durant lesquelles le PSI est intervenu, les montants échangés représentent plus de 20 % de l’activité quotidienne\.

 De fait, pour 3 % des séances, l’intervention du PSI teneur de marché permet à l’entreprise d’avoir un prix de cotation dans la journée\. Ces transactions de valorisation concernent 79 titres non liquides, appartenant pour la plupart au compartiment C d’Euronext ou à Euronext Growth\.

L’analyse statistique et économétrique montre que les contrats de liquidité permettent d’améliorer les spreads affichés ainsi que l’impact prix des transactions pour l’ensemble des titres \(non liquides, liquides et très liquides\)\. On constate, par ailleurs, que les contrats de liquidité viennent resserrer les *spreads *affichés des titres liquides et très liquides en période de stress de marché\. Le bilan est plus nuancé à propos des *spreads *effectifs, avec un effet positif pour les titres liquides seulement, qui ne perdure pas durant la crise\. L’impact observé est négligeable sur la profondeur de marché\.

__Les interventions au sein des contrats de liquidité peuvent s’inscrire dans le cadre d’une pratique de marché admise __sous réserve qu’elles respectent des conditions de volume, de prix et de ressources financières allouées\. Ces conditions sont variables selon le degré de liquidité des titres\. Elles ont été précisées par l’ESMA en 2017 et l’AMF a estimé nécessaire d’en assouplir certaines\. __Le non\-respect de ces conditions entraîne l’application du droit commun par la perte du *safe harbour *conféré par la pratique de marché admise \(PMA\)\.__

 __S’agissant des conditions de volumes __\(plafond de volumes quotidiens échangés une fois déduite une franchise de 20k€\) :

- Les dépassements des limites sont faibles en nombre comme en montants : ils représentent 420 dépassements sur les 6 mois étudiés, soit moins de 0,5 % des observations \(sur plus de 85 000 observations\) et à peine 12m€ \(soit 0,2 % des montants échangés dans le cadre des contrats de liquidité\)\.

<a id="_bookmark0"></a>1 Trois catégories de titres sont considérées : \(i\) les actions très liquides \(actions liquides au sens de MIF entrant dans la composition de l’indice CAC40 ; \(ii\) les actions liquides \(actions liquides au sens de MIF n’entrant pas dans la composition de l’indice CAC40\) et \(iii\) les valeurs non liquides \(autres actions\)\. En application du règlement délégué 2017/567 \(article 1er\), une action négociée quotidiennement est considérée comme ayant un marché liquide si toutes les conditions suivantes sont remplies :

1. le flottant de cette action : i\) est d'au moins 100 m€, pour les actions admises à la négociation sur un marché réglementé, ii\) est d'au moins 200 m€, pour les actions qui ne sont négociées que sur des systèmes multilatéraux de négociation \(MTF\) ;
2. le nombre quotidien moyen de transactions sur cette action est d'au moins 250 ;
3. le volume d'échanges quotidien moyen portant sur cette action est d'au moins 1 m€\.
	- Ces dépassements ne concernent qu’une minorité d’émetteurs \(un tiers de sociétés concernées\) et restent très épisodiques\.

__Des simulations faisant varier les critères en vigueur sur la franchise et les limites de volume ont été effectuées dans la perspective de leur possible évolution\. Elles montrent qu’un léger relèvement de la franchise de 20k€ à 25k€ conjugué à une augmentation de la limite ESMA en pourcentage du turnover de 25% à 30% permettrait de capturer les deux tiers des dépassements sur les valeurs non liquides\. Une augmentation de 5% de la limite relative aux actions liquides \(i\.e\. : en étant porté à 20%\) permettrait de capturer 80% des dépassements observés\. S’agissant de la limite réservée aux actions très liquides, le bénéfice d’un relèvement de la limite paraît moins évident\.__

 __Les dépassements en termes de limites de prix__, approximés par les resserrements des *spreads*, représentent environ un quart des montants échangés\. Il apparaît toutefois que ces resserrements des *spreads *correspondent pour l’essentiel à des ordres passifs ou partiellement passifs \(i\.e\. venant alimenter la liquidité du carnet d’ordres\)\. Les ordres agressifs \(i\.e\. venant consommer de la liquidité\) ne représentent pas plus de 7% des montants échangés pour les titres non liquides et moins de 2% pour les titres liquides et très liquides\.

 __S’agissant des conditions sur les ressources allouées par l’émetteur aux contrats de liquidité __\(plafond sur les montants absolus et les montants en relatif, en pourcentage de la capitalisation et en pourcentage du turnover\), il apparaît que :

- 
	- Pour les titres non liquides, 90% des contrats disposent d’une enveloppe n’excédant pas 1m€ et 95% des contrats disposent d’une enveloppe n’excédant pas 1% de la capitalisation boursière de l’émetteur \(pour une limite maximale fixée à 1% par l’ESMA et à 1,5% par l’AMF\)\. Elle montre par ailleurs que la limite en *turnover *ne semble pas pertinente pour cette catégorie de titres ;
	- Pour les titres liquides, 95% des contrats disposent d’une enveloppe n’excédant pas 20m€ \(soit la limite fixée par l’ESMA, la limite fixée par l’AMF étant de 30m€ par l’AMF\)\. De plus, 90% des contrats disposent d’une enveloppe n’excédant pas le turnover moyen maximal autorisé ;
	- 30% des contrats sur titres très liquides disposent d’une enveloppe atteignant le plafond de ressources \(50m€\) et 70% des contrats disposent d’une enveloppe représentant environ 70% du turnover moyen observé au cours de l’année 2020\.

# INTRODUCTION

Le règlement Abus de marché \(MAR\)[2 ](#_bookmark1)fournit un cadre harmonisé pour l'interdiction des manipulations de marché dans l’Union européenne mais prévoit la possibilité d’y déroger si les comportements observés l’ont été pour des raisons légitimes et conformément à une pratique de marché acceptée par une autorité nationale compétente \(« pratique de marché admise » ou PMA\)\.

Ainsi, l’AMF a établi les contrats de liquidité sur actions en tant que pratique de marché admise\. Conclu entre un émetteur et un prestataire de service d’investissement, le contrat de liquidité vise à favoriser la liquidité du marché et à assurer la régularité des cotations des titres de l’émetteur\. À cette fin, le prestataire de services d’investissement intervient sur le marché pour le compte de l’émetteur en utilisant une enveloppe de titres et d’espèces que ce dernier lui a confiée\.

En avril 2017, l’ESMA a précisé dans une Opinion dite « points de convergence » les conditions lui semblant nécessaires pour harmoniser la pratique de marché admise relative aux contrats de liquidité, en fixant notamment des limites d’intervention en termes de prix et de volumes, variables selon le degré de liquidité des titres[3](#_bookmark2)\. Après consultation de la Place, l’AMF a estimé nécessaire d’assouplir certaines de ces conditions\.

Du fait du faible nombre de pays dans laquelle existe cette pratique de marché et de la faible disponibilité des données, très peu d’études académiques ont été publiées sur le sujet\. Elles portent sur les marchés français \(Venkataraman et Waisburd, 2007\) ou suédois \(Anand, Tanggaard et Weaver, 2009\)\. Anand *et alii *montrent en particulier que la signature d’un contrat de liquidité contribue à une baisse des spreads cotés et une augmentation de la profondeur du carnet d’ordre, ainsi qu’à une diminution de la volatilité et à une augmentation de la taille des transactions\. De leur côté, Menkveld et Wang \(2013\) analysent l’impact des apporteurs de liquidité désignés par Euronext pour les valeurs les moins liquides et montrent notamment que leur intervention se traduit par une amélioration de la liquidité des titres concernés\.

Cette note analyse la pratique de marché admise des contrats de liquidités sur la Bourse de Paris \(Euronext et Euronext Growth\) entre décembre 2019 et mai 2020\. À la connaissance de l’AMF, aucune étude comparable n’a pu être réalisée par le passé dans la mesure où seule la mise en place d’un *reporting *à partir du 2nd semestre 2019, permettant l’identification des interventions des apporteurs de liquidité dans le cadre des contrats de liquidité, l’a rendue possible[4](#_bookmark3)\. Il convient de noter l’intérêt de la période étudiée, qui intègre des conditions de marché très diverses, puisqu’elle inclut à la fois des périodes de fonctionnement normal du marché et l’épisode d’importantes turbulences boursières enregistrées au début de la crise sanitaire\.

<a id="_bookmark1"></a>2 Règlement \(UE\) n° 596/2014 du Parlement européen et du Conseil du 16 avril 2014\.

<a id="_bookmark2"></a>3 Dans cette note, trois catégories de titres sont considérées :

- les actions très liquides : actions liquides au sens de MIF entrant dans la composition du panier sous\-jacent de l’indice CAC40 ;
- les actions liquides : actions liquides au sens de MIF n’entrant pas dans la composition du panier sous\-jacent de l’indice CAC40 ;
- les valeurs non liquides : autres actions\.

En application du règlement délégué 2017/567 \(article 1er\), une action négociée quotidiennement est considérée comme ayant un marché liquide si toutes les conditions suivantes sont remplies :

- 
	1. Le flottant de cette action: i\) est d'au moins 100 millions d'EUR, pour les actions admises à la négociation sur un marché réglementé, ii\) est d'au moins 200 millions d'EUR, pour les actions qui ne sont négociées que sur des systèmes multilatéraux de négociation \(MTF\) ;
	2. Le nombre quotidien moyen de transactions sur cette action est d'au moins 250 ;
	3. Le volume d'échanges quotidien moyen portant sur cette action est d'au moins 1 million d'EUR\.

<a id="_bookmark3"></a>4 À la suite des échanges entre Euronext, l’AMAFI et les services de l’AMF, l’entreprise de marché a mis en place un flag *ad hoc *permettant aux gestionnaires de contrats de liquidité de relayer à l’AMF l’information permettant d’identifier les ordres s’inscrivant dans la mise en œuvre d’un contrat de liquidité dans le fichier des ordres d’Euronext\.

__Graphique 1 : Évolution du CAC 40 et volatilité implicite__



*Source : Refinitiv*

L’étude s’attache dans un premier temps à identifier les entreprises ayant recours à un contrat de liquidité et les prestataires de services d’investissement \(PSI\) fournissant ce service, puis à mesurer l’activité des PSI dans le cadre des contrats de liquidité et leur impact sur la liquidité des titres concernés\. Enfin, une dernière partie est consacrée à l’analyse du respect des critères de l’ESMA et de l’AMF ainsi qu’à leur possible évolution\.

# PRESENTATION DE L’ECHANTILLON

## CARACTERISTIQUES DES EMETTEURS RECOURANT AUX CONTRATS DE LIQUIDITE

Entre le 1er décembre 2019 et le 30 mai 2020, 442 sociétés françaises dont les titres de capital étaient admis aux négociations sur Euronext et Euronext Growth ont eu recours, sur tout ou partie de la période, aux services d’un PSI dans le cadre d’un contrat de liquidité[5](#_bookmark4)\. Parmi elles, 6 sociétés ont changé d’apporteur de liquidité au cours de la période d’étude\.

Cette pratique est donc très répandue en France, puisqu’elle concerne près de 70 % des sociétés cotées, y compris des sociétés dont les titres sont considérés comme liquides\. Ainsi, si les trois quarts des titres bénéficiant d’un contrat de liquidité sont considérés comme non liquides au sens de la MIF, à l’opposé, plus de la moitié des entreprises françaises du CAC40 ont signé un contrat de liquidité\.

__Tableau 1 \- Répartition des sociétés de l’échantillon selon le critère de liquidité du titre et comparaison avec la cote au 30/06/2020__

__Échantillon__

__Cote parisienne au 30/06/2020__

__Degré de liquidité des titres__

__Nombre__

__%__

__Avec CL__

__%__

__Sans__

__CL__

__%__

__Total__

__%__

__% de sociétés avec__

__CL dans Total Cote__

Non liquides

339

77 %

333

76%

155

81%

488

78%

68%

Liquides

81

18 %

82

19%

23

12%

105

17%

78%

Très liquides \(CAC 40\)

22

5 %

22

5%

14

7%

36

6%

61%

Total

442

100 %

437

100%

192

100%

629

100%

69%

*Source : ESMA/Euronext, calculs AMF*

<a id="_bookmark4"></a>5 Parmi elles, 6 sociétés ont changé d’apporteur de liquidité au cours de la période d’étude\.

L’analyse de la répartition des sociétés par segment de marché selon qu’elles aient ou non conclu un contrat de liquidité montre néanmoins que les entreprises n’ayant pas souscrit de contrat de liquidité sont surreprésentées sur les segments de faible capitalisation \(Euronext C et Euronext Growth\)\. Une explication pourrait résider dans le fait que certains de ces émetteurs de petite taille, dont les titres ne sont pas liquides, n’auraient pas les ressources financières suffisantes pour allouer des capitaux à un contrat de liquidité\.

__Graphique 2 : Répartition des entreprises cotées à Paris ayant ou non conclu un contrat de liquidité par segment de marché \(au 30 juin 2020\)__



*Source : Euronext, calculs AMF*

## APPORTEURS DE LIQUIDITE

Sur la dizaine d’apporteurs de liquidité répertoriés, trois se partagent plus de la moitié du marché en termes de sociétés couvertes\. En revanche, en termes de volumes de transactions, la moitié des apporteurs de liquidité se partagent la quasi\-totalité du marché, un acteur concentrant à lui seul un tiers du marché\.

__Graphique 3 : Parts de marché__

__\(nombre d’entreprises couvertes\)__

__Graphique 4 : Parts de marché__

__\(volumes de transactions\)__

__PSI 10__

__0%__

PSI 10    PSI 8

PSI 1

6%

5%

5%

PSI 2

7%

PSI 7

9%

PSI 6

20%

PSI 4

21%

PSI 5

4%

PSI 9

9%

PSI 3

14%

__PSI 8__

__1%__

__PSI 7__

__1%__

__PSI 1__

__16%__

__PSI 2__

__18%__

__PSI 6__

__5%	PSI 5__

__0%__

__PSI 4__

__35%__

__PSI 3__

__24%__

*Source : Euronext, calculs AMF*

__PSI 9__

__0%__

Cet écart entre les parts de marché selon le nombre de sociétés suivies et selon le volume de transactions effectuées dans le cadre des contrats de liquidité reflète en partie l’incomplétude des déclarations des acteurs, même si une amélioration est perceptible depuis la mise en place du *reporting *fin 2019\. Ainsi, sur l’ensemble de la période d’étude, un acteur n’a reporté au régulateur aucune donnée permettant d’identifier les ordres passés et transactions réalisées dans le cadre des contrats de liquidité\.

L’écart reflète également une relative spécialisation des acteurs selon la taille des émetteurs\. Les apporteurs de liquidité ayant les plus importantes parts de marché en volume sont aussi ceux ayant conclu un contrat avec les sociétés présentes sur les compartiments A et B d’Euronext\. À l’opposé, certains PSI sont majoritairement positionnés sur les très petites valeurs, notamment d’Euronext Growth\.

__Graphique 5 : Répartition des sociétés bénéficiant d’un contrat de liquidité par segment de marché et apporteur de liquidité__

PSI 1 PSI 2 PSI 3 PSI 4 PSI 5 PSI 6 PSI 7 PSI 8 PSI 9 PSI 10

78%

22%

58%

35%

6%

35%

30%

21%

14%

33%

34%

22%

12%

11%

5%

32%

53%

4%

20%

32%

43%

3%

15%

30%

53%

42%

33%

25%

13%

31%

56%

5%

25%

70%

Euronext A	Euronext B	Euronext C	Euronext Growth

*Source : Euronext, calculs AMF, prestataires de services d’investissement*

# ANALYSE DU MARCHÉ : L’ACTIVITÉ DANS LE CADRE DES CONTRATS DE LIQUIDITÉ APPARAÎT GLOBALEMENT LIMITÉE MAIS SIGNIFICATIVE POUR LES VALEURS NON LIQUIDES

Entre début décembre 2019 et fin mai 2020, 4,9 milliards d’euros ont été échangés dans le cadre de contrats de liquidité\. Au niveau global, ce montant représente à peine 0,8 % du total des montants échangés sur ces valeurs \(et 0,4 % du total des montants échangés sur l’ensemble de la cote parisienne\)\.

__Tableau 2 : Montants échangés dans le cadre du contrat de liquidité__

Contrats de liquidité : Montants

échangés \(Buy & Sell, Mds€\)

Montants totaux échangés

\(single count, Mds €\)

%

Non Liquides

0,4

10,6

2,0%

Liquides

2,4

90,1

1,3%

Très Liquides

2,1

221,8

0,5%

__Total général__

4,9

322,6

0,8%

*Source : Euronext, calculs AMF*

Les montants échangés dans le cadre des contrats de liquidité ont par ailleurs connu une très nette diminution après le déclenchement de la crise de la Covid \(baisse de moitié entre mars et avril\)\. Celle\-ci s’est amorcée au mois de mars, tandis que les montants totaux échangés sur les mêmes titres ont connu un point haut au cours de cette période\.

__Graphique 6 : Montants et valeurs échangées dans le__

__cadre d’un contrat de liquidité __\(en millions d’euros\)

__Graphique 7 : Montants et valeurs échangées à Paris__

\(en milliards d’euros\)

1 200

Millions €

1 000

800

600

400

200

\-

déc\-19	janv\-20		févr\-20	mars\-20	avr\-20	mai\-20 Montant \(M€\)	Nb de valeurs échangées \(échelle de droite\)

400

395

390

385

380

375

370

365

360

355

350

*Source : reporting des transactions, calculs AMF*

Cette diminution des échanges ne s’explique pas par des suspensions officielles des contrats de liquidité depuis le déclenchement de la crise Covid\. En effet, seules 2 notifications ont été reçues par l’AMF en mars 2020 \(concernant les titres Accor et Antalis\), et 3 sur l’ensemble de la période étudiée \(décembre 2019 – mai 2020\)\. À titre de comparaison, 7 suspensions de contrats de liquidité avaient été enregistrées au cours du premier semestre 2019\. On remarque néanmoins que, dans les faits, certains PSI ont totalement stoppé leurs interventions sur le mois de mars et début avril\. De fait, les PSI teneurs de marché ont fortement accru le poids des échanges effectués en dehors du contrat de liquidité, passant de moins de la moitié en début de période à près des deux tiers des échanges en mai 2020\.

__Graphique 8 : Montants échangés par les PSI teneurs de marchés sur les titres bénéficiant d'un contrat de liquidité __\(en millions d’euros\)

3 000	60%

54%

53%

52%

35%

37%

33%

Millions

2 500	50%

2 000	40%

1 500	30%

1 000	20%

500	10%

0	0%

déc\.\-19	janv\.\-20	févr\.\-20	mars\-20	avr\.\-20	mai\-20

Hors contrat de liquidité	Contrat de liquidité	Part des contrats de liquidité

*Source : reporting des transactions, calculs AMF*

Il conviendra de noter également que 3 PSI ne déclarent aucune transaction hors contrat de liquidité\. À cet égard, il semblerait que ces PSI préfèrent en effet passer pour ces transactions par un autre membre de marché lui fournissant un accès direct au marché \(DMA\), en raison de coûts plus faibles[6](#_bookmark5)\.

D’une manière générale, la fréquence d’intervention des PSI dans le cadre des contrats de liquidité est élevée, et ce quel que soit le niveau de liquidité\. Pour la moitié des valeurs liquides ou non liquides, les PSI interviennent, sauf exception, lors de chaque séance de cotation \(graphique de gauche\)\. En revanche, le poids des volumes échangés dans le cadre des contrats de liquidité par rapport aux volumes quotidiens est d’autant plus important que la liquidité des titres est faible\. Sur les valeurs non liquides, pour un quart des séances de bourse durant lesquelles le PSI est intervenu, les montants échangés représentent plus de 20 % de l’activité quotidienne, voire 40 % dans 10 % des cas \(graphique de droite\)\.

__Graphique 9 : Fréquence d’intervention des PSI dans__

__le cadre des contrats de liquidité __\(% du nombre total

de séances de cotation par valeur\)

__Graphique 10 : Poids des volumes échangés dans le__

__cadre des CL par rapport aux volumes totaux__

\(données quotidiennes, en %\)

100

__N = 21__

Not Liquid

Liquid

Very Liquid

P5      P10	Q1      Q2      Q3      P90	P95

__N = 299__

__N = 78__

80

60

40

20

0

*Source : reporting des transactions, calculs AMF*

<a id="_bookmark5"></a>6 Les gestionnaires de contrat de liquidité ont l’obligation d’intervenir en tant que membre du marché pour les transactions relevant du contrat de liquidité\. Cette disposition avait été retenue dans la pratique de marché admise pour améliorer la traçabilité des opérations concernées\.

Les montants moyens échangés dans le cadre de contrats de liquidités sont très faibles et ne dépassent pas 10 000 € pour une majorité d’émetteurs\.

__Tableau 3 : Distribution des montants moyens échangés par émetteur et par séance de cotation dans le cadre d’un contrat de liquidité __\(en milliers d’euros\)

__Nb__

__P5__

__P10__

__Q1__

__Médiane__

__Q3__

__P90__

__P95__

Non liquides

299

0,6

1,0

2,6

5,7

13,2

25,8

34,4

Liquides

78

21,0

40,5

71,2

193,3

407,0

694,9

907,9

Très liquides

21

276,6

321,5

451,5

898,5

1 358,1

2 157,1

2 181,0

__Total__

__398__

__0,6__

__1,2__

__3,7__

__10,4__

__40,5__

__399,8__

__708,1__

*Source : reporting des transactions, calculs AMF*

De fait, pour 3 % des séances, l’intervention du PSI teneur de marché permet à l’émetteur d’avoir un prix de cotation dans la journée\. Ces « transactions de valorisation » \(dans lesquelles on retrouve l’apporteur de liquidité à l’achat et à la vente pour une seule action échangée[7](#_bookmark6) dans la journée\) concernent 79 titres non liquides, appartenant pour la plupart d’entre elles au compartiment C d’Euronext ou Euronext Growth\. Elles représentent plus du quart des sociétés de ces segments de marché bénéficiant d’un contrat de liquidité\.

__Tableau 4 : Valeurs bénéficiant d’un contrat de liquidité ayant fait l’objet au moins une fois d’une transaction de valorisation au cours d’une séance de négociation__

__Euronext B__

__Euronext C__

__Euronext Growth__

__Total__

Nombre

17

29

33

79

%

16%

28%

26%

18%

*Source : reporting des transactions, calculs AMF*

Pour certaines de ces valeurs, ces transactions de valorisation sont très fréquentes : elles concernent près de la moitié des séances de cotation pour une dizaine de sociétés\.

__Tableau 5 : Fréquence des transactions de valorisation par émetteur__

\(Par rapport à l’ensemble des séances de cotation\)

__Nombre	des	valeurs__

__concernées__

__Moyenne__

__Q1__

__médiane__

__Q3__

__perc90__

__perc95__

__perc99__

79

15,5

2,0

7,3

19,3

54,2

75,2

96,7

*Note de lecture : pour 25% des valeurs ayant fait l’objet au moins une fois d’une transaction de valorisation dans le cadre du contrat de liquidité, cette unique transaction de valorisation permet d’afficher un prix pour près de 20% des journées de négociation durant la période étudiée\.*

*Source : reporting des transactions, calculs AMF*

Pour les valeurs les moins liquides, l’existence d’un contrat de liquidité améliore ainsi très nettement le taux de cotation : la quasi\-totalité de ces valeurs bénéficiant d’un contrat de liquidité et présentes sur toute la période d’étude affichent un cours lors de chaque séance de bourse\. À l’inverse, un quart des valeurs sans contrat de liquidité cotent sur moins de 37 % des séances\.

<a id="_bookmark6"></a>7 Les « transactions de valorisation » s’inscrivent dans un mécanisme mis en place par Euronext pour permettre d’afficher un cours indicatif sans générer de transaction\.

__Tableau 6 : Fréquence de cotation des valeurs non liquides__

\(Jours de cotation/nombre de séances sur la période, en %\)

__Nombre__

__Moyenne__

__P10__

__Q1__

__Q2__

__Q3__

Pas de CL

144

66

16

37

77

100

Avec CL

304

99

100

100

100

100

*Note de lecture : 25% \(Q1\) des valeurs ne bénéficiant pas d’un contrat de liquidité affichent un cours moins 37 % des journées de négociation\.*

*Source : reporting des transactions, calculs AMF*

# LES CONTRATS DE LIQUIDITE PERMETTENT\-ILS D’AMELIORER LA LIQUIDITE DES TITRES ?

## DESCRIPTION DES VARIABLES ET STATISTIQUES DESCRIPTIVES

Afin de mesurer l’éventuel apport de liquidité induit par les contrats de liquidité, quatre mesures classiques sont analysées successivement[8 ](#_bookmark7):

    __La fourchette de prix affichée __*\(quoted spread\) *est égale à la différence entre le meilleur prix à la vente et le meilleur prix à l’achat affichés dans le carnet d’ordre\. Elle est exprimée en points de base \(i\.e\. rapportée au milieu de fourchette correspondant\)\. Plus la fourchette est large, plus le coût d’exécution d’un ordre sera élevé et plus la liquidité sera faible ;

 __la fourchette de prix effective __*\(effective spread\) *correspond, pour sa part, au coût indirect de transaction effectivement payé par un investisseur au moment de l’échange : elle est définie comme l’écart entre le prix de la transaction et le milieu de la fourchette observé au moment de la transaction \(en points de base, i\.e\. rapporté au milieu de fourchette\)\. Un *effective spread *qui augmente reflète une dégradation de la liquidité ;

 __la profondeur de marché __*\(depth\) *correspond aux montants disponibles aux meilleures limites \(i\.e\. exprimée en euros\)\. Plus la profondeur est importante, meilleure est la liquidité ;

    __l’indicateur d’illiquidité d’Amihud__[9](#_bookmark8), qui est une mesure de l’impact de marché, vise à mesurer la sensibilité des prix aux volumes échangés\. Il rapporte la valeur absolue de la variation de prix entre deux transactions au volume associé\. Il peut être interprété comme la réponse des prix associée à un euro de volume d'échanges\. L’indicateur d’Amihud est négativement corrélé avec la liquidité d’un titre\. En effet, plus le titre est liquide, plus le volume de transaction requis pour faire bouger le prix du titre devra être important\.

<a id="_bookmark7"></a>8 Pour une revue des mesures de liquidité, se reporter notamment à Fong, Holden et Trzcinka \(2017\)\.

<a id="_bookmark8"></a>9 Voir Amihud \(2002\)\.

__Tableau 7 : Statistiques descriptives pour l’ensemble des titres, par classe de liquidité__



*Source : reporting des transactions, calculs AMF*

## L’analyse des spreads

Dans le cas des valeurs non liquides, les titres bénéficiant d’un contrat de liquidité arborent des spreads affichés et effectifs plus faibles que ceux des autres titres\. Cette différence est beaucoup moins évidente pour les valeurs liquides\. Pour les valeurs les plus liquides, les *spreads *affichés semblent mêmes être plus larges pour les titres bénéficiant d’un contrat de liquidité, en étant probablement la conséquence d’un biais de composition car les valeurs du CAC ayant recours aux contrats de liquidité sont en moyenne moins liquides que celles n’y recourant pas[10](#_bookmark9)\. On notera que durant la phase de correction boursière, la présence du contrat de liquidité semble permettre de limiter l’impact sur les *spreads *pour les titres liquides et, dans une moindre mesure, pour les titres non liquides\.

<a id="_bookmark9"></a>10 cf\. statistiques descriptives \(Tableau 5\)\.

__Graphique 11 : Évolution des spreads affichés et effectifs par degré de liquidité du titre__

\(décembre 2019\-mai 2020\)

Les *spreads *sont exprimés en points de base i\.e\. l’écart de prix est rapporté au milieu de fourchette prévalant\.

Les courbes bleues \(respectivement rouges\) correspondent aux valeurs observées pour les titres bénéficiant d’un contrat de liquidité \(respectivement, ne bénéficiant pas d’un contrat de liquidité\)\. La barre verticale indique le début de la phase de correction boursière liée à la crise sanitaire \(18/02/2020\)\.

__Spreads affichés__

__Spreads effectifs__

__A\)	Valeurs non liquides__

__B\)	Valeurs liquides__

__C\)	Valeurs très liquides__

	

*Source : reporting des transactions, calculs AMF*

## Profondeur de marché

Les quantités offertes aux meilleures limites apparaissent plus importantes pour les titres liquides ne bénéficiant pas d’un contrat de liquidité, surtout en conditions normales de marché\. S’agissant des valeurs très liquides et non liquides, il ne semble pas y avoir d’écart entre les titres bénéficiant d’un contrat de liquidité et ceux qui n’en bénéficient pas\.

__Graphique 12 : Évolution des montants disponibles à la meilleure limite par degré de liquidité du titre \(décembre 2019\-mai 2020\)__

La profondeur est exprimée en euros\. Les courbes bleues \(respectivement rouges\) correspondent aux valeurs observées pour les titres bénéficiant d’un contrat de liquidité \(respectivement, ne bénéficiant pas d’un contrat de liquidité\)\. La barre verticale indique le début de la phase de correction boursière liée à la crise sanitaire \(18/02/2020\)\.

__A\) Valeurs non liquides__

__B\)   Valeurs liquides__

__C\)   Valeurs très liquides__

	 

*Source : reporting des transactions, calculs AMF*

## Impact prix

L’analyse de l’indicateur d’impact\-prix d’Amihud montre que dans le cas des valeurs non liquides, la liquidité est améliorée pour les titres bénéficiant d’un contrat de liquidité\. Pour les valeurs liquides, l’apport des contrats de liquidité est surtout perceptible après le déclenchement de la correction boursière\. Il semble en revanche peu significatif pour les valeurs très liquides et ce sur l’ensemble de la période d’étude\.

__Graphique 13 : Évolution de l’indicateur d’illiquidité par degré de liquidité du titre \(décembre 2019\-mai 2020\)__

L’indicateur d’Amihud s’interprète comme la réponse des prix associée à un euro de volume d'échanges\. Les courbes bleues \(respectivement rouges\) correspondent aux valeurs observées pour les titres bénéficiant d’un contrat de liquidité \(respectivement, ne bénéficiant pas d’un contrat de liquidité\)\. La barre verticale indique le début de la phase de correction boursière liée à la crise sanitaire \(18/02/2020\)\.

__A\) Valeurs non liquides__

__B\)   Valeurs liquides__

__C\)   Valeurs très liquides__

		

*Source : reporting des transactions, calculs AMF*

Les tendances observées à travers ces indicateurs doivent faire l’objet d’une analyse plus approfondie\. En effet, comme indiqué précédemment et comme le montrent les statistiques descriptives, des biais de composition peuvent exister\. Ainsi, parmi les titres très liquides, ce sont sûrement les titres les moins liquides qui ont recours au contrat de liquidité\. De même, parmi les titres les moins liquides, seuls ceux qui ont les moyens d’allouer des ressources à un contrat de liquidité peuvent y avoir recours\. L’analyse économétrique qui suit permet de prendre en compte ces variabilités et de capter l’effet des contrats de liquidité sur le degré de liquidité des titres\.

## ANALYSE ECONOMETRIQUE

- 
	- 
		1. __Description de la modélisation__

L’analyse proposée cherche à tester l’hypothèse nulle selon laquelle les contrats de liquidité permettent d’améliorer la liquidité des titres\. L’intérêt de la période d’étude considérée \(décembre 2019\-mai 2020\) réside dans le fait que cette liquidité pourra également être appréciée dans des conditions stressées de marchés\.

Pour ce faire nous considérons le modèle suivant :

𝐿𝐿𝐿𝐿𝐿𝐿𝐿𝐿𝑖𝑖   = 𝖺𝐿𝐿\+ 𝛽𝛽1𝑝𝑝𝑝𝑝𝑝𝑝𝑝𝑝𝑝𝑝𝐿𝐿𝐿𝐿𝑖𝑖  \+ 𝛽𝛽2𝑝𝑝𝐿𝐿\_𝑎𝑎𝑎𝑎𝑖𝑖𝐿𝐿𝑎𝑎 𝑥𝑥 𝑎𝑎𝑝𝑝𝐿𝐿𝑐𝑐𝑐𝑐𝐿𝐿𝑖𝑖  \+ 𝛽𝛽2𝑝𝑝𝐿𝐿𝐿𝐿𝑖𝑖  \+  β𝑋𝑋𝐿𝐿𝑖𝑖  \+ 𝛾𝛾𝐿𝐿  \+ 𝛿𝛿𝑖𝑖  \+  εit

où 𝐿𝑳𝑳𝑳𝑳𝑳𝑳𝑡 est l’indicateur de liquidité moyen observé à la date t pour le titre i, i\.e\. le spread affiché *\(quoted spread\)*, le spread effectif \(*effective spread*\), la profondeur à la meilleure limite \(logdepth\) ainsi que l’indicateur d’impact\-prix d’Amihud \(multiplié par 1 000\)\.

La variable 𝐶𝐿𝑳𝑳𝑡  est une variable indicatrice qui prend la valeur 1 lorsque le titre bénéficie d’un contrat de liquidité\. Toutefois, le fait de bénéficier d’un contrat de liquidité n’implique pas que le teneur de marché intervient sur la valeur\. De plus, cette variable va également refléter les biais exposés dans la section précédente\. C’est pourquoi, afin de capter l’apport réel du teneur de marché, nous considérons la  proportion  du  volume  échangé  dans  le  cadre  du  contrat  de  liquidité  \(𝑝𝑟𝑜𝑝𝐶𝐿𝑳𝑳𝑡  \)\.  Ainsi,  si  notre hypothèse nulle est vérifiée, le coefficient associé à la variable devra être significatif et négatif pour les variables mesurant les spreads et l’impact\-prix et positif pour la variable mesurant la profondeur de marché\.

L’impact des conditions stressées de marché est capté par la variable 𝐶𝑟𝑳𝑳𝑠𝑒 qui est une indicatrice égale à 1 durant la période liée à la crise sanitaire de la Covid \(de mars 2020 à mai 2020\)\. Afin de déterminer si l’activité liée aux contrats de liquidité est bénéfique pour les titres concernés, nous la croisons avec une autre variable indicatrice qui prend la valeur 1 lorsque le teneur de marché est actif durant la journée considérée  \(𝐶𝐿\_𝑎𝑐𝑡𝑳𝑳𝑓 𝗑 𝑐𝑟𝑳𝑳𝑠𝑒𝑳𝑳𝑡\)\. Cette  variable  permet  de  prendre  en compte  le  fait  que, même si certains titres continuaient de bénéficier en théorie du contrat de liquidité, certains prestataires ont stoppé leurs interventions sur les mois de mars et avril \(cf\. partie 1\)\. Un coefficient significatif et inférieur à celui associé à 𝐶𝐿𝑳𝑳𝑡  dénotera d’un apport de liquidité par ces contrats en période de stress\.

Enfin,  X𝑳𝑳𝑡   correspond  à  une  matrice  rassemblant  l’ensemble  des  variables  de  contrôles  calculées quotidiennement pour chaque titre \(capitalisation boursière, volumes échangés niveau de prix, volatilité du titre, volatilité du marché\)\. 𝜸𝜸𝑳𝑳  et 𝜹𝜹𝑡  représentent les effets fixes liés respectivement aux titres et à la période\.

## Résultats

Le tableau, ci\-après, présente les résultats obtenus pour chaque catégorie de titres[11](#_bookmark10)\.

Concernant les *spreads *affichés, toutes choses égales par ailleurs, ils apparaissent plus resserrés pour les titres non liquides bénéficiant d’un contrat de liquidité\. *A contrario*, les *spreads *affichés pour les titres liquides et très liquides se révèlent plus larges\.

Les résultats montrent par ailleurs que la présence active du teneur de marché améliore les *spreads *affichés pour l’ensemble des titres non liquides, liquides et très liquides : ils diminuent respectivement de 3,7 pb, 4 pb et 1,37 pb lorsque l’activité liée au contrats de liquidité croît de 10 %\.

Le bilan est plus nuancé pour les *spreads *effectifs : s’ils se resserrent avec l’activité du contrat de liquidité pour les titres liquides \(\- 1,33 pb pour une augmentation de 10 % de l’activité\), ils augmentent pour les titres non liquides \(\+1 pb\) et varient non significativement pour les titres très liquides\.

La présence du teneur de marché en période de crise permet par ailleurs de densifier le carnet d’ordres des titres liquides et très liquides et de limiter l’impact sur les *spreads *affichés \(qui présentent des coefficients associés à la variable 𝐶𝐿\_𝑎𝑐𝑡𝑳𝑳𝑓 𝗑 𝑐𝑟𝑳𝑳𝑠𝑒𝑳𝑳𝑡   significatifs et inférieurs à ceux associés à 𝐶𝐿𝑳𝑳𝑡\)\. Nous n’observons pas cette densification pour les titres non liquides\. De même, nous n’observons pas d’impact significatif de la tenue de marché sur les *spreads *effectifs durant la crise\.

Pour ce qui est de la profondeur de marché, si les coefficients associés à la part de marché des contrats de liquidité sont significatifs et négatifs pour les titres non liquides et liquides, l’impact associé apparaît négligeable et même non significatif pour les titres très liquides\. Ce constat ne change pas en période de crise\.

Enfin, concernant l’impact prix, le recours au contrat de liquidité permet de diminuer l’impact\-prix des transactions de l’ensemble des titres\. Cette diminution est plus marquée pour les titres non liquides\. Ce n’est en revanche plus le cas en période de crise\.

<a id="_bookmark10"></a>11 Un test augmenté de Dickey\-Fuller ainsi qu’un test KPSS ont permis de s’assurer de la stationarité des variables sur la periode étudiée\. De plus, des régressions séparées sur les deux sous\-périodes ont donné des résultats qualitatifs similaires\. La prise en compte de l'ensemble de la période permet de capter l'effet des contrats de liquidité pendant la crise\.

__Tableau 8 : Impact des contrats de liquidité sur la liquidité des titres	__

Titres non liquides	Titres Liquides	Titres très liquides

Quoted spread

Effective spread

Logdepth	Amihud	Quoted spread

Effective spread

Logdepth	Amihud	Quoted spread

Effective spread

Logdepth	Amihud

\(1\)	\(2\)	\(3\)	\(4\)	\(1\)	\(2\)	\(3\)	\(4\)	\(1\)	\(2\)	\(3\)	\(4\)

__Prop\_CL	\-37\.4185\*\*\*  10\.1601\*\*  \-0\.2258\*\*\*    \-0\.0556\*\*\*	\-40\.0883\*\*\* \-13\.3635\*\*    \-0\.1788\*\*  \-0\.0048\*\*	\-13\.7336\*\*\*  27\.8124	0\.1157	\-0\.0005\*\*__

\(5\.2870\)	\(4\.4888\)	\(0\.0203\)	\(0\.0176\)	\(5\.4334\)	\(5\.4136\)	\(0\.0717\)	\(0\.0022\)	\(3\.9962\)	\(21\.7960\)   \(0\.3179\)	\(0\.0002\)

__CL\_actif x crise	\-2\.1687	2\.2734	\-0\.0109	0\.0150\*\*	\-1\.6567\*\*\*   \-0\.4504   \-0\.0417\*\*\*   \-0\.0002	0\.6871\*\*\*	\-0\.1424	0\.0040	0\.00001\*\*\*__

\(1\.8206\)	\(1\.5457\)	\(0\.0070\)	\(0\.0060\)	\(0\.3424\)	\(0\.3412\)	\(0\.0045\)	\(0\.0001\)	\(0\.0866\)	\(0\.4726\)	\(0\.0069\)  \(0\.000004\)

CL	\-223\.3099\*\*\* \-48\.7765\*\*\*   0\.3950\*\*\*     \-0\.0988\*\*	27\.8795\*\*\*   10\.9236\*\*     0\.1448\*\*   0\.0059\*\*\*	18\.2139\*\*\*		1\.8923	0\.2474\*\*   0\.0009\*\*\* \(12\.5946\)	\(10\.6932\)  \(0\.0484\)	\(0\.0418\)		\(4\.3607\)	\(4\.3449\)	\(0\.0576\)	\(0\.0018\)	\(1\.2118\)	\(6\.6094\)	\(0\.0964\)	\(0\.0001\)

Crise	56\.3638	62\.1145	0\.1693	\-0\.1812	1\.6630	6\.1317	0\.5758\*\*\*	\-0\.0036	\-5\.6897	49\.8808\*\*  \-0\.1307	\-0\.0003

\(70\.7894\)	\(60\.1024\)  \(0\.2721\)	\(0\.2352\)	\(15\.7319\)  \(15\.6747\)   \(0\.2077\)	\(0\.0065\)	\(4\.1011\)	\(22\.3681\)   \(0\.3262\)	\(0\.0002\)

Niveau Prix	\-59\.0723\*\*\*	\-8\.8899   \-0\.4013\*\*\*   \-0\.0041	\-12\.0676\*\*\*   \-6\.3604\*\*\*    \-0\.5703\*\*\*    \-0\.0072\*\*\*	\-4\.8539\*\*\*	\-2\.6439   \-0\.7894\*\*\* \-0\.0008\*\*\* \(6\.8113\)	\(5\.7830\)	\(0\.0262\)	\(0\.0226\)		\(1\.5162\)	\(1\.5107\)	\(0\.0200\)	\(0\.0006\)	\(0\.5286\)	\(2\.8828\)	\(0\.0420\)   \(0\.00003\)

Log capi		6\.3180		10\.1274   \-0\.7656\*\*\*    \-0\.2085\*\*\*	\-27\.2199\*\*\*  \-8\.6579   \-0\.2363\*\*\*   \-0\.0052\*	\-40\.7075\*\*\*   \-8\.8939	0\.5241\*\*   \-0\.0026\*\*\* \(19\.7141\)	\(16\.7379\)  \(0\.0758\)	\(0\.0655\)		\(6\.5520\)	\(6\.5281\)	\(0\.0865\)	\(0\.0027\)	\(2\.8816\)	\(15\.7165\)   \(0\.2292\)	\(0\.0001\)

Log volumes	\-21\.0657\*\*\*   \-11\.2522\*\*\*   0\.1365\*\*\*    \-0\.0664\*\*\*	\-8\.2738\*\*\*  \-1\.1210\*\*  0\.1962\*\*\*  \-0\.0025\*\*\*	\-4\.4035\*\*\*	\-0\.3864	0\.1683\*\*\*  \-0\.0002\*\*\* \(1\.0281\)	\(0\.8729\)	\(0\.0040\)	\(0\.0034\)		\(0\.4862\)	\(0\.4844\)	\(0\.0064\)	\(0\.0002\)	\(0\.2195\)	\(1\.1970\)	\(0\.0175\)   \(0\.00001\)

Volatilité	720\.3944\*\*\*   466\.1572\*\*\* \-0\.8933\*\*\*     0\.5172\*\*\*	104\.2921\*\*\* 62\.9366\*\*\*   \-0\.5107\*\*\*     0\.0073\*\*\*	32\.7456\*\*\*   10\.2185   \-0\.6609\*\*\*  0\.0007\*\*\* \(13\.8530\)	\(11\.7616\)  \(0\.0532\)	\(0\.0460\)		\(4\.0491\)	\(4\.0344\)	\(0\.0535\)	\(0\.0017\)	\(1\.7027\)	\(9\.2868\)	\(0\.1354\)	\(0\.0001\)

Vcac	\-3\.1325	\-4\.5283	\-0\.0151	0\.0129	0\.4640	\-0\.2438   \-0\.0476\*\*\*	0\.0003	0\.4915\*	\-3\.3171\*\*	\-0\.0117	0\.00002

\(4\.9530\)	\(4\.2052\)	\(0\.0190\)	\(0\.0165\)	\(1\.1013\)	\(1\.0972\)	\(0\.0145\)	\(0\.0005\)	\(0\.2872\)	\(1\.5663\)	\(0\.0228\)   \(0\.00001\)

Constante	492\.3774\*\*\*   230\.2005\*\*\*   3\.6037\*\*\*      0\.5746\*\*	152\.5629\*\*\*   46\.2579\*     3\.7567\*\*\*   0\.0377\*\*\*	196\.2428\*\*\*   94\.6521	0\.6896	0\.0129\*\*\* \(80\.4391\)	\(68\.2952\)  \(0\.3092\)	\(0\.2672\)		\(26\.8550\)  \(26\.7573\)   \(0\.3546\)	\(0\.0110\)	\(12\.3188\)   \(67\.1881\)   \(0\.9799\)	\(0\.0006\)

Observations	23 450	23 450	23 450	23 450	11 856	11 856	11 856	11 856	4 458	4 458	4 458	4 458

R2	0\.6096	0\.3303	0\.8445	0\.2326	0\.8108	0\.4500	0\.9038	0\.4687	0\.8675	0\.0511	0\.9391	0\.7625

*Note: degré de significativité \*\*\*p\-value < 0\.001, \*\*p\-value < 0\.01, \*p\-value < 0\.05, \. p\-value< 0\.1*

*Source : reporting des transactions, calculs AMF*

Ainsi, les contrats de liquidité permettent d’améliorer les *spreads *affichés ainsi que l’impact prix des transactions de l’ensemble des titres\. On observe par ailleurs, pour les *spreads *affichés, une densification du carnet d’ordres des titres liquides et très liquides durant la crise\. Le bilan est plus nuancé concernant les *spreads *effectifs et négligeable sur la profondeur de marché\.

# ANALYSE	DES	DÉPASSEMENTS	DE	LIMITES	ET	ORIENTATIONS	DES EVOLUTIONS DE LA PRATIQUE DE MARCHE

Pour bénéficier du *safe harbour*, les interventions des intermédiaires financiers doivent respecter des limites d’intervention en termes de volume \(5\.1\) et de prix \(5\.2\) et les émetteurs peuvent allouer des ressources au contrat de liquidité jusqu’à certaines limites \(5\.3\)\. Ces limites sont différentes selon la liquidité des titres\.

## ANALYSE DES DEPASSEMENTS EN VOLUMES

- 
	- 
		1. __Rappel des règles sur les volumes__

Pour pouvoir bénéficier du *safe harbour *prévu par la pratique de marché admise de l’AMF, le gestionnaire du contrat de liquidité doit respecter des limites d’intervention quotidienne en volume dépendant du segment de liquidité de l’action\. Ces limites s’appliquent dès lors que les interventions cumulées du PSI à l’achat et à la vente dans le cadre du contrat de liquidité représentent plus de 20 k€ \(« la franchise »\)\.

Ainsi, les interventions quotidiennes du contrat de liquidité corrigées de la franchise ne doivent pas représenter, à l’achat comme à la vente, plus d’un certain pourcentage des montants moyens échangés au cours des 30 séances de bourse précédentes, ce pourcentage étant défini en fonction du degré de liquidité du titre\. Ces limites, telles qu’elles sont fixées dans une position de l’ESMA, s’élèvent respectivement à 25, 15 et 5 % pour les valeurs non liquides, liquides et très liquides\. Pour sa part, l’AMF a considéré en 2018 qu’un assouplissement de ces limites était justifié pour les valeurs non liquides et liquides et les a dès lors portées à respectivement 50 et 25 %[12](#_bookmark11)\. Cet assouplissement est cependant assorti d’une condition : lorsque les limites fixées par l’ESMA ne sont pas respectées, l’animateur de marché doit être en mesure de justifier les dépassements\. Au\-delà de la limite définie par l’AMF, le droit commun s’applique : l’émetteur et l’intermédiaire ne bénéficient pas de la sécurité juridique conférée par la pratique de marché admise\.

__Tableau 9 : Limites sur les volumes fixées par l’ESMA et l’AMF __\(en pourcentage du turnover moyen des 30 séances de bourse précédentes\)

__Segment__

__« Limite ESMA »__

__« Limite AMF »__

Actions non liquides

25 %

50 %

Actions liquides

15 %

25 %

Actions très liquides

5 %

5 %

*Sources : ESMA, AMF*

## Résultats

### 	Les dépassements des limites fixées par l’ESMA et l’AMF sur les volumes ne concernent qu’une minorité d’émetteurs et restent très épisodiques

Les deux tiers des valeurs bénéficiant d’un contrat de liquidité n’ont connu aucun dépassement des limites de l’ESMA au cours de la période sous revue\. Pour la quasi\-totalité des valeurs ayant connu au moins un dépassement, celui\-ci reste occasionnel : il se produit moins d’une fois par mois environ \(une

<a id="_bookmark11"></a>12 [Décision AMF n° 2018\-01 du 2 juillet 2018 sur l’Instauration des contrats de liquidité sur titres de capital au titre de pratique de marché](https://www.amf-france.org/sites/default/files/resource/Decision%20AMF%20ndeg%202018-01%20du%202%20juillet%202018%20%20Instauration%20des%20contrats%20de%20liquidite%20sur%20titres%20de%20capital%20au%20titre%20de%20pratique%20de%20marche%20admise.pdf) [admise](https://www.amf-france.org/sites/default/files/resource/Decision%20AMF%20ndeg%202018-01%20du%202%20juillet%202018%20%20Instauration%20des%20contrats%20de%20liquidite%20sur%20titres%20de%20capital%20au%20titre%20de%20pratique%20de%20marche%20admise.pdf)

séance sur vingt\)\. Seules 3 % des sociétés dont les titres sont non liquides \(soit moins de 10 sociétés\) enregistrent des dépassements plus fréquents\.

Une minorité d’émetteurs, représentant moins de 10 % de l’échantillon, a dépassé au moins une fois les limites fixées par l’AMF\. Fait notable, très peu de valeurs non liquides et liquides sont concernées et, quand elles le sont, là encore, les fréquences de dépassement sont faibles \(inférieures à 5 % des séances de cotation\)\.

__Graphique 14 : Fréquence des dépassements des limites de l’ESMA selon le degré de liquidité des titres__

\(en % des séances de négociation\)

__100%__

__4%__

__5%__

__  __

__  __

__  __

__ESMA__

__AMF__

__ESMA__

__AMF	ESMA/AMF	ESMA__

__AMF__

__Non liquides	Liquides	Très liquides	Total__

__66%__

__64%__

__69%__

__71%__

__13%__

__8%__

__14%__

__14%__

__91%__

__ 	__

__92%__

__92%__

__17%__

__4%__

__5%__

__3%__

__18%__

__14%__

__3%__

__5%__

__1%__

__5%__

__2%__

__19%__

__90%__

__80%__

__70%__

__60%__

__50%__

__40%__

__30%__

__20%__

__10%__

__0%__

__    N=299	N=78	N=21	N=398 __

0	<1%	<5%	<10%	>=10%

*Source : reporting des transactions, calculs AMF*

### 	Les dépassements des limites de l’ESMA comme ceux de l’AMF sont très limités en nombre comme en volume

Les dépassements en volume des limites de l’ESMA \(dits « dépassements de niveau 1 »\) à l’achat ou à la vente sont au nombre de 420 sur l’ensemble de la période \(décembre 2019 \- mai 2020\), tous titres confondus, sur un total de plus de 85 000 observations[13](#_bookmark12), soit __0,5 %__\. Ces dépassements portent sur un volume total de 12 millions d’euros, soit __0,2 % __des montants échangés dans le cadre des contrats de liquidité \(4,9 milliards d’euros\) au cours de la période\.

En nombre, ces dépassements sont essentiellement observés sur les titres non liquides \(75%\) puis, dans une moindre mesure, sur les titres liquides \(22%\)\. Les titres très liquides ne représentent que 3% des dépassements\. En valeur, la répartition est en revanche plus équilibrée puisque chaque classe de liquidité totalise un tiers des montants en dépassement sur l’ensemble de la période\.

<a id="_bookmark12"></a>13 Une observation correspond ici à la synthèse quotidienne par société des transactions enregistrée à l’achat ou à la vente dans le cadre des contrats de liquidité\.

__Graphique 15 : Nombre de dépassements des limites de volumes__

__Graphique 16 : Dépassement des limites de volumes en montants__

\(millions €\)

1,8

Millions

1,6

1,4

1,2

1

0,8

0,6

0,4

0,2

0

100%

90%

%

35%

34%

9%

2

Non liquides

Liquides

Très liquides

déc\.\- janv\.\- févr\.\- mars\- avr\.\- mai\- déc\.\- déc\.\- janv\.\- févr\.\- mars\- avr\.\-  mai\- janv\.\- févr\.\- mars\- avr\.\-  mai\-

19   20   20   20   20   20   19   19   20   20   20   20   20   20   20   20   20   20

2%  4%

7

7%

%

2

%

1%

1%

26

2

35%

45

58%

46%

61%

36%

43%

91%

80%

70%

60%

50%

40%

30%

20%

10%

0%

*Source : reporting des transactions, calculs AMF*

Niveau 1 \- achat	Niveau 2\- achat	Niveau 1 \- vente	Niveau 2\-vente	Part dans les dépassements

par niveau de liquidité \(EdD\)

Parmi ces 420 observations, un peu moins de 366, pour un volume de transactions de 7,5 millions d’euros, dépassent la limite de l’ESMA alors qu’elles sont toujours considérées comme bénéficiant *a priori *du *safe harbour *par l’AMF\. 54 dépassements, pour un montant total de 4,5 millions d’euros, se font au\-delà des limites fixées par l’AMF \(dits « dépassements de niveau 2 »\)\.

Il apparaît également que, pour les valeurs liquides et très liquides, les dépassements sont majoritairement liés à des interventions vendeuses\. Cette prépondérance des dépassements à la vente pour les valeurs liquides et très liquides pourrait s’expliquer par des rééquilibrages des ressources entre les espèces et les titres du compte de liquidité, les apporteurs de liquidité souhaitant pouvoir faire face à d’éventuelles et futures pressions vendeuses\.

En tout état de cause, au regard des montants totaux échangés sur contrats de liquidité, les montants en dépassement apparaissent très faibles, quel que soit le degré de liquidité des titres\. Pour les valeurs non liquides, la franchise représente plus de 60 % des volumes échangés, reflétant les faibles montants échangés dans le cadre des contrats de liquidité sur ces valeurs\. À l’inverse, cette part est plus faible pour les valeurs plus liquides\.

__Graphique 17 : Répartition des montants échangés dans le cadre des contrats de liquidité par degré de liquidité__

100%     	

90%     	

80%     	

70%     	

60%     	

50%     	

40%     	

30%     	

20%     	

10%     	

0%				 NOT LIQUID	LIQUID	VERY LIQUID

Franchise	Pas de dépassement	Depassement niveau 1	Depassement niveau 2

*Source : reporting des transactions, calculs AMF*

### 	Pistes d’évolution des seuils de volume

S’agissant des limites de volume, deux pistes peuvent être envisagées : d’une part, une modification de la franchise ; d’autre part, une évolution des limites\.

__Impact d’une variation de la franchise sur la part des montants actuellement en dépassement au regard des__

__« limites ESMA » \(niveau 1\) et des « limites AMF » \(niveau 2\)__

__Graphique 18 : Évolution du nombre de dépassements de limites en fonction du montant de franchise__

\(Base 0 = franchise actuelle à 20 000€\)

*Note de lecture : un relèvement de la franchise à 25 000 € permettrait de réduire le nombre de dépassements de 41% pour les titres non liquides, de 16% pour les titres liquides et de 8% pour les titres très liquides*

*Source : reporting des transactions, calculs AMF*

__*Impact d’une évolution des limites sur les volumes sur le nombre de dépassement au regard des « limites ESMA » \(niveau 1\)*__

__Graphique 19 : Distribution des écarts à la limite de__

__volumes ESMA – achats __\(% des montants échangés\)



__Graphique 20 : Distribution des écarts à la limite de__

__volumes ESMA – ventes __\(% des montants échangés\)



Degré de Liquidité       Q1	Q2	Q3	Q4	Q5	Q6	Q7	Q8	Q9

Liquidity\_status	Q1	Q2	Q3	Q4	Q5	Q6	Q7	Q8	Q9

NOT LIQUID	1,2%       2,7%       3,8%       4,9%       6,9%       9,1%       12,7%        19,1%        27,3%

LIQUID	0,4%       0,8%       0,9%       1,2%       2,3%       3,2%	4,5%	5,4%	7,4%

VERY LIQUID	0,4%       0,6%       0,7%       0,8%       0,9%       1,1%	1,2%	1,4%	1,5%

NOT LIQUID	1,1%      2,4%      4,3%      5,4%      7,3%      9,8%     13,5%   18,6%   27,8%

LIQUID	0,1%      0,5%      1,1%      1,8%      2,6%      3,0%      4,7%      6,7%      9,0%

VERY LIQUID	0,0%      0,0%      0,0%      0,1%      0,3%      0,4%      1,0%      2,1%      3,0%

*Note de lecture : une augmentation de la limite ESMA de 15 points pour les titres non liquides \(i\.e\. une limite de volumes portée à 40%\) ermettrait de réduire les montants échangés en dépassement de 75% pour ces titres\.*

*p*

*Source : reporting des transactions, calculs AMF*

__Graphiques 21 \- 22 \- 23 : Impact d’une évolution de la franchise conjuguée à une évolution des limites en volume sur le nombre de dépassement au regard des « limites ESMA » \(niveau 1\) par segment de liquidité \(non liquides – liquides – très liquides\)__

0%

30%

25%

20%

15%

Limite à

Limite à

Limite à

Limite à

\-10%

\-20%

\-30%

\-40%

\-50%

\-60%

\-70%

\-80%

\-90%

\-100%

Franchise de 20 000 €

Franchise de 30 000 €

Franchise de 25 000 €

Franchise de 35 000 €



*Note de lecture : une augmentation de la franchise de 25 000 € conjuguée à une augmentation de la limite ESMA à 30% pour les titres non liquides permettrait de réduire le nombre de dépassement de 64%\.*

*Source : reporting des transactions, calculs AMF*

L’étude montre que les limites d’intervention en volume telles que fixées dans la décision actuellement en vigueur n’ont pas entravé la mise en œuvre des contrats de liquidité portant sur les valeurs des segments liquides et très liquides\. __Toutefois, une augmentation de 5 points \(de 15 à 20%\) de la limite inférieure relative aux actions liquides permettrait de capturer 80% des dépassements observés\. __S’agissant de la limite réservée aux actions très liquides, le bénéfice d’un relèvement de cette limite paraît peu évident\.

En revanche, les constats de l’étude concernant les dépassements sur les titres non liquides sont sensiblement différents dans la mesure où la limite de 25% devrait être portée à 50% pour capturer 80% des dépassements observés\. Il a également été constaté que la franchise avait un impact significatif sur les actions de ce segment « en absorbant » plus de 60% des volumes échangés \(l’impact de la franchise sur les titres liquides et très liquides étant beaucoup plus faible\)\.

__Un relèvement léger de la franchise de 20 000 € à 25 000 € conjugué à une augmentation de la limite en pourcentage du turnover de 25% à 30% permettrait de capturer les deux tiers des dépassements pour les titres non liquides\.__

## ANALYSE DES DÉPASSEMENTS EN PRIX

- 
	- 
		1. __Rappel des règles sur les prix__

Outre des conditions sur les volumes de transaction, la décision AMF 2018\-01 prévoit également des restrictions en termes de limite de prix des ordres pour bénéficier du *safe harbour*\. Quand elles concernent un ordre d’achat, ces conditions sont comparables à celles gouvernant les interventions d’un émetteur mettant en œuvre un programme de rachat\. La limite de prix des ordres d’achat ne doit pas être supérieure à la plus élevée des deux valeurs suivantes : le prix de la dernière opération indépendante ou l'offre d'achat indépendante actuelle la plus élevée\. Un raisonnement comparable est applicable aux ordres de vente\.

Concrètement, voici ce que cela signifie lorsqu’un participant de marché indépendant émet un ordre de vente « au marché » portant sur 30 titres alors que la configuration de marché est la suivante[14 ](#_bookmark13):

__Achat__

__Vente__

__Acheteur__

__Quantité__

__limite de prix__

__limite de prix__

__Quantité__

__Vendeur__

Animateur

20

99

100

20

Animateur

Indépendant

10

96

Indépendant

Indépendant

50

92

Indépendant

L’ordre de vente \(30 titres vendus au cours moyen de 98€\) s’apparie avec l’ordre d’achat de l’animateur pour 20 titres à 99€, puis avec l’ordre d’achat d’un autre participant de marché \(indépendant\) pour 10 titres à 96€\. En conséquence, la dernière transaction indépendante a été effectuée à 96€ et la meilleure limite indépendante à l’achat devient 92€\. Ainsi, l’animateur peut repositionner un ordre d’achat à 96€ et la fourchette des meilleures limites deviendrait 96\-100\.

Au\-delà de ce que préconisait l’ESMA dans ses points de convergence, l’AMF a assoupli cette règle en prévoyant la faculté pour l’animateur de resserrer le *spread*, sauf pour les actions très liquides : la limite de prix peut être plus compétitive que la limite mentionnée au paragraphe précédent à condition que l’ordre correspondant se positionne entre la meilleure offre de prix à l’achat et la meilleure offre de prix à la vente\. Aussi, l’animateur peut repositionner un ordre d’achat dont la limite de prix est supérieure à 96€ et strictement inférieure à 100€ s’il est en mesure d’apporter la justification l’ayant conduit à considérer que le recours à cette faculté était nécessaire à la mise en œuvre du contrat et n’a pas altéré le fonctionnement du marché\.

Enfin, il convient de souligner qu’il est extrêmement complexe d’isoler les seuls ordres dont la limite de prix a resserré le *spread *tout en étant égale au prix de la dernière transaction indépendante\. Un tel exercice suppose de reconstituer le carnet d’ordres au moment de l’intégration de chaque ordre du contrat de liquidité ayant resserré le *spread *afin de déterminer si sa limite de prix est égale ou non au prix de la dernière transaction indépendante\. En conséquence, l’analyse considère l’ensemble des ordres passés ayant resserré le spread par l’animateur de marché, comme le prévoit la pratique de marché AMF\.

<a id="_bookmark13"></a>14 Pour simplifier l’exposé, on suppose qu’il n’y a pas de mécanisme coupe\-circuit et que le pas de cotation est égal à 1€\.

## Résultats

Afin d’appréhender les dépassements de limites de prix, l’étude a porté sur les ordres positionnés pendant la phase de marché continu entre la meilleure offre de prix à l’achat et la meilleure offre de prix à la vente et leur impact sur le marché\.

Un ordre entré en carnet peut engendrer une exécution instantanée, auquel cas l’ordre est dit agressif puisqu’il consomme la liquidité\. *A contrario*, un ordre passif n’engendre pas d’exécution au moment de son intégration dans le carnet d’ordres et vient donc augmenter la liquidité disponible en carnet\.

__Graphique 24 : Dépassements de prix selon le degré de liquidité des titres __\(parts des montants échangés\)

90%

80%

70%

60%

50%

40%

30%

20%

10%

0%

NOT LIQUID	LIQUID	VERY LIQUID	Total

Part des dépassements dans les CL     Part des dépassements hors contrats de liquidité

*Source : reporting des transactions, calculs AMF*

Les dépassements de limites de prix sont assez stables quelle que soit le segment de liquidité et représentent 27 % des montants échangés : un quart pour les titres liquides, un tiers pour les titres non liquides et 28% pour les titres très liquides\.

Il conviendra, dans cette analyse de prendre en compte le fait que le prestataire a toutefois pu, de bonne foi, introduire un ordre, passif lors de sa saisie \(i\.e\. apportant de la liquidité en carnet\), mais qui, du fait d’un décalage de prix intervenu entre\-temps[15](#_bookmark14), se révèle agressif \(i\.e\. consommateur de liquidité\) une fois qu’il atteint le carnet\. Aussi nous classons les ordres dans les catégories suivantes :

- agressif : lorsque l‘ordre est totalement agressif et qu’il consomme donc la liquidité en étant exécuté entièrement au moment de son intégration dans le carnet central ;
- agressif\-passif : lorsqu’il est agressif pour partie mais que le reliquat vient alimenter la file d’attente des ordres non exécutés le carnet central ;
- passif : lorsque l’ordre est entièrement passif et vient apporter de la liquidité en carnet\.

De plus, certains ordres peuvent également être exécutés pour partie ou totalement au *fixing*\. Cet effet est capté à travers les catégories : *fixing*, agressif\-*fixing*[16](#_bookmark15), passif\-*fixing*, agressif\-passif\-*fixing*\.

<a id="_bookmark14"></a>15 Entre le moment de l’émission de l’ordre par l’apporteur de liquidité et le moment d’intégration de l’ordre dans le carnet central par l’entreprise de marché\.

<a id="_bookmark15"></a>16 On notera toutefois que la catégorie « agressif\-fixing » est quasi nulle que l’on considère l’activité liée aux contrats de liquidité ou non\.

De fait, s’agissant des valeurs liquides et non liquides, environ 60 % des dépassements en montant concernent des ordres entièrement passifs, i\.e\. apportant de la liquidité en carnet\. Les valeurs liquides se caractérisent en outre par une proportion significative d’ordres partiellement passifs \(agressifs\- passifs\)\. Au total, les ordres purement agressifs, venant consommer de la liquidité, représentent en montant moins de 20 % des dépassements pour les valeurs non liquides et moins de 10 % pour les valeurs liquides\. Rapportée à l’ensemble des montants des ordres passés dans le cadre des contrats de liquidité, cette proportion devient même négligeable pour les valeurs non liquides \(inférieure à 2 %\)\.

S’agissant des valeurs très liquides, un peu plus de la moitié des dépassements résulte d’ordres partiellement agressifs \(53%\) et, pour 39%, d’ordres purement passifs\. À noter que la part des ordres agressifs est similaire à celle des valeurs liquides mais, par rapport au total des montants échangés dans le cadre des contrats de liquidité, elle apparaît négligeable \(de l’ordre de 2 %\)\. Ainsi, il apparaît probable que, pour les titres très liquides, les ordres en dépassement passés l’aient été avec une volonté d’améliorer les fourchettes affichées plutôt que d’obtenir une exécution immédiate\.

__Graphique 25 : Dépassements des limites de prix__

__observés dans le cadre des contrats de liquidité__

\(en proportion des montants en dépassement\)

__Graphique 26 : Dépassements des limites de prix__

__observés dans le cadre des contrats de liquidité__

\(en proportion des montants totaux échangés dans

le cadre du contrat de liquidité\)

	

*Source : reporting des transactions, calculs AMF*

En comparaison, le poids des ordres agressifs dans les volumes des dépassements en dehors du contrat de liquidité apparaît beaucoup plus élevé : elle varie entre 40 % et 50 % du montant des dépassements observés et entre 30 et 40 % du total des montants échangés selon la classe de liquidité considérée\.

__Graphique 27 : Dépassements les limites de prix__

__observés hors contrats de liquidité__

\(en proportion des montants en dépassements\)

__Graphique 28 : Dépassements des limites de prix :__

__Répartition des ordres hors contrat de liquidité__

\(en proportion des montants échangés\)



*Source : reporting des transactions, calculs AMF*

## ANALYSE DES RESSOURCES

À titre liminaire, l’analyse ci\-après s’appuie sur la valorisation du portefeuille des comptes de liquidité \(espèces et titres\) au 31 décembre des années 2019 et 2020\. Aussi, cette estimation de la valorisation ne reflète pas *stricto sensu *le montant alloué par les émetteurs au moment de l’allocation puisque, d’une part, les opérations du gestionnaire de contrat de liquidité ont engendré des plus ou moins\-values et, d’autre part, la valeur du portefeuille est étroitement liée au cours des titres détenus en inventaire\. En conséquence, le montant des ressources tel qu’il ressort des données fournies par les prestataires de services d’investissement peut parfois excéder le montant maximal de ressources fixé par la décision de l’AMF alors que les émetteurs, en lien avec les prestataires, avaient pris les dispositions nécessaires pour allouer un montant initial respectant la Décision de l’AMF\.

À cet égard, la Décision de l’AMF prévoit que les limites de ressources sont appréciées sur la base des données de marché à la date de la signature du contrat et sont réexaminées lors de l’échéance du contrat et de sa reconduction\. Lorsque les ressources doivent être diminuées, le réajustement peut être réalisé dans un délai n’excédant pas 6 mois suivant la reconduction du contrat\.

Il sera également signalé qu’il ressort des entretiens informels avec les gestionnaires de contrat que les limites de ressources fixées par la Décision de l’AMF n’ont pas été contraignantes au point d’entraver la bonne mise en œuvre des contrats\. Un établissement a toutefois indiqué que le plafond fixé en valeur absolue pouvait parfois être un peu juste quand le contrat porte sur une action figurant parmi les plus liquides du segment des actions non liquides\. D’ailleurs, il a été observé que le segment des actions non liquides est beaucoup plus hétérogène que celui des actions liquides et très liquides\. Ainsi, le ratio entre la plus importante et la plus faible capitalisation boursière des actions du segment non liquide au 31 décembre 2019 est d’environ 1 700 contre 97 pour les actions du segment liquide et 25 pour celles du segment non liquide\. En termes de *turnover *moyen calculé sur les 30 séances de bourse précédant le 31 décembre 2019, la différence est encore plus marquée puisque les ratios correspondants sont respectivement d’environ 8 100, 98 et 8,5\.

Enfin, il convient de rappeler que la révision du classement des actions entre les segments liquides et non liquides s’appuie sur les données de marché de l’année *n\-1 *et n’est publiée qu’au mois d’avril de l’année *n*\.

## Segment des actions non liquides

Pour mémoire, la Décision en vigueur prévoit que les ressources allouées par un émetteur du segment non liquide à un contrat de liquidité ne peuvent dépasser :

__Variable__

__Plafond « relatif »__

__Plafond « absolu »__

*Turnover *moyen sur 30 séances de bourse ou

750%

Entre 500% et 750% justifié et documenté

Dans tous les cas, le montant des ressources ne doit pas excéder 3m€ Lorsqu’il est compris entre 1 et 3m€, il est justifié et documenté

Capitalisation boursière

1,50%

Entre 1% et 1,5% justifié et documenté

En s’appuyant sur les données recueillies auprès des 10 prestataires de services d’investissement gérant des contrats de liquidité portant sur des actions non liquides \(283 contrats\), l’analyse statistique des avoirs inscrits sur le compte de liquidité des émetteurs[17 ](#_bookmark16)fait ressortir :

__Variable__

__Nb__

__Moyenne__

__Mini__

__Perc5__

__Perc10__

__Quart1__

__Médiane__

__Quart3__

__Perc85__

__Perc90__

__Perc95__

__Maxi__

__Enveloppe 31/12/19 \(K€\)__

283

370,6

2,0

30,5

39,5

85,3

203,2

435,6

753,9

964,0

1 192,8

3047,4

__Enveloppe 31/12/20 \(K€\)__

283

376,0

0,4

34,7

48,6

97,0

220,2

451,7

682,3

927,3

1 170,5

2 703,7

__Market cap 31/12/2019 \(M€\)__

283

298,4

2,7

5,7

10,2

27,4

80,7

278,5

533,1

763,6

1 101,0

4 597,8

__Market cap 31/12/2020 \(M€\)__

283

303,8

2,6

9,0

16,6

36,2

99,0

343,1

530,0

717,0

1 190,8

5 128,2

__Moving ATO 31/12/19 \(K€\)__

278

126,0

0,2

2,5

6,0

14,8

51,4

169,1

251,3

328,0

441,4

1 431,3

__Moving ATO 31/12/20 \(K€\)__

276

496,9

0,2

3,7

7,9

32,7

101,3

337,0

592,6

918,9

2 183,9

15 375,9

__Average Daily Amount Traded 2019 \(K€\)__

283

93,0

0,4

3,3

5,4

12,9

44,3

115,9

193,3

258,8

334,2

912,6

__Average Daily Amount Traded 2020 \(K€\)__

283

300,6

0,3

3,9

7,9

22,7

68,2

234,4

385,2

481,5

915,4

18 964,3

__Ratio Enveloppe 2019/ADT 31/12/19 \(%\)__

278

1 725,3%

2,8%

31,7%

54,9%

159,5%

415,6%

1 174,7%

1 926,5%

3 119,0%

5 223,9%

134 381,2%

__Ratio Enveloppe 2019/ADT 2020 \(%\)__

276

1 328,9%

0,2%

7,0%

16,5%

66,1%

210,4%

821,0%

1 715,3%

2 781,5%

4 896,6%

55 517,4%

__Ratio Enveloppe 2019/ Market Cap 2019 \(%\)__

283

0,3%

0,0%

0,0%

0,1%

0,1%

0,2%

0,5%

0,6%

0,8%

1,0%

2,3%

Il apparaît que :

- 90% des contrats disposent d’une enveloppe n’excédant pas 1 million d’euros ;
- 95% des contrats disposent d’une enveloppe n’excédant pas 1% de la capitalisation boursière de l’émetteur \(au 31/12/2019\) ;
- environ 57% des contrats disposent d’une enveloppe excédant 5 fois le *turnover *moyen observé au 31/12/19 \(contre 35% au 31/12/20\) ;
- environ 33% des contrats disposent d’une enveloppe excédant 7,5 fois le *turnover *moyen observé au 31/12/19 \(contre 27% au 31/12/20\)\.

Ainsi, la limite « *turnover *moyen » fixée dans la Décision ne semble pas complètement pertinente dans la mesure où la limite en pourcentage de la capitalisation boursière donne beaucoup plus de flexibilité\. En d’autres termes, la limite « *turnover *moyen » n’est pas mordante tant que la limite de ressources peut s’appuyer sur un pourcentage de la capitalisation boursière\. À cet égard, on constate que les enveloppes sont bien corrélées avec le critère « capitalisation boursière » comme le montre le graphique suivant :

<a id="_bookmark16"></a>17 Il s’agit ici de la somme de la contrevaleur des titres inscrits sur le compte de liquidité à l’issue de la séance de bourse du 31 décembre et du cash disponible sur le compte espèces associé au compte titres\.

__Graphique 29 : Actions non liquides :__

Enveloppe au 31/12/2019 vs Market Cap 31/12/19

10 000 000,00

1 000 000,00

SOFRAGI

PATRIMOINE ET COMMERCE

FONCIERE INEA

CR CA TOULOUSE 31

CR CA ILLE ET VILAINE   COLAS ID LOGISTICS GROUP

SOCIETE FONCIERE LYONNAISE

TIKEHAU CAPITAL CR CA SUD RHONE ALPES

BOIRON

NRJ GROUP

Montant de l'enveloppe \(euros\)

100 000,00

10 000,00

1 000,00

1,00	10,00	100,00	1 000,00	10 000,00

*à noter :*

*Echelle logarithmique trait bleu : limite "ESMA" trait rouge : limite "AMF"*

*Source : Bloomberg, données PSI*

Market Cap au 31/12/19 \(millions d'euros\)

À l’inverse, la corrélation entre la taille de l’enveloppe et le *turnover *paraît moins évidente :

__Graphique 30 : Actions non liquides :__

Enveloppe au 31/12/2019 vs Ratio \(Enveloppe 2019/Moving average daily turnover au 31/12/19\)

10 000 000,00

ID LOGISTICS GROUP

FONCIERE INEA

COLAS

BOIRON

TIKEHAU CAPITAL

NRJ GROUP

SOCIETE FONCIERE LYONNAISE

CR CA TOULOUSE 31 CR CA ILLE ET VILAINE

CR CA SUD RHONE ALPES

SOFRAGI

PATRIMOINE ET COMMERCE

1 000 000,00

100 000,00

Montant de l'enveloppe \(euros\)

10 000,00

1 000,00

0,01	0,10	1,00	10,00	100,00	1 000,00	10 000,00

*à noter :*

*Echelle logarithmique trait bleu : limite "ESMA" trait rouge : limite "AMF"*

Ratio Enveloppe au 31/12/2019 / moving averade daily turnover au 31/12/19

*Source : Bloomberg, données PSI*

À cet égard, il peut être fait l’hypothèse que le *turnover *moyen sur les actions du segment non liquide est trop volatil pour servir d’assiette aux limites de ressources d’autant que celui\-ci est fixé au moment

de la signature du contrat puis revu chaque année[18](#_bookmark17)\. Ainsi, pour que ce critère puisse couvrir 90% des contrats, il faudrait porter la limite à 30 fois le turnover moyen alors que les points de convergence la fixent à 5, l’AMF l’ayant portée à 7,5 dans sa décision\.

S’agissant du plafond « absolu », l’AMF observe que seul un nombre limité de contrats dispose d’un montant excédant 1,5 ou même 2 millions d’euros\. En conséquence, il pourrait être pertinent d’abaisser le plafond actuellement fixé à 3 millions d’euros comme l’illustre le graphique ci\-après :

__Graphique 31 : Actions non liquides :__

Enveloppe au 31/12/2019 vs Ratio \(Enveloppe 2019/Market Cap 31/12/19\)

3 500 000,00

COLAS

CR CA TOULOUSE 31 SOCIETE FONCIERE LYONNAISE

ID LOGISTICS GROUP

CR CA ILLE ET VILAINE

CR CA SUD RHONE ALPES

NRJ GROUP    FONCIERE INEA

BOIRON

TIKEHAU CAPITA

L

SOFRAGI

PATRIMOINE ET COMMERCE

TURENNE INVESTISSEMENTS

ADUX THERANEXUS

3 000 000,00

2 500 000,00

2 000 000,00

Enveloppe au 31/12/2019

1 500 000,00

1 000 000,00

500 000,00

0,00

0,00%

0,50%

1,00%

1,50%

INTEGRAGEN 2,00%

*à noter :*

*Echelle linéaire*

*trait bleu : limite "ESMA" trait rouge : limite "AMF"*

*Source : Bloomberg, données PSI*

Ratio

\(Enveloppe 31/12/19 / Market Cap au 31/12/2019\)

## Segment des actions liquides

Pour mémoire, la décision en vigueur prévoit que les ressources allouées par un émetteur du segment très liquide ne peuvent dépasser :

__Variable__

__Plafond « relatif »__

__Plafond « absolu »__

*Turnover *moyen sur 30 séances de bourse

300%

Entre 200% et 300% justifié et documenté

Dans tous les cas, le montant des ressources ne doit pas excéder 30m€

Lorsqu’il est compris entre 20 et 30m€, il est justifié et documenté

En s’appuyant sur les données recueillies auprès des 7 prestataires de services d’investissement gérant des contrats de liquidité portant sur des actions liquides \(76 contrats\), l’analyse statistique des avoirs inscrits sur le compte de liquidité des émetteurs fait ressortir :

<a id="_bookmark17"></a>18 D’autant que le marché de certaines actions peut très sensiblement changer de configuration au cours d’une année\. À titre d’illustration, le *turnover *moyen en 2019 de l’un des titres couvert par un contrat de liquidité s’établissait à 75k€\. En raison de la crise sanitaire, le cours de cette action s’est envolé et le turnover moyen a été porté à environ 3,5m€\.

__Variable__

__Nb__

__Moyenne__

__Mini__

__Perc5__

__Perc10__

__Quart1__

__Médiane__

__Quart3__

__Perc90__

__Perc95__

__Maxi__

__Enveloppe 31/12/19 \(K€\)__

76

6 775,5

229,2

612,5

1 061,8

1 795,2

3 782,4

10 687,5

16 493,9

19 206,0

31 278,7

__Enveloppe 31/12/20 \(K€\)__

76

6 923,4

152,8

675,4

959,3

1 930,5

3 954,9

10 534,2

16 927,5

20 955,0

30 220,8

__Market cap 31/12/2019 \(M€\)__

76

4 808,1

315,8

543,1

710,9

1 199,5

3 081,0

6 875,2

10 863,7

12 763,7

30 812,8

__Market cap 31/12/2020 \(M€\)__

76

4 696,6

123,9

226,6

471,2

1 058,1

2 868,3

6 994,3

9 904,9

11 959,1

39 973,5

__Moving ATO 31/12/19 \(K€\)__

76

8 105,0

346,9

507,6

601,3

1 310,6

5 609,1

12 288,3

20 483,9

22 390,1

34 067,0

__Moving ATO 31/12/20 \(K€\)__

76

7 404,5

210,5

455,7

681,5

1 435,9

4 200,7

11 071,7

17 375,8

23 466,2

33 097,8

__Average Daily Amount Traded 2019 \(K€\)__

76

7 947,5

381,2

567,0

664,9

1 377,3

5 357,8

11 228,6

21 334,4

24 163,7

32 942,0

__Average Daily Amount Traded 2020 \(K€\)__

76

8 257,1

187,8

368,2

761,4

1 298,3

4 630,4

11 251,4

20 120,8

28 771,3

37 997,2

__Ratio Enveloppe 2019/ADT 31/12/19 \(%\)__

76

131,5%

1,8%

22,3%

26,8%

47,8%

98,2%

191,9%

290,8%

328,1%

407,0%

__Ratio Enveloppe 2019/ADT 2020 \(%__

76

141,0%

6,5%

24,4%

31,1%

61,7%

116,6%

172,2%

288,6%

426,6%

558,3%

__Ratio Enveloppe 2019/ Market Cap 2019 \(%\)__

76

0,2%

0,0%

0,0%

0,1%

0,1%

0,1%

0,2%

0,4%

0,6%

0,7%

Ainsi, il apparaît que :

- 95% des contrats disposent d’une enveloppe n’excédant pas 20 millions d’euros ;
- 90% des contrats disposent d’une enveloppe n’excédant pas 3 fois le *turnover *moyen observé au 31/12/19 ;
- 75% des contrats disposent d’une enveloppe n’excédant pas 2 fois le *turnover *moyen observé au 31/12/19 ;
- alors que le segment des actions liquides est sensiblement plus homogène que celui des actions non liquides, le rapport entre la moyenne et la médiane des enveloppes est à un niveau comparable pour les 2 segments \(environ 1,8\)\. Ce constat suggère que le montant de l’enveloppe alloué par les émetteurs est plus dispersé par rapport aux besoins du contrat \(voir graphe ci\-après\)\.

__Graphique 32 : Actions liquides :__

Enveloppe au 31/12/2019 vs Ratio \(Enveloppe 2019/Moving Average daily turnover 31/12/19\)

35 000 000,00

ACCOR

SUEZ

CASINO

NATIXIS

EDF

GETLINK SE

SCOR SE LIQUIDITE

ADP REXEL

 	  CNP ASSURANCES

WENDEL INVESTISSEMENT

MERCIALYS

INTERPARFUMS VICAT

QUADIENT

ALBIOMA

ROTHSCHILD & CO

CHARGEURS LECTRA

30 000 000,00

25 000 000,00

20 000 000,00

Montant de l'enveloppe \(euros\)

15 000 000,00

10 000 000,00

5 000 000,00

*à noter : Echelle linéaire*

0,00

0,00	1,00	2,00	3,00	4,00	5,00

Ratio \(Enveloppe 2019 / Moving average daily turnover\)

*trait bleu : limite "ESMA" trait rouge : limite "AMF"*

*Source : Bloomberg, données PSI*

## Segment des actions très liquides

Pour mémoire, la décision en vigueur prévoit que les ressources allouées par un émetteur du segment très liquide ne peuvent dépasser :

__Variable__

__Plafond « relatif »__

__Plafond « absolu »__

*Turnover *moyen sur 30 séances de bourse

100%

Dans tous les cas, le montant des ressources ne doit pas excéder 50m€

Pour mémoire, l’AMF n’avait pas souhaité assouplir les dispositions applicables aux actions les plus liquides par rapport à celles posées par les points de convergence de l’ESMA\.

En s’appuyant sur les données recueillies auprès des 4 prestataires de services d’investissement gérant des contrats de liquidité portant sur des actions très liquides \(20 contrats\), l’analyse statistique des avoirs inscrits sur le compte de liquidité des émetteurs fait ressortir les données suivantes :

__Variable__

__Nb__

__Moyenne__

__Mini__

__Perc5__

__Perc10__

__Quart1__

__Médiane__

__Quart3__

__Perc90__

__Perc95__

__Maxi__

__Enveloppe 31/12/19 \(K€\)__

20

25 608,9

4 191,3

4 779,7

4 959,9

8 237,5

19 936,9

50 066,8

50 471,7

51 058,2

55 578,5

__Enveloppe 31/12/20 \(K€\)__

20

26 453,3

4 513,4

4 942,4

5 227,9

13 976,9

21 396,9

46 670,2

50 364,9

50 736,9

55 737,1

__Market cap 31/12/2019 \(M€\)__

20

40 728,4

8 116,9

9 624,6

11 356,0

13 972,7

30 683,6

46 432,3

70 687,2

80 671,2

209 349,6

__Market cap 31/12/2020 \(M€\)__

20

43 154,9

8 225,3

10 004,6

11 430,0

15 581,7

23 983,7

45 408,6

76 840,3

101 109,9

257 880,5

__Moving ATO 31/12/19 \(K€\)__

20

67 382,5

23 433,5

25 854,2

28 224,5

36 531,5

52 400,0

102 026,5

108 319,5

119 148,2

198 054,4

__Moving ATO 31/12/20 \(K€\)__

20

69 954,6

28 945,4

30 706,2

30 865,2

38 531,6

53 547,5

83 979,3

111 113,9

116 945,3

225 950,9

__Average Daily Amount Traded 2019 \(K€\)__

20

66 635,1

20 716,9

22 342,5

31 948,8

35 441,3

55 318,7

92 626,3

115 581,6

127 515,7

202 182,2

__Average Daily Amount Traded 2020 \(K€\)__

20

77 502,7

30 694,5

32 462,3

32 752,3

43 710,5

56 576,0

99 708,2

122 250,3

139 687,4

249 986,3

__Ratio Enveloppe 2019/ADT 31/12/19 \(%\)__

20

41,2%

4,7%

7,7%

8,5%

19,1%

45,7%

56,4%

66,5%

72,4%

80,4%

__Ratio Enveloppe 2019/ADT 2020 \(%\)__

20

44,0%

4,8%

6,6%

9,2%

22,3%

43,3%

63,6%

72,5%

80,1%

95,7%

__Ratio Enveloppe 2019/ Market Cap 2019 \(%\)__

20

0,1%

0,0%

0,0%

0,0%

0,0%

0,1%

0,1%

0,2%

0,2%

0,2%

Ainsi, l’AMF observe que,

- 30% des contrats disposent d’une enveloppe atteignant le plafond de ressources \(50 millions d’euros\) ;
- 70% des contrats disposent d’une enveloppe représentant environ 70% le turnover moyen observé au cours de l’année 2020\.

Ce constat est illustré par le graphique ci\-après :

__Graphique 33 : Actions très liquides :__

Enveloppe au 31/12/2019 vs Moving Average daily turnover 31/12/19

60 000 000,00

SAFRAN

LVMH

ORANGE       KERING

ENGIE

CREDIT AGRICOLE SA

THALES

DASSAULT SYSTEMES

CAPGEMINI

HERMES INTERNATIONAL

BOUYGUES

ATOS

AIR LIQUIDE

PUBLICIS GROUPE SA

SOCIETE GENERALE

COMPAGNIE DE ST\-GOBAIN

TELEPERFORMANCE

WORLDLINE

PERNOD RICARD

VEOLIA ENVIRONNEMENT

50 000 000,00

40 000 000,00

Montant de l'enveloppe \(euros\)

30 000 000,00

20 000 000,00

10 000 000,00

0,00

0,00	0,10	0,20	0,30	0,40	0,50	0,60	0,70	0,80	0,90	1,00

*A noter :*

*Echelle linéaire*

*Trait rouge : limite AMF\-ESMA*

*Source : Bloomberg, données PSI*

ratio \(Enveloppe / Moving Average daily turnover 31/12/2020

__BIBLIOGRAPHIE__

Amihud Y\. \(2002\) : “Illiquidity and stock returns: cross\-section and time\-series effects”, *Journal of Financial markets, 5\(1\), pp\. *31\-56\.

Anand A\., C\. Tanggaard et D\.G\. Weaver \(2009\): “Paying for Market Quality”, *Journal of Financial and Quantitative Analysis, Volume 44 \(06\), pp\. 1427\-1457*

Fong K\., C\.W\. Holden, et C\.A\. Trzcinka \(2017\): “What are the best liquidity proxies for global research?”,

*Review of Finance 21 \(4\), 1355\-1401\.*

Menkveld A\. et T\. Wang \(2013\) *: “How do designated market makers create value for small\-caps?”, Journal of Financial Markets, *Volume 16, Issue 3, August, Pages 571\-603

Venkatamaran K\. et A\.C\. Waisburd \(2007\) : *“The Value of the Designated Market Maker”, Journal of Financial and Quantitative Analysis, Volume 42, Issue 3, September, pp\. 735\-758*

