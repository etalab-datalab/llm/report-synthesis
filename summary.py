import os
import mammoth
from html_parser import enlever_tableaux_et_balises, extraire_titres_sous_titres_texte_entier, extraire_titres_sous_titres_texte_entier_paragraphe
import re
import pandas as pd
import tiktoken
from openai import OpenAI

from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import DataFrameLoader
from langchain.embeddings import HuggingFaceEmbeddings
from llama_index.embeddings.langchain import LangchainEmbedding
from RAG_preprocess import split_text_into_chunks, process_dataframe, process_docx_files, convert_docx_to_md
from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain.chains import (
    StuffDocumentsChain, LLMChain, ConversationalRetrievalChain
)
from langchain_community.vectorstores import Chroma
from langchain_core.prompts import PromptTemplate
from dataframe_management import concate_chapter
from RAG_inference import RAG_prompt_generator, generate_RAG, dossier_docx_summary_RAG
from summary_inference import prompt_generator, generate_summary , one_level_summary, iterative_summary_reduction
from langchain_community.vectorstores import FAISS
from langchain_community.llms import LlamaCpp
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler

from langchain_openai import ChatOpenAI
from langchain.prompts import ChatPromptTemplate
from langchain.chains.summarize import load_summarize_chain
from langchain import PromptTemplate
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain.schema.output_parser import StrOutputParser


import os
import pandas as pd

#### DATASET PREPARATION ####


# Fonction pour définir les chapitres en fonction du nom du dossier (vous pouvez personnaliser cette fonction)
def definir_chapitres(nom_dossier):
    # Exemple de définition des chapitres
    chapitres = {
        '2023_Etude Retail_VF_FR': ['Introduction et Périmètre de l’étude', 'Analyse de la population d’investisseurs français actifs et leurs transactions effectuées en 2022', 'Portraits types'],
        '202005_etude_internalisateurs_fr': ['Introduction et Périmètre de l’étude', 'POIDS DES SI DANS LE MARCHE', 'PANORAMA DES ACTEURS', 'APPORT EN TERMES DE TRANSPARENCE PRE-NEGOCIATION', 'AMELIORATION DES PRIX OFFERTS PAR RAPPORT A EURONEXT','RESPECT DU REGIME TICK SIZE A VENIR','QUALITE DES PRIX OFFERTS DU POINT DE VUE DU CLIENT'],
        '202104-amf-impact-contrats-de-liquidite-_vf_0': ['Introduction et Périmètre de l’étude', 'Marché des Contrats de Liquidité', 'Impact des Interventions des PSI', 'Analyse Statistique et Économétrique', 'Conditions et Réglementation des Contrats de Liquidité'],
        '20211129-etude-a-publier-version-finale-fr': ['Introduction et Périmètre de l’étude', 'Activité des Investisseurs Particuliers Pendant la Crise COVID-19', 'Émergence des Neo-brokers','Profils et Comportements des Investisseurs','Limitations du Reporting Actuel et Recommandations'],
        'Analyse_facteurs_risques_202301_FR': ['Introduction et Périmètre de l’étude', 'Technologies de Traitement Automatique du Langage Naturel (NLP)', 'Perspectives et Applications Futures','Promotion des Formats « Machine-Readable »','Conclusion'],

        # Ajoutez d'autres définitions de chapitres selon vos besoins
    }
    return chapitres.get(nom_dossier, [])

def preparation_dataset(root_dir = 'AMF_txt', output_dir = 'dataframe_AMF.csv'):

    # Liste pour stocker les données
    data = []

    # Parcourir les dossiers dans le répertoire principal
    for folder_name in os.listdir(root_dir):
        folder_path = os.path.join(root_dir, folder_name)
        
        # Vérifier si c'est un dossier
        if os.path.isdir(folder_path):
            # Chemin des fichiers texte
            text_file_path = os.path.join(folder_path, 'text.txt')
            ground_truth_file_path = os.path.join(folder_path, 'ground_truth_summary.txt')
            
            # Lire les fichiers texte s'ils existent
            if os.path.isfile(text_file_path) and os.path.isfile(ground_truth_file_path):
                with open(text_file_path, 'r', encoding='utf-8') as text_file:
                    text_content = text_file.read()
                with open(ground_truth_file_path, 'r', encoding='utf-8') as ground_truth_file:
                    ground_truth_content = ground_truth_file.read()
                
                # Définir les chapitres pour le dossier actuel
                chapitres = definir_chapitres(folder_name)
                
                # Ajouter les données à la liste
                data.append({
                    'nom_document': folder_name,
                    'texte': text_content,
                    'ground_truth_summary': ground_truth_content,
                    'chapitres': chapitres
                })

    # Créer le DataFrame
    df = pd.DataFrame(data)

    # Définir 'nom_document' comme indice
    df.set_index('nom_document', inplace=True)

    # Afficher le DataFrame
    # print(df)
    df.to_csv(output_dir)
    return df


def doc_infos(df, nom_doc):
    raw = df.loc[nom_doc]
    chapter_list = raw['chapitres']
    print(raw.name)
    for chapitre in chapter_list:
        print(f" - {chapitre}")
    text = raw['texte']
    ground_truth_summ = raw['ground_truth_summary']

    return text, ground_truth_summ, chapter_list




#### EndPoint Prep ####

def set_model_endpoint(model_name):
    model_endpoints = {
        "meta-llama/Meta-Llama-3-8B-Instruct": ("http://llama38b.multivacplatform.org/v1", "multivac-FQ1cWX4DpshdhkXY2m"),
        "gradientai/Llama-3-8B-Instruct-262k": ("http://llama38b262k.multivacplatform.org/v1", "multivac-U8nH6PpNxvw2cXK9tM"),
        "mistralai/Mixtral-8x7B-Instruct-v0.1": ("http://mixtral8x7b.multivacplatform.org/v1", "multivac-4Q1cWX4DpshdhkXY2m"),
    }

    model_info = model_endpoints.get(model_name, "Model name not found")
    return model_name, model_info[0], model_info[1]
# set one of these models as the value to set_model_endpoint function



def client_openAI_init(my_model_name):
    # Set OpenAI's API key and API base to use vLLM's API server.



    model_name, openai_api_base, openai_api_key= set_model_endpoint(my_model_name)
    print(f"Model: {model_name}, Endpoint: {openai_api_base}, api_key: {openai_api_key}")

    client = OpenAI(
        api_key=openai_api_key,
        base_url=openai_api_base,
    )
    return client


#### TOC Proposition ####

def TOC_proposition( text , my_model_name = "gradientai/Llama-3-8B-Instruct-262k"):




    prompt_systeme = """Tu es un agent de l'administration française qui fait des synthèses de textes. Tu sais en particulier faire des plans de synthèses. Tu parles en français."""


    prompt_user_template = f"""Tu dois me faire un plan d'une synthèse du texte suivant : \n 
                        ```{text}``` \n 
                        Réponds en français.
                        Je veux un plan 4 parties. Donne moi les titres et détails succintement le contenu de chaque chapitre.
                        REPONDS EN FRANCAIS
                        """


    chat_messages = [
        {"role": "system", "content": prompt_systeme},
        {"role": "user", "content": prompt_user_template},
    ]


    # Mistral models don't have any system role messages
    if my_model_name == "mistralai/Mixtral-8x7B-Instruct-v0.1":
        chat_messages.pop(0)

    client = client_openAI_init(my_model_name)

    # stream chat.completions
    chat_response = client.chat.completions.create(
        model=my_model_name, # this must be the model name the was deployed to the API server
    #    stream=True,
        max_tokens=3000,
        top_p=0.9,
        temperature=0.2,
        messages=chat_messages
    )
    output = chat_response.choices[0].message.content
    print(output)
    return output


#### Summary with Llama3 262K only ####


def Summary_only( text , my_model_name = "gradientai/Llama-3-8B-Instruct-262k"):


    prompt_systeme = """Tu es un agent de l'administration française qui fait des synthèses de textes. Tu sais en particulier des synthèses dans un très bon français. """

    prompt_user_template = f"""Tu dois me faire un résumé structuré du texte suivant : \n 
                            ```{text}``` \n 

                            Je veux tire les principales informations du texte et que tu les retranscrivent de manière élocante.
                            Je veux que ton résumé ailles à l'essentiel. 
                            REPONDS EN FRANCAIS
                            """

    chat_messages = [
            {"role": "system", "content": prompt_systeme},
            {"role": "user", "content": prompt_user_template},
        ]


        # Mistral models don't have any system role messages
    if my_model_name == "mistralai/Mixtral-8x7B-Instruct-v0.1":
            chat_messages.pop(0)

    client = client_openAI_init(my_model_name)

        # stream chat.completions
    chat_response = client.chat.completions.create(
            model=my_model_name, # this must be the model name the was deployed to the API server
        #    stream=True,
            max_tokens=6000,
            top_p=0.9,
            temperature=0.2,
            messages=chat_messages
        )
    output = chat_response.choices[0].message.content
    print(output)
    return output



def Summary_with_toc( text ,plan, my_model_name = "gradientai/Llama-3-8B-Instruct-262k"):

    prompt_systeme = """Tu es un agent de l'administration française qui fait des synthèses de textes. Tu sais en particulier des synthèses dans un très bon français. """

    prompt_user_template = f"""Tu vas devoir faire un résumé de texte en respectant le plan suivant : 
                            ```{plan}``` \n
                                Tu dois me faire un résumé structuré du texte suivant : \n 
                            ```{text}``` \n 
                            \n

                            Je veux tire les principales informations du texte et que tu les retranscrivent de manière élocante.
                            Je veux que ton résumé ailles à l'essentiel. 
                            REPONDS EN FRANCAIS
                            """

    chat_messages = [
            {"role": "system", "content": prompt_systeme},
            {"role": "user", "content": prompt_user_template},
        ]


        # Mistral models don't have any system role messages
    if my_model_name == "mistralai/Mixtral-8x7B-Instruct-v0.1":
            chat_messages.pop(0)

    client = client_openAI_init(my_model_name)

        # stream chat.completions
    chat_response = client.chat.completions.create(
            model=my_model_name, # this must be the model name the was deployed to the API server
        #    stream=True,
            max_tokens=6000,
            top_p=0.9,
            temperature=0.2,
            messages=chat_messages
        )
    output = chat_response.choices[0].message.content
    print(output)
    return output


def Summary_with_theme( text ,theme, my_model_name = "gradientai/Llama-3-8B-Instruct-262k"):

    prompt_systeme = """Tu es un agent de l'administration française qui fait des synthèses de textes. Tu sais en particulier des synthèses dans un très bon français. """

    prompt_user_template = f"""Tu vas devoir faire un résumé de texte que je vais te donner. Tu devras en particulier me faire un chapitre sur le thème suivant : 
                            ```{theme}``` \n
                                Tu dois me faire un résumé structuré du texte suivant : \n 
                            ```{text}``` \n 
                            \n

                            Je veux tire les principales informations du texte et que tu les retranscrivent de manière élocante.
                            Je veux que ton résumé ailles à l'essentiel. 
                            REPONDS EN FRANCAIS
                            """

    chat_messages = [
            {"role": "system", "content": prompt_systeme},
            {"role": "user", "content": prompt_user_template},
        ]


        # Mistral models don't have any system role messages
    if my_model_name == "mistralai/Mixtral-8x7B-Instruct-v0.1":
            chat_messages.pop(0)

    client = client_openAI_init(my_model_name)

        # stream chat.completions
    chat_response = client.chat.completions.create(
            model=my_model_name, # this must be the model name the was deployed to the API server
        #    stream=True,
            max_tokens=6000,
            top_p=0.9,
            temperature=0.2,
            messages=chat_messages
        )
    output = chat_response.choices[0].message.content
    print(output)
    return output



#### MAP REDUCE ####



    #my_model_name = "meta-llama/Meta-Llama-3-8B-Instruct"
    #my_model_name = "gradientai/Llama-3-8B-Instruct-262k"
    #my_model_name = "mistralai/Mixtral-8x7B-Instruct-v0.1"

def map_reduce_only(list_doc, my_model_name = "meta-llama/Meta-Llama-3-8B-Instruct",chunk_size=2000,chunk_overlap=100):



    model_name, openai_api_base, openai_api_key= set_model_endpoint(my_model_name)
    print(f"Model: {model_name}, Endpoint: {openai_api_base}, api_key: {openai_api_key}")

    llm = ChatOpenAI(
            openai_api_base=openai_api_base,
            api_key=openai_api_key,
            model=my_model_name,
            temperature=0.5,
        )


    text_splitter = RecursiveCharacterTextSplitter.from_tiktoken_encoder(
        model_name="gpt-4",
        chunk_size=chunk_size,
        chunk_overlap=chunk_overlap,
    )

    text_docs_splitted = text_splitter.split_documents(list_doc)



    map_prompt_template = """
                            <|begin_of_text|><|start_header_id|> system <|end_header_id|>  Tu es un agent de l'administration qui fait des résumés de textes en français. 
                            <|eot_id|>
                            <|start_header_id|>user<|end_header_id|>
                            \n Tu dois résumer le texte suivant: 
                            '''{text}'''
                            \n Ne garde que les éléments importants et pertinents du passage. Concerver les idées générales et les conculsions.
                        <|eot_id|><|start_header_id|>assistant<|end_header_id|>
                        """


    map_prompt = PromptTemplate(template=map_prompt_template, input_variables=["text"])



    combine_prompt_template = """
                        <|begin_of_text|><|start_header_id|> system <|end_header_id|> Tu es un agent de l'administration qui fait des synthèses de textes. 
                        <|eot_id|>
                        <|start_header_id|>user<|end_header_id|>

                        Voici plusieurs textes qui sont des résumés d'un seul document. Tu dois synthéstiser ces textes pour obtenir un seul résumé du document initial.
                        Soit court et synthétique. Voici les textes : \n 
                        ```{text}``` \n 
                        Le résumé doit reprendre les principales informations chiffrés et déduire les tendances de celles-ci, et ne parler que de ce qui est important. Ne fais pas de redit d'information.
                     
                        Tu dois être exhaustif et structure ta synthèse. Utilise des connecteurs logiques.
                        <|eot_id|>
                        <|start_header_id|>assistant<|end_header_id|>
                        """

    combine_prompt = PromptTemplate(
        template=combine_prompt_template, input_variables=["text"]
    )

    map_reduce_chain = load_summarize_chain(
        llm,
        chain_type="map_reduce",
        map_prompt=map_prompt,
        combine_prompt=combine_prompt,
        #return_intermediate_steps=True,
    )

    map_reduce_outputs = map_reduce_chain(text_docs_splitted)

    return map_reduce_outputs




def map_reduce_with_toc(list_doc, plan, my_model_name = "meta-llama/Meta-Llama-3-8B-Instruct",chunk_size=2000,chunk_overlap=100):



    model_name, openai_api_base, openai_api_key= set_model_endpoint(my_model_name)
    print(f"Model: {model_name}, Endpoint: {openai_api_base}, api_key: {openai_api_key}")

    llm = ChatOpenAI(
            openai_api_base=openai_api_base,
            api_key=openai_api_key,
            model=my_model_name,
            temperature=0.5,
        )


    text_splitter = RecursiveCharacterTextSplitter.from_tiktoken_encoder(
        model_name="gpt-4",
        chunk_size=chunk_size,
        chunk_overlap=chunk_overlap,
    )

    text_docs_splitted = text_splitter.split_documents(list_doc)



    map_prompt_template = """
                            <|begin_of_text|><|start_header_id|> system <|end_header_id|>  Tu es un agent de l'administration qui fait des résumés de textes en français. 
                            <|eot_id|>
                            <|start_header_id|>user<|end_header_id|>
                            \n Tu dois résumer le texte suivant: 
                            '''{text}'''
                            \n Ne garde que les éléments importants et pertinents du passage. Concerver les idées générales et les conculsions.
                        <|eot_id|><|start_header_id|>assistant<|end_header_id|>
                        """


    map_prompt = PromptTemplate(template=map_prompt_template, input_variables=["text"])



    combine_prompt_template = """
                        <|begin_of_text|><|start_header_id|> system <|end_header_id|> Tu es un agent de l'administration qui fait des synthèses de textes. 
                        <|eot_id|>
                        <|start_header_id|>user<|end_header_id|>

                        Voici plusieurs textes qui sont des résumés d'un seul document. Tu dois synthéstiser ces textes pour obtenir un seul résumé du document initial.
                        Soit court et synthétique. Voici les textes : \n 
                        ```{text}``` \n 
                        Le résumé doit reprendre les principales informations chiffrés et déduire le sens de celles-ci, et ne parler que de ce qui est important. Ne fais pas de redit d'information.
                        Si tu peux, déduis des conclusions de chaque fait en quelques mots ou phrases. 
                        Tu dois être exhaustif et essaie de structurer ta synthèse. Utilise des connecteurs logiques.
                        Ta synthèse dois suivre le plan suivant:
                        """ + plan + """
                        Explique la démarche des portraits types de manière succinte. Tire des conclusions uniquements sur les statistiques.
                        <|eot_id|>
                        <|start_header_id|>assistant<|end_header_id|>
                        """

    combine_prompt = PromptTemplate(
        template=combine_prompt_template, input_variables=["text"]
    )

    map_reduce_chain = load_summarize_chain(
        llm,
        chain_type="map_reduce",
        map_prompt=map_prompt,
        combine_prompt=combine_prompt,
        #return_intermediate_steps=True,
    )

    map_reduce_outputs = map_reduce_chain(text_docs_splitted)

    return map_reduce_outputs


#### Map reduce Theme ####

def inference_map_reduce_theme(llm, text_docs_splitted, theme):
  map_prompt_template = """
                          <|im_start|>system \n Tu es un agent de l'administration qui fait des résumés de textes en français.
                          <|im_end|> \n
                          <|im_start|>user Tu dois m'écrire un chapitre sur le thème suivant : ''' """+ theme + """'''. 
                          \n Si le texte le permet, écris un résumé du texte sur le thème. Sinon ne réponds rien. Sois concis, clair et factuel.
                          Voici le texte à partir duquel tu dois composer : 
                          '''{text}'''
                          \n Ne garde que les éléments importants et pertinents du passage et Conclue bien tes pensées pour les structurer.
                          <|im_end|> \n 
                          <|im_start|>assistant
                        """
              

  map_prompt_template = """
                          <|begin_of_text|><|start_header_id|>system<|end_header_id|>
                        \n Tu es un agent de l'administration qui fait des résumés de textes en français.
                        <|eot_id|>\n\n
                        <|start_header_id|>user<|end_header_id|>
                        Tu dois m'écrire un chapitre sur le thème suivant : ''' """+ theme + """'''. 
                          \n Si le texte le permet, écris un résumé du texte sur le thème. Sinon ne réponds rien. Sois concis, clair et factuel.
                          Voici le texte à partir duquel tu dois composer : 
                          '''{text}'''
                          \n Ne garde que les éléments importants et pertinents du passage et Conclue bien tes pensées pour les structurer.
                          <|eot_id|>
                          \n\n<|start_header_id|>assistant<|end_header_id|>
                        """
              


  combine_prompt_template = """
                        <|im_start|>system \nTu es un agent de l'administration qui fait des synthèses de textes.
                        <|im_end|> \n
                        <|im_start|>user \n Voici plusieurs textes qui sont des résumés d'un seul document. Tu dois synthétiser ces textes pour obtenir un seul résumé du document initial.
                        Soit court et synthétique. Voici les textes : \n 
                        JE VEUX UN RAPPORT SUR LE THÈME SUIVANT : ''' """+ theme + """'''.
                        Voici le texte depuis lequel tu dois faire ton rapport: \n 
                                              ```{text}``` \n 
                        Le résumé doit reprendre les principales informations chiffrés et déduire le sens de celles-ci, et ne parler que de ce qui est important. 
                        Si tu peux, déduis des conclusions de chaque fait en quelques mots ou phrases. 
                        Tu dois être exhaustif et essaie de structurer ta pensée. Utilise des connecteurs logiques.
                        SOIS CONCIS.

                        <|im_end|> \n 
                        <|im_start|>assistant
                        """

  combine_prompt_template = """
                        <|begin_of_text|><|start_header_id|>system<|end_header_id|>\n
                        Tu es un agent de l'administration qui fait des synthèses de textes.
                        <|eot_id|>\n\n
                        <|start_header_id|>user<|end_header_id|>
                        \n Voici plusieurs textes qui sont des résumés d'un seul document. Tu dois synthétiser ces textes pour obtenir un seul résumé du document initial.
                        Soit court et synthétique. Voici les textes : \n 
                        JE VEUX UN RAPPORT SUR LE THÈME SUIVANT : ''' """+ theme + """'''.
                        Voici le texte depuis lequel tu dois faire ton rapport: \n 
                                              ```{text}``` \n 
                        Le résumé doit reprendre les principales informations chiffrés et déduire le sens de celles-ci, et ne parler que de ce qui est important. 
                        Si tu peux, déduis des conclusions de chaque fait en quelques mots ou phrases. 
                        Tu dois être exhaustif et essaie de structurer ta pensée. Utilise des connecteurs logiques.
                        SOIS CONCIS.

                          
                        <|eot_id|>
                        \n\n<|start_header_id|>assistant<|end_header_id|>
                        """

  map_prompt = PromptTemplate(template=map_prompt_template, input_variables=["text"])

  combine_prompt = PromptTemplate(
      template=combine_prompt_template, input_variables=["text"]
  )

  map_reduce_chain = load_summarize_chain(
      llm,
      chain_type="map_reduce",
      map_prompt=map_prompt,
      combine_prompt=combine_prompt,
      return_intermediate_steps=True,
  )

  map_reduce_outputs = map_reduce_chain(text_docs_splitted)

  output_summary = map_reduce_outputs['output_text']

  return output_summary 




def map_reduce_chapter_sum(doc_texte, chapter_list, my_model_name = "meta-llama/Meta-Llama-3-8B-Instruct", chunk_size=2000, chunk_overlap=300):



    model_name, openai_api_base, openai_api_key= set_model_endpoint(my_model_name)
    print(f"Model: {model_name}, Endpoint: {openai_api_base}, api_key: {openai_api_key}")

    llm = ChatOpenAI(
            openai_api_base=openai_api_base,
            api_key=openai_api_key,
            model=my_model_name,
            temperature=1.0,
        )

    text_splitter = RecursiveCharacterTextSplitter.from_tiktoken_encoder(
        model_name="gpt-4",
        chunk_size=chunk_size,
        chunk_overlap=chunk_overlap,
    )

    text_docs_splitted = text_splitter.split_documents([doc_texte])


    chapters_summ = []
    for chapitre in chapter_list:
        print(f" - {chapitre}")
        chap_summ = inference_map_reduce_theme(llm, text_docs_splitted, chapitre)
        chapters_summ.append(chap_summ)

    return chapters_summ



def inference_harmonisation( chapters_summ, chapitres, my_model_name =  "meta-llama/Meta-Llama-3-8B-Instruct"):

    model_name, openai_api_base, openai_api_key= set_model_endpoint(my_model_name)
    print(f"Model: {model_name}, Endpoint: {openai_api_base}, api_key: {openai_api_key}")

    llm = ChatOpenAI(
            openai_api_base=openai_api_base,
            api_key=openai_api_key,
            model=my_model_name,
            temperature=0.5,
        )


    chaine_concatenee = "\n\n".join(chapters_summ)
    chapitre_concate = "\n".join(chapitres)


    harmonisation_prompt_template = """
                        <|begin_of_text|><|start_header_id|>system<|end_header_id|>\n
                        Tu es un agent de l'administration qui rédige des textes dans un français parfait et avec une grande maîtrise de la rédaction.
                        <|eot_id|>\n\n
                        <|start_header_id|>user<|end_header_id|>
                        \n Voici un texte, je souhaite que tu repasses deçu pour que je puisse le publier. Tu dois rédiger avec des connecteurs logiques et rédiger parfaitement.
                        \n 
                        Tu dois garder la structure suivante : \n 
                        '''{chap}'''
                        Voici le texte :\n 
                                                ```{text}``` \n 
                        Le rapport doit garder la même structure mais en gardant au maximum les conclusions plutôt que les faits. Tu dois argumenter.
                        Tu dois être exhaustif et essaie de structurer ta pensée. Le texte dois ensuite être publiable.
                        SOIS CONCIS.

                            
                        <|eot_id|>
                        \n\n<|start_header_id|>assistant<|end_header_id|>
                        """
    harmonisation_prompt = PromptTemplate(template=harmonisation_prompt_template, input_variables=["text","chap"])

    output_parser = StrOutputParser()

    chain = harmonisation_prompt | llm | output_parser

    output = chain.invoke({"text": chaine_concatenee, "chap":chapitre_concate})

    return output


### RAG MAP REDUCE ###


def map_reduce_chapter_sum_RAG(doc_texte, chapter_list, my_model_name = "meta-llama/Meta-Llama-3-8B-Instruct", chunk_size=2000, chunk_overlap=300):


    text_splitter = RecursiveCharacterTextSplitter(
        # Set a really small chunk size, just to show.
        chunk_size=2000,
        chunk_overlap=300,
        length_function=len,
        is_separator_regex=False,
        separators=[
            "\n\n",
            "\n",
            ".",
            " ",],
    )

    chunks = text_splitter.split_documents(doc_texte)

    lc_embed_model = HuggingFaceEmbeddings(
        model_name="BAAI/bge-m3"
    )
    embed_model = LangchainEmbedding(lc_embed_model)

    db = FAISS.from_documents(chunks, lc_embed_model)
    score_thresh = 0.1
    tok_k = int(len(chunks) /4)

    retriever = db.as_retriever(search_type="similarity_score_threshold", search_kwargs={"score_threshold": score_thresh,"k": tok_k})

    model_name, openai_api_base, openai_api_key= set_model_endpoint(my_model_name)
    print(f"Model: {model_name}, Endpoint: {openai_api_base}, api_key: {openai_api_key}")

    llm = ChatOpenAI(
            openai_api_base=openai_api_base,
            api_key=openai_api_key,
            model=my_model_name,
            temperature=0.5,
        )

    text_splitter = RecursiveCharacterTextSplitter.from_tiktoken_encoder(
        model_name="gpt-4",
        chunk_size=chunk_size,
        chunk_overlap=chunk_overlap,
    )

    summary_theme_list = []

    for chap in chapter_list:
        theme = chap
        retrieve_docs = retriever.get_relevant_documents(theme)
        summ_theme = inference_map_reduce_theme(llm, retrieve_docs, chap)
        summary_theme_list.append(summ_theme)

        return summary_theme_list






#### Utils ####

def num_tokens_from_string(string: str, encoding_name: str) -> int:
    """Returns the number of tokens in a text string."""
    encoding = tiktoken.get_encoding(encoding_name)
    num_tokens = len(encoding.encode(string))
    return num_tokens