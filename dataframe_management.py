import pandas


# Définir une fonction personnalisée pour extraire la 4ème valeur de la suite
def extraire_sous_chapitre(chapitre):
    return chapitre[-1]

# Définir une fonction personnalisée pour concaténer les chaînes
def concatener_chaines(series):
    return '\n'.join(series)

# Définir une fonction personnalisée pour retirer le dernier élément de la liste
def retirer_dernier_element_liste(liste):
    if liste:
        liste.pop()
    return liste


def concate_chapter(df):

    df_copie = df.copy()
    # Ajouter une nouvelle colonne avec la valeur du dernier sous chapitre
    df_copie['sous_chapitre'] = df_copie['chapitre'].apply(extraire_sous_chapitre)

    df_agrege = df_copie.groupby('sous_chapitre').agg({'niveau':'min','chapitre':'first','titre': 'first','texte_associé': concatener_chaines }).reset_index()

    df_agrege = df_agrege.drop(columns=['sous_chapitre'])

    # Appliquer la fonction personnalisée à la colonne à modifier
    df_agrege['chapitre'] = df_agrege['chapitre'].apply(retirer_dernier_element_liste)

    return df_agrege
