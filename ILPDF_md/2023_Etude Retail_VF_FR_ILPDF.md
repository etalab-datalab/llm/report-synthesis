

__MARS 2023__

ACTIVITE DES INVESTISSEURS PARTICULIERS ET PORTRAITS TYPES



#### PATRICIA DOS SANTOS, EDOUARD CHATILLON, MICHEL DEGRYSE

*Cette étude a été réalisée par la Direction de la supervision des intermédiaires et infrastructures de marché\. Elle repose sur l’utilisation de sources considérées comme fiables mais dont l’exhaustivité et l’exactitude ne peuvent être garanties\. Les données chiffrées ou autres informations ne sont valables qu'à la date de publication du rapport et sont sujettes à modification dans le temps\.*

*Toute copie, diffusion et reproduction de cette étude, en totalité ou partie, sont soumises à l’accord exprès, préalable et écrit de l’AMF\.*

# SYNTHESE

L’AMF a publié en avril 2020 et novembre 2021 deux études relatives aux investisseurs particuliers et leur activité\. Après ces analyses des comportements des investisseurs pendant et après la crise COVID, il paraissait utile de s’intéresser aux comportements des investisseurs français selon leur âge et sexe\. L’AMF a donc utilisé les données issues du *reporting *MiFIR1 pour analyser les différences de comportement selon les tranches d’âge et entre hommes et femmes\.

####     Périmètre de l’étude

La présente étude est réalisée à partir des __transactions __effectuées sur des titres vifs2 par les investisseurs particuliers de nationalité française sur les instruments financiers français3\. Il est utile de préciser que __cette étude porte donc sur les titulaires des comptes titres4 ayant enregistré au moins une transaction au cours de la période __et non sur l’ensemble des investisseurs, puisque ceux n’ayant pas eu d’activité sont mécaniquement écartés\. Afin de présenter un portrait type de l’investisseur actif français en 2022 et d’y inclure une vision par sexe, les transactions effectuées sur des comptes joints n’ont pas été incluses, puisqu’il n’est pas possible de distinguer l’activité de chacun des titulaires du compte\. Enfin, pour permettre d’identifier le sexe des investisseurs, nous avons pris le parti de dresser une correspondance entre prénom et sexe, en incluant les transactions pour lesquelles le sexe peut être déduit du prénom avec un niveau de confiance de 98%5\.

####     Analyse de la population d’investisseurs français actifs et leurs transactions effectuées en 2022

Entre 2019 et 2022, __la population d’investisseurs actifs s’est fortement rajeunie __du fait de l’activité croissante des moins de 35 ans\. La population des plus de 35 ans est stable à l’exception toutefois d’un fléchissement du nombre d’investisseurs actifs entre 35 et 45 ans\.

Les investisseurs actifs en 2022 représentent 2,1% de la population française6 dont __30% de femmes et 70% d’hommes\.__

La proportion d’investisseurs actifs femmes augmente avec l’âge, passant de 17% des moins de 35 ans à plus de 30% après 55 ans\. Ceci montre un comportement différencié : __la population d’investisseurs actifs hommes croît rapidement dès les premières tranches d’âge et la population d’investisseurs actifs femmes augmente lentement au fil des tranches d’âge\.__

L’étude des transactions des investisseurs nous révèle par ailleurs que __les hommes effectuent une portion plus importante des transactions que les femmes, __de manière logique car ils constituent plus des deux tiers de la

1 Règlement \(UE\) N° 600/2014 du Parlement Européen et du Conseil du 15 mai 2014 concernant les marchés d’instruments financiers et modifiant le règlement \(UE\) no 648/2012\. Les données issues du *reporting *MiFIR sont désignées ci\-après par

« Source AMF »\.

2 Les titres vifs sont ceux cotés individuellement et détenus directement par les investisseurs\. Les titres détenus au travers d’organismes de placement collectif \(y compris ceux issus d’épargne salariale et d’assurance\-vie\) échappent donc à cette étude\. 3 Instruments dont l’autorité compétente est l’AMF au titre du règlement MiFIR\.

4 Ces titulaires de comptes sont considérés comme étant les investisseurs qui ont effectué les transactions, dans la mesure où ces dernières leurs sont affectées, nonobstant les éventuelles délégations et gestion sous mandat\.

5 Des informations plus détaillées sur les données de cette étude sont fournies en annexe\.

6 Le nombre d’investisseurs actifs en 2022 diffère fortement des investisseurs pour lesquels le taux de détention d’actions en direct est estimé par l’institut Kantar à 6,7% des Français de 15 ans et plus en mars 2022 \(cf\. la Lettre de l’Observatoire de l’épargne de l’AMF n°49\)\.

population d’investisseurs, mais aussi parce que le nombre médian de transactions qu’ils concluent est __1,5 fois à 2 fois celui des femmes selon la classe d’âge__\.

Si l’on ne constate pas de différence sensible entre hommes et femmes concernant la répartition des sommes des transactions acheteuses et vendeuses en montant, il est tout de même notable que, sans surprise, __les jeunes investisseurs effectuent un montant global de transactions à l’achat plus élevé qu’à la vente__, tandis que __les tranches d’âge les plus élevées effectuent un montant global de transactions vendeuses plus important qu’à l’achat__\.

En ce qui concerne le montant des transactions réalisées sur actions, on observe que __les montants des transactions des femmes sont très proches de ceux des hommes__\. Les femmes appartenant aux classes d’âge les plus jeunes ont toutefois tendance à effectuer des transactions de montant significativement plus important que celles des hommes : 3 fois plus important pour les 24 ans ou moins, et 1,4 fois plus important pour les 25\-29 ans\.

Les investisseurs se répartissent de manière prédominante sur les actions \(78,1% des investisseurs\), les ETF7 \(13,4%\) puis les instruments complexes \(5,5%\) et enfin les obligations \(3%\)\. Les instruments privilégiés des investisseurs français sont les actions, au sein desquelles l’action la plus traitée en 2022 est TotalEnergies\. On peut toutefois observer que __la part des transactions sur instruments complexes et sur ETF est plus importante pour les classes d’âge les plus jeunes __et tend à diminuer avec l’âge des investisseurs au profit des actions\. Parmi les différences de comportement observées, __les hommes négocient plus d’instruments complexes8 que les femmes__\.

En ce qui concerne la __valorisation des portefeuilles constitués sur valeurs françaises depuis 20189, elle s’accroît au fil des tranches d’âge __et elle est légèrement supérieure pour les hommes\.

    __Portraits types__

Cette étude présente des portraits types d’investisseurs selon les tranches d’âge et le sexe et s’intéresse aux caractéristiques de leurs transactions sur les instruments financiers sous compétence de l’AMF\.

Ces portraits types permettent de mettre en lumière des différences selon les profils, notamment les tailles de portefeuilles, le nombre de transactions et d’instruments différents négociés par an\.

7 Les ETF \(Exchange Traded Funds\) sont des fonds cotés dont la performance est généralement indexée sur celle des indices boursiers\.

8 Les instruments complexes sont déterminés par opposition aux actions, obligations et ETF, ils recouvrent notamment les warrants, les certificats et les contrats dérivés\.

9 La valorisation des portefeuilles est une reconstitution effectuée à partir des transactions sur actions enregistrées depuis 2018, année à laquelle le *reporting *MiFIR a démarré\. Le périmètre retenu pour l’étude ne couvre que les instruments français\. Cet indicateur progressera en qualité au fil des années avec la constitution d’un historique plus long dans la mesure où les positions des investisseurs avant 2018 ne sont pas connues\.

# TABLE DES MATIERES

Synthèse	3

Table des matières	5

Présentation de l’étude	6

1. [Analyse de la population d’investisseurs	6](#_TOC_250001)
	1. Evolution du nombre d’investisseurs français et leur répartition par tranche d’âge et sexe	6
	2. Les transactions négociées par les investisseurs français en 2022	12
	3. Les instruments financiers négociés par les investisseurs français	16
2. [Profils types	21](#_TOC_250000)

Annexe : Données de l’étude	26

__PRESENTATION DE L’ETUDE__

L’étude vise à définir un portrait type d’investisseur particulier français actif en 2022, selon une répartition par classe d’âge et sexe\. Le périmètre choisi dans le cadre de cette étude porte donc sur l’ensemble des transactions des investisseurs français sur instruments financiers dont l’AMF est l’autorité compétente \(et reçues au titre du règlement MiFIR\)\. De plus, le but de cette étude étant d’analyser des comportements individuels d’investisseurs particuliers, les transactions effectuées sur les comptes joints ont été volontairement exclues\. Enfin, dans le cadre du règlement MiFIR, les investisseurs particuliers français sont identifiés à l’aide d’un code ne permettant pas d’en connaître le sexe\. Aussi, sur la base du référentiel des prénoms de l’INSEE, le sexe de chaque investisseur a pu être extrapolé à partir du prénom\. Plus de détails concernant ce périmètre sont donnés en annexe\.

L’analyse est construite autour de trois axes principaux : la répartition des investisseurs, les caractéristiques de leurs transactions et enfin leurs investissements avant de finir par la présentation des portraits\-robots des investisseurs\.

# <a id="_TOC_250001"></a>ANALYSE DE LA POPULATION D’INVESTISSEURS

## EVOLUTION DU NOMBRE D’INVESTISSEURS FRANÇAIS ET LEUR REPARTITION PAR TRANCHE D’AGE ET SEXE

Avant de centrer l’analyse sur l’année 2022, l’évolution du nombre d’investisseurs au cours des quatre dernières années est présentée ci\-après\. Ainsi, le graphique ci\-dessous représente le nombre d’investisseurs actifs par âge et par année, entre 2019 et 2022\. Sont ici représentés l’ensemble des investisseurs français ayant effectué au moins une transaction sur tous types d’instruments financiers sous compétence française au cours de la période\.

### Nombre d’investisseurs actifs10 par âge et par an

__1,15 M__

__d’investisseurs actifs__

__1,22 M__

__d’investisseurs actifs__

__1,41 M__

__d’investisseurs actifs__

__1,43 M__

__d’investisseurs actifs__

__Age__

*Source : AMF*

__Investisseurs actifs__

10 Investisseur actif : un investisseur ayant effectué une transaction au cours de l’année concernée, qu’elle soit à l’achat ou à la vente ; un investisseur inactif pendant une année ne sera donc pas compté pour l’année concernée\.

La succession des graphiques des années 2019 à 2022 permet de formuler les observations suivantes :

 la répartition des investisseurs actifs par âge semble évoluer en 2019 selon une logique où les investisseurs sont au fil de l’eau plus nombreux par tranche d’âge, puis leur nombre décroît progressivement ;

 depuis 2020, on observe un fort accroissement des populations d’investisseurs au\-dessous de 35 ans, une croissance ensuite plus mesurée de 35 à 70 ans \(avec toutefois un fléchissement du nombre d’investisseurs de 35 à 45 ans\) et enfin un nombre décroissant d’investisseurs au\-delà de 70 ans ;

 depuis le démarrage du *reporting *MiFIR en 2018, on observe que le nombre de nouveaux investisseurs de moins de 30 ans ne cesse d’augmenter : les Français deviennent investisseurs plus jeunes qu’auparavant ;

 au global, on observe qu’entre 2019 et 2022, le nombre d’investisseurs actifs est passé de 1,15 million à 1,43 million\.

Lorsque la population d’investisseurs actifs est présentée par sexe, les graphiques permettent d’observer les premiers éléments de différenciation entre hommes et femmes\.

### Nombre d’investisseurs hommes actifs par âge et par an

__Age__

*Source : AMF*

__Investisseurs actifs__

 Les mêmes constats que sur la population totale peuvent être formulés en ce qui concerne la population masculine, car comme nous le verrons dans la suite de cette note, la proportion d’investisseurs hommes est plus importante que celle des femmes\.

__Age__

*Source : AMF*

### Nombre d’investisseurs femmes actifs par âge et par an

__Investisseurs actifs__

 Les graphiques concernant la population féminine des investisseurs diffèrent de manière notable des précédents\. On constate notamment une croissance plus progressive des investisseurs   femmes jusqu’à 75 ans, qui contraste avec l’évolution de la population masculine d’investisseurs, puis deux phases de décroissance de la population féminine dont le pivot se situe autour de 90 ans\.

### Evolution de l’âge moyen des investisseurs actifs par sexe

*Source : AMF*

Ce graphique, qui s’étale de 2018 à 2022, permet de constater que :

   le rajeunissement global observé est encore plus marqué chez les investisseurs hommes, puisque l’âge moyen des investisseurs actifs hommes est passé de 57 ans à 49 ans et pour les femmes de 64 à 60 ans ;

 l’âge moyen des investisseurs femmes est bien supérieur à celui des hommes, avec un écart de 11 ans entre investisseurs hommes et femmes en 2022, alors que la différence n’était que de 7 ans en 2018\.

Si les graphiques précédents portaient sur la période précédant 2022, les données présentées dans la suite de cette étude portent uniquement sur l’année 2022\.

### Population française 2022 et investisseurs individuels français actifs en 2022 par sexe

#### Population française	Population française d’investisseurs actifs11 en 2022

*68 millions	1,4 million d’investisseurs actifs en 2022	0,43 million de femmes*

*1 million d’hommes*

__48,4%	51,6%__

#### Les investisseurs actifs en 2022 représentent :

__2,1% de la population française__

__2,8% de la population de plus de 24 ans__

__70%	30%__

*Sources : AMF et INSEE*

En premier lieu, la  répartition  homme\-femme de la  population d’investisseurs actifs  en comparaison de la population française permet de dresser les constats suivants :

 les investisseurs actifs en 2022 représentent 2,1% de la population française et 2,8% de la population de plus de 24 ans, mais on observe une différenciation forte au sein de la population d’investisseurs entre hommes et femmes ;

	la part des femmes est de 30% des investisseurs contre 70% pour les hommes ;

 les investisseurs femmes représentent 1,23% de la population des femmes françaises, tandis que les investisseurs hommes représentent 3,05% de la population des hommes français\.

11 Le nombre d’investisseurs actifs ne tient pas compte des investisseurs n’ayant pas effectué de transaction au cours de l’année 2022 : par conséquent, les investisseurs qui détiennent des actions sans effectuer d’opérations cette année\-là ne sont pas pris en compte\. Le taux de détention d’actions en direct estimé par l’institut Kantar était de 6,7% des Français de 15 ans et plus en mars 2022 \(cf\. la Lettre de l’Observatoire de l’épargne de l’AMF n°49\)\.

__Nomb__

La répartition par tranche d’âge et par sexe permet une meilleure appréhension de la population d’investisseurs français, notamment sur les tranches d’âge entre 25 et 74 ans qui sont constituées chacune d’environ 4 millions de Français\. Au contraire, les tranches d’âge de 24 ans ou moins et de 75 ans et plus, constituées respectivement de 20 millions et 6,8 millions de Français, peuvent difficilement être comparées aux autres\.

__Pourcentages présentés dans ce graphique__

Les pourcentages sont exprimés en pourcentage de la population française par classe d’âge et sexe\.

Ainsi, pour la classe d’âge de 25 à 29 ans, 1% des 1,9 million de femmes ont effectué des transactions en 2022 et ont ainsi été considérées comme investisseurs actifs, soit 19 000 femmes\. Sur cette même classe d’âge, 87 000 hommes ont effectué des transactions en 2022, soit 4,7% de la population masculine de cette classe d’âge\. De manière analogue, pour les plus de 75 ans, on dénombre 120 000 femmes investisseurs actifs sur les 4,1 millions de femmes de cette tranche d’âge \(soit 2,9%\), tandis qu’on compte 124 000 hommes investisseurs actifs sur les 2,7 millions d’hommes de cette tranche d’âge\.

 On observe que la population d’investisseurs \(hommes et femmes confondus\) par tranche d’âge tend à augmenter avec l’âge\.

__re d’investisseurs français actifs en 2022 en % de la population française, par tranche d’âge et__

__répartition par sexe__

*Sources : AMF et INSEE*

__Nb investisseurs actifs__

__% de la population concernée__

	L’évolution de la population d’investisseurs est toutefois très différente selon les sexes :

- dès la tranche d’âge 25\-29 ans, le nombre d’investisseurs hommes se maintient à un niveau élevé aux alentours de 4% de la population ;
- tandis que la population des femmes qui investissent augmente régulièrement avec l’âge pour atteindre 2,9% pour les 75 ans et plus ;
- pour les 75 ans et plus, la parité est presque atteinte en nombre d’investisseurs actifs mais ne l’est pas en termes de proportion de chacune des populations\.

 Les hommes effectuent des transactions sur instruments financiers dès qu’ils ont 25 ans et plus, et chaque tranche d’âge contient entre 3,6% et 4,7% d’investisseurs actifs\.

 De manière contrastée, les investisseurs femmes des tranches d’âge les plus jeunes ne représentent qu’une faible proportion des femmes françaises, mais cette proportion tend à progresser lentement d’une tranche d’âge à une autre\.

### Répartition H/F en nombre d’investisseurs actifs en 2022

*Source : AMF*

 La population d’investisseurs actifs de moins de 35 ans est constituée de manière prédominante d’investisseurs hommes et de peu de femmes en proportion \(259 000 investisseurs actifs hommes et 54 000 investisseurs actifs femmes pour les moins de 35 ans\)\.

 Si les femmes ne représentent que 17% des investisseurs actifs de moins de 35 ans, elles représentent au moins 30% de la population d’investisseurs actifs des tranches d’âge après 55 ans\.

## LES TRANSACTIONS NEGOCIEES PAR LES INVESTISSEURS FRANÇAIS EN 2022

La présentation de la population d’investisseurs français étant faite, il convient à présent d’analyser leur comportement au cours de l’année 2022\. Tout d’abord, le graphique suivant présente le nombre de transactions réalisées pour chacune des classes d’âge, réparties par sexe\.

### Répartition par sexe du nombre de transactions en 2022

*Source : AMF*

Ainsi, plusieurs constats peuvent être formulés :

     le nombre de transactions total augmente avec l’âge des investisseurs jusqu’à 54 ans, puis diminue jusqu’à 74 ans ;

 la proportion des transactions effectuées par des femmes reste très largement inférieure à celle des hommes pour toutes les classes d’âge, même si cette proportion tend à augmenter avec l’âge \(passant de 10,3% pour les 24 ans ou moins à 29,6% pour les 75 ans et plus\), de manière corrélée avec l’augmentation de la proportion de femmes chez les investisseurs actifs ;

  les investisseurs réalisant le plus de transactions ont entre 45 et 64 ans, avec plus de 4 millions de transactions pour chacune des tranches d’âge concernées ;

 le nombre de transactions effectuées par les 75 ans et plus se compare difficilement aux autres tranches d’âge dans la mesure où cette classe d’âge est plus large que les autres en matière d’âges pris en compte et regroupe au moins 40% d’individus de plus\.

### Répartition par sexe et âge des transactions acheteuses et vendeuses en montant en 2022

*Source : AMF*

Si l’on ne constate pas de différence sensible entre hommes et femmes concernant la répartition des sommes des transactions acheteuses et vendeuses en montant, il est tout de même notable que les jeunes investisseurs effectuent un montant global de transactions à l’achat plus élevé qu’à la vente, tandis que les tranches d’âge les plus élevées effectuent un montant global de transactions vendeuses plus important qu’à l’achat\.

Les analyses menées sur des nombres et des montants globaux mettent en avant la difficulté d’analyse de ces données pour lesquelles les tranches d’âge sont constituées d’un nombre différent d’individus, c’est pourquoi il semble à présent pertinent d’analyser les comportements observés par investisseur\. Pour cela, une analyse en médiane a été privilégiée, plutôt qu’une analyse en moyenne qui serait plus sensible aux observations extrêmes de l’échantillon\.

Tout d’abord, l’activité des investisseurs peut être analysée sous l’angle du nombre médian de transactions réalisées par un investisseur, selon sa tranche d’âge et son sexe, au cours de l’année 2022\.

### Nombre médian de transactions12 en 2022 par classe d’âge et par sexe

*Source : AMF*

L’analyse du nombre médian de transactions par classe d’âge et sexe permet de formuler les constats suivants :

 l’évolution du nombre médian de transactions par tranches d’âge est relativement différente chez les hommes et les femmes : ces dernières réalisent un nombre médian de transactions relativement similaire d’une tranche d’âge à une autre \(2 à 3 transactions selon la classe d’âge\), alors que les hommes réalisent de 4 à 6 transactions \(médiane\) selon la classe d’âge ;

   les hommes les plus actifs ont entre 30 et 54 ans alors que les 75 ans et plus effectuent moins de transactions ;

 quelle que soit la tranche d’âge, le nombre médian de transactions des hommes est toujours supérieur à celui des femmes : les hommes effectuent 1,5 à 2 fois plus de transactions que les femmes\.

Si l’activité des investisseurs peut s’observer en nombre de transactions réalisées, il est également possible de l’analyser sous l’angle du nombre de mois d’activité des investisseurs\. Cet indicateur mesure combien de fois dans l’année l’investisseur médian apporte des modifications à son portefeuille\.

### Nombre médian de mois où l’investisseur a été actif8 en 2022 par classe d’âge et par sexe

*Source : AMF*

Ce graphique amène à établir les constats ci\-après :

12 Pour tous types d’instruments confondus \(actions, obligations, ETF et instruments complexes\)\.

    les femmes ont tendance à effectuer des transactions sur un nombre médian de mois moins important que les hommes : leurs transactions sont regroupées sur un ou deux mois en médiane \(contre deux à trois mois pour les hommes, en excluant les 75 ans et plus\)\. Ce résultat est cohérent avec le fait qu’elles effectuent moins de transactions que les hommes, comme indiqué précédemment ;

 les investisseurs les plus actifs en nombre \(médian\) de mois sont des hommes entre 30 et 44 ans et réalisant l’ensemble de leurs transactions sur trois mois de l’année\.

Enfin, l’activité des investisseurs français peut également s’observer en montant par transaction\. Le graphique suivant met en avant les montants médians par transaction observés pour les hommes et les femmes selon leur classe d’âge\. Si, d’une façon générale, les femmes effectuent moins de transactions que les hommes quelle que soit leur classe d’âge \(comme cela est présenté dans cette sous\-section\), les montants de leurs transactions sont similaires, voire légèrement supérieurs à ceux des hommes\.

### Montants médians par transaction réalisée sur actions en 2022 par sexe

*Source : AMF*

Le graphique ci\-dessus, qui porte sur les montants médians des transactions sur actions \(excluant les autres instruments notamment dérivés dont les montants sont moins révélateurs\), permet de constater que :

    les montants par transaction négociés par les femmes sont très proches de ceux des hommes ;

 les femmes appartenant aux classes d’âge les plus jeunes ont tendance à effectuer des transactions d’un montant significativement plus important que celles des hommes : 3 fois plus pour les 24 ans ou moins et 1,4 fois plus pour les 25\-29 ans\.

## LES INSTRUMENTS FINANCIERS NEGOCIES PAR LES INVESTISSEURS FRANÇAIS

Après l’analyse des transactions, il semble pertinent d’étudier la composition en valeurs françaises des portefeuilles des investisseurs français constitués depuis 2018\. Un premier angle d’analyse concerne le nombre d’instruments différents traités par les investisseurs\. Ainsi, le graphique suivant présente le nombre médian d’instruments distincts traités par investisseur, au cours de l’année 2022, par sexe et classe d’âge\.

### Nombre médian13 d’instruments traités en 2022 par classe d’âge et par sexe

*Source : AMF*

Il en découle que :

	les investisseurs ont traité de manière prédominante 2 instruments financiers différents au cours de l’année 2022 ;

	les hommes de 25 à 54 ans négocient de manière générale 3 instruments financiers au cours de l’année, alors que les femmes de moins de 25 ans et celles de plus de 74 ans n’en traitent qu’un\.

Parmi les transactions réalisées en 2022 par les investisseurs français, la répartition entre actions, ETF, instruments complexes14 et obligations s’avère intéressante\.

13 Pour tous types d’instruments confondus \(actions, obligations, ETF et instruments complexes\)\.

14 Les instruments complexes sont déterminés par opposition aux actions, obligations et ETF, ils recouvrent notamment les warrants, les certificats et les contrats dérivés\. Cette convention tend à minorer la part des instruments complexes puisqu’on compte les ETF à effet de levier sont catégorisés parmi les ETF\. Il en est de même des produits structurés comptés parmi les obligations\.

### Répartition du nombre d’investisseurs actifs en 2022 par type d’instrument, par classe d’âge et par sexe





*Source : AMF*

Ce graphique met en avant que :

 les investisseurs se répartissent de manière prédominante sur les actions \(78,1% des investisseurs\), les ETF \(13,4%\) puis les instruments complexes \(5,5%\) et enfin les obligations \(3%\) ;

 les classes d’âge les plus jeunes sont celles qui traitent le plus sur ETF et instruments complexes, mais cela tend à diminuer avec l’âge des investisseurs au profit des actions ;

 les investisseurs hommes sont plus nombreux que les femmes sur les produits complexes \(6,6% des investisseurs hommes contre 2,8% de femmes, toutes classes d’âge confondues\) et, à l’inverse, les investisseurs femmes sont plus nombreux que les hommes sur les obligations \(5,5% des femmes contre 1,9% d’hommes, toutes classes d’âge confondues\) ;

	les instruments privilégiés des investisseurs français \(hommes comme femmes\) sont donc les actions\.

Le tableau suivant présente les actions les plus échangées, en montant, au cours de l’année 2022\.

### Actions françaises les plus échangées en 2022 en montant par classe d’âge et par sexe

__Femmes	Hommes__

__1__

__2__

__3__

__1__

__2__

__3__

__24 ans ou moins__

LVMH

L'Oréal

Air Liquide

TotalEnergies

Renault

Kering

__25\-29 ans__

TotalEnergies

Saint\-Gobain

LVMH

Orpea

TotalEnergies

Renault

__30\-34 ans__

Nestlé

TotalEnergies

Airbus

CGG

Renault

Société Générale

__35\-39 ans__

Société Générale

TotalEnergies

Airbus

Renault

Orpea

Société Générale

__40\-44 ans__

Safran

TotalEnergies

CGG

Orpea

TotalEnergies

Société Générale

__45\-49 ans__

Société Générale

TotalEnergies

LVMH

Société Générale

Orpea

TotalEnergies

__50\-54 ans__

TotalEnergies

LVMH

Airbus

TotalEnergies

Société Générale

Atos

__55\-59 ans__

TotalEnergies

Airbus

Société Générale

BNP Paribas

Société Générale

TotalEnergies

__60\-64 ans__

LVMH

Société Générale

TotalEnergies

Airbus

TotalEnergies

Société Générale

__65\-69 ans__

TotalEnergies

LVMH

Air Liquide

Airbus

TotalEnergies

LVMH

__70\-74 ans__

BNP Paribas

TotalEnergies

LVMH

TotalEnergies

Eramet

LVMH

__75 ans ou plus__

Air Liquide

LVMH

TotalEnergies

TotalEnergies

LVMH

Air Liquide

*Source : AMF*

 Globalement, l’action TotalEnergies est l’action la plus échangée en montant, quels que soient l’âge et le sexe de l’investisseur\. En effet, cette action est très fréquemment présente dans le classement des hommes et des femmes : dans 11 tranches d’âge sur 12 pour les femmes et 10 tranches d’âge sur 12 pour les hommes\. La place de cette action dans ce classement basé sur les transactions n’est toutefois pas surprenante compte tenu de sa présence récurrente au sein des portefeuilles des Français et de l’actualité de 2022\.

 Parmi les différences notables entre sexes, l’action LVMH compte plus souvent parmi les trois actions les plus échangées par les femmes \(dans 8 classes d’âge sur 12\) que chez les hommes \(dans seulement 3\)\. A l’inverse, l’action Société Générale compte plus souvent parmi les trois actions les plus négociées par les hommes \(dans 7 classes d’âge sur 12\) que chez les femmes \(dans 4 classe d’âge sur 12\)\. Enfin, si l’action Orpea est présente dans le classement des trois actions les plus échangées par les hommes \(dans 4 classes d’âge sur 12\), elle est absente dans celui des femmes\.

Une des limites de ce classement est qu’il repose sur le montant des transactions réalisées\. Il peut donc être déformé par quelques grosses transactions réalisées par un nombre peu significatif d’investisseurs, d’où l’intérêt de compléter cette analyse par le classement des actions les plus échangées en nombre d’investisseurs\.

### Actions françaises les plus échangées en 2022 en nombre d’investisseurs par classe d’âge et par sexe

__Femmes	Hommes__

__1__

__2__

__3__

__1__

__2__

__3__

__24 ans ou moins__

TotalEnergies

Air Liquide

LVMH

TotalEnergies

Air Liquide

Crédit Agricole

__25\-29 ans__

TotalEnergies

Air Liquide

LVMH

TotalEnergies

Air Liquide

Crédit Agricole

__30\-34 ans__

TotalEnergies

Air Liquide

LVMH

TotalEnergies

Air Liquide

Crédit Agricole

__35\-39 ans__

TotalEnergies

Air Liquide

Crédit Agricole

TotalEnergies

Air Liquide

Orpea

__40\-44 ans__

TotalEnergies

Air Liquide

Crédit Agricole

TotalEnergies

Crédit Agricole

Air Liquide

__45\-49 ans__

TotalEnergies

EDF

Air Liquide

TotalEnergies

Crédit Agricole

Orpea

__50\-54 ans__

TotalEnergies

EDF

Air Liquide

TotalEnergies

Crédit Agricole

EDF

__55\-59 ans__

EDF

TotalEnergies

Air Liquide

TotalEnergies

EDF

Crédit Agricole

__60\-64 ans__

EDF

TotalEnergies

Air Liquide

TotalEnergies

EDF

Crédit Agricole

__65\-69 ans__

EDF

TotalEnergies

Air Liquide

TotalEnergies

EDF

Air Liquide

__70\-74 ans__

EDF

TotalEnergies

Air Liquide

EDF

TotalEnergies

Air Liquide

__75 ans ou plus__

EDF

Crédit Agricole

TotalEnergies

EDF

TotalEnergies

Air Liquide

*Source : AMF*

 De la même façon que dans le précédent tableau, l’action la plus négociée en 2022 est TotalEnergies\. Quels que soient la classe d’âge de l’investisseur et son sexe, l’action TotalEnergies reste l’une des trois actions les plus échangées par les investisseurs français\.

 L’action EDF est également l’une des actions les plus négociées par les investisseurs français \(tous sexes confondus\) de plus de 49 ans \(voire de plus de 44 ans pour les femmes\), ceci s’expliquant très certainement par l’actualité en 2022 du titre\.

 L’action Air Liquide est elle aussi très largement représentée dans ce classement \(et bien plus que dans le classement en montant\)\. En effet, on la retrouve chez les femmes dans quasiment toutes les classes d’âge, sauf la plus âgée, et chez les hommes dans 8 classes d’âge sur 12\.

    Parmi les spécificités, l’action LVMH n’est présente que dans le classement des femmes de moins de 35 ans\.

 A l’inverse, on retrouve plus d’actions bancaires \(et notamment Crédit Agricole\) dans le classement masculin \(dans 8 classes d’âge sur 12\) que dans le classement féminin \(dans seulement 3 classes d’âge sur 12\)\.

Afin de dresser un profil type des investisseurs complet, il est important de rappeler que la valorisation de leurs portefeuilles a été reconstituée à partir des transactions sur actions françaises effectuées depuis 2018, année à laquelle le *reporting *MiFIR a démarré\. Cet indicateur, qui ne donne qu’une vue parcellaire, progressera en qualité au fil des années avec la constitution d’un historique plus long dans la mesure où les positions des investisseurs avant 2018 ne sont pas connues\.

### Répartition de la valorisation des portefeuilles constitués depuis 2018 en actions françaises et ETF français à fin décembre 2022

*Source : AMF*

 L’analyse de la répartition des valorisations des portefeuilles repose sur un sous\-périmètre de l’étude comprenant les actions et les ETF et excluant les instruments dérivés et les obligations\.

 La répartition des valorisations des portefeuilles montre que les investisseurs actifs hommes de moins de 24 ans ont une proportion de portefeuilles de moins de 1 000 € plus importante que chez les femmes, tandis que c’est l’inverse pour les classes d’âge suivantes\. Les investisseurs actifs hommes ont une proportion de portefeuilles entre 10 000 € et 50 000 € plus importante que la population d’investisseurs actifs femmes\.

### Répartition de la valorisation médiane des portefeuilles constitués depuis 2018 en actions françaises et ETF français à fin décembre 2022

*Source : AMF*

 L’étude des valorisations des portefeuilles permet de constater la croissance des valorisations selon les tranches d’âge\. On constate par ailleurs que, pour toutes les tranches d’âge à l’exception des moins de 24 ans, la valorisation des portefeuilles est plus élevée pour les hommes que pour les femmes\.

# <a id="_TOC_250000"></a>PROFILS TYPES

Dans cette partie sont présentés les profils médians des investisseurs, selon leurs classes d’âge et sexes\. Pour chacun de ces profils, les caractéristiques suivantes relatives à leur classe d’âge et sexe sont présentées :

	un prénom, correspondant à celui le plus représenté parmi les investisseurs actifs en 2022 ;

	un nombre de transactions annuel, correspondant à la médiane du nombre de transactions sur tous types d’instruments par investisseur effectué sur l’année 2022 ;

	un nombre d’instruments traités annuel, correspondant à la médiane du nombre distinct d’instruments \(sur tous types d’instruments\) traités par investisseur sur l’année 2022 ;

	un montant en euros par transaction sur actions, correspondant à la médiane des montants par transaction sur actions effectués en 2022 ;

	un portefeuille, correspondant à la valorisation médiane des portefeuilles \(en actions et ETF\) à la fin de l’année 2022 ;

	les actions les plus traitées, en se basant sur le nombre d’investisseurs distincts au cours de l’année 2022\.

# 24 ANS OU MOINS

Les femmes représentent 16% des investisseurs actifs de moins de 25 ans, contre 84% pour les hommes, c’est la tranche d’âge où les femmes sont les moins représentées, mais où les montants de leurs transactions sont trois fois plus élevés et leurs portefeuilles plus de deux fois supérieurs à ceux des hommes\.

__LEA__

__24 ANS OU MOINS__

__THOMAS__

__24 ANS OU MOINS__

2 transactions/an

1 instrument traité/an

312 €/transaction sur actions

4 transactions/an

2 instruments traités/an

103 €/transaction sur actions

Portefeuille : 1 090 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, LVMH*

Portefeuille : 585 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, Crédit Agricole*

Les femmes représentent 17,9% des investisseurs de cette tranche d’âge contre 82,1% pour les hommes\.

__MARIE__

__25 A 29 ANS__

__ALEXANDRE__

__25 A 29 ANS__

3 transactions/an

2 instruments traités/an

240 €/transaction sur actions

5 transactions/an

3 instruments traités/an

171 €/transaction sur actions

Portefeuille : 956 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, LVMH*

Portefeuille : 1 303 €

Actions les plus traitées:

*TotalEnergies, Air Liquide, Crédit Agricole*

# 30 A 34 ANS

Les femmes représentent 17,9% des investisseurs de cette tranche d’âge contre 82,1% d’hommes\.

__MARIE__

__30 A 34 ANS__

__NICOLAS__

__30 A 34 ANS__

3 transactions/an

2 instruments traités/an

386 €/transaction sur actions

6 transactions/an

3 instruments traités/an

366 €/transaction sur actions

Portefeuille : 1 324 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, LVMH*

Portefeuille : 1 917 €

Actions les plus traitées:

*TotalEnergies, Air Liquide, Crédit Agricole*

# 35 A 39 ANS

Les femmes représentent 18,9% des investisseurs de cette tranche d’âge contre 81,1 % pour les hommes\.

__AURELIE__

__35 A 39 ANS__

__JULIEN__

__35 A 39 ANS__

3 transactions/an

2 instruments traités/an

498 €/transaction sur actions

6 transactions/an

3 instruments traités/an

494 €/transaction sur actions

Portefeuille : 1 802 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, Crédit Agricole*

Portefeuille : 2 563 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, Orpea*

Les femmes représentent 21% des investisseurs de cette tranche d’âge contre 79% pour les hommes\.

__CELINE__

__40 A 44 ANS__

__NICOLAS__

__40 A 44 ANS__

3 transactions/an

2 instruments traités/an

600 €/transaction sur actions

6 transactions/an

3 instruments traités/an

625 €/transaction sur actions

Portefeuille : 2 309 €

Actions les plus traitées :

*TotalEnergies, Air Liquide, Crédit Agricole*

Portefeuille : 3 475 €

Actions les plus traitées :

*TotalEnergies, Crédit Agricole, Air Liquide*

# 45 A 49 ANS

Les femmes représentent 23,9% des investisseurs de cette tranche d’âge contre 76,1% pour les hommes\.

__STEPHANIE__

__45 A 49 ANS__

__SEBASTIEN__

__45 A 49 ANS__

3 transactions/an

2 instruments traités/an

824 €/transaction sur actions

6 transactions/an

3 instruments traités/an

761 €/transaction sur actions

Portefeuille : 3 302 €

Actions les plus traitées :

*TotalEnergies, EDF, Air Liquide*

Portefeuille : 4 366 €

Actions les plus traitées :

*TotalEnergies, Crédit Agricole, Orpea*

# 50 A 54 ANS

Les femmes représentent 27,1% des investisseurs de cette tranche d’âge contre 72,9% pour les hommes\.

__NATHALIE__

__50 A 54 ANS__

__LAURENT__

__50 A 54 ANS__

3 transactions/an

2 instruments traités/an

1 045 €/transaction sur actions

6 transactions/an

3 instruments traités/an

985 €/transaction sur actions

Portefeuille : 3 833 €

Actions les plus traitées :

*TotalEnergies, EDF, Air Liquide*

Portefeuille : 4 983 €

Actions les plus traitées :

*TotalEnergies, Crédit Agricole, EDF*

Les femmes représentent 31,1% des investisseurs de cette tranche d’âge contre 68,9% pour les hommes\. A partir de cette tranche d’âge, les hommes effectuent moins de transactions\.

__ISABELLE__

__55 A 59 ANS__

__PHILIPPE__

__55 A 59 ANS__

3 transactions/an

2 instruments traités/an

978 €/transaction sur actions

5 transactions/an

2 instruments traités/an

1 040 €/transaction sur actions

Portefeuille : 4 346 €

Actions les plus traitées :

*EDF, TotalEnergies, Air Liquide*

Portefeuille : 6 100 €

Actions les plus traitées :

*TotalEnergies, EDF, Crédit Agricole*

# 60 A 64 ANS

Les femmes représentent 33,7% des investisseurs de cette tranche d’âge contre 66,3% pour les hommes\.

__CATHERINE__

__60 A 64 ANS__

__JEAN__

__60 A 64 ANS__

3 transactions/an

2 instruments traités/an

1 197 €/transaction sur actions

5 transactions/an

2 instruments traités/an

1 094 €/transaction sur actions

Portefeuille : 4 505 €

Actions les plus traitées :

*EDF, TotalEnergies, Air Liquide*

Portefeuille : 7 039 €

Actions les plus traitées :

*TotalEnergies, EDF, Crédit Agricole*

# 65 A 69 ANS

Les femmes représentent 36% des investisseurs actifs de cette tranche d’âge contre 64% pour les hommes\. L’écart de valorisation des portefeuilles entre sexe est le plus élevé sur cette tranche d’âge\.

A partir de cette tranche d’âge, les femmes effectuent moins de transactions\.

__MARIE__

__65 A 69 ANS__

__JEAN__

__65 A 69 ANS__

2 transactions/an

2 instruments traités/an

983 €/transaction sur actions

4 transactions/an

2 instruments traités/an

1 133 €/transaction sur actions

Portefeuille : 5 038 €

Actions les plus traitées :

*EDF, TotalEnergies, Air Liquide*

Portefeuille : 7 404 €

Actions les plus traitées :

*TotalEnergies, EDF, Air Liquide*

Les femmes représentent 38,6% des investisseurs actifs de cette tranche d’âge contre 61,4% pour les hommes\.

__MARIE__

__70 A 74 ANS__

__JEAN__

__70 A 74 ANS__

2 transactions/an

2 instruments traités/an

1 156 €/transaction sur actions

4 transactions/an

2 instruments traités/an

1 111 €/transaction sur actions

Portefeuille : 5 833 €

Actions les plus traitées :

*EDF, TotalEnergies, Air Liquide*

Portefeuille : 7 873 €

Actions les plus traitées :

*EDF, TotalEnergies, Air Liquide*

# 75 ANS OU PLUS

Les femmes représentent 49,3% des investisseurs actifs de cette tranche d’âge contre 50,7% pour les hommes\. Sur cette classe d’âge, l’activité des investisseurs n’est pas liée à des transferts de titres entre comptes, contrairement à ce qui pourrait être supposé, mais bien à de nouvelles transactions\.

__MARIE__

__75 ANS ET PLUS__

__JEAN__

__75 ANS ET PLUS__

2 transactions/an

1 instrument traité/an

1 397 €/transaction sur actions

3 transactions/an

2 instruments traités/an

1 325 €/transaction sur actions

Portefeuille : 9 612 €

Actions les plus traitées :

*EDF, Crédit Agricole, TotalEnergies*

Portefeuille : 10 921 €

Actions les plus traitées :

*EDF, TotalEnergies, Air Liquide*

# ANNEXE : DONNEES DE L’ETUDE

Les régulateurs européens collectent, au titre de l’article 26 du règlement MiFIR, les déclarations des transactions sur instruments financiers exécutées par les entreprises d’investissement européennes\.

Entre les déclarations reçues par l’AMF directement de la part des entreprises d’investissement françaises et celles reçues via le système de routage entre autorités européennes, l’AMF dispose de l’exhaustivité des transactions sur les deux périmètres suivants :

- transactions réalisées par une entreprise d’investissement établie sur le sol français \(y compris succursales françaises d’entreprises d’investissement étrangères\) ;
- transactions réalisées sur un instrument sous compétence de l’AMF ou dont le sous\-jacent est sous compétence de l’AMF\.

C’est un sous\-ensemble de ce dernier périmètre qui a été choisi pour la présente étude, dans la mesure où il permet d’obtenir à la fois une base stable et l’exhaustivité des transactions sur les instruments sous compétence de l’AMF\.

Ainsi, cette étude se base spécifiquement sur __les transactions effectuées par les investisseurs particuliers de nationalité française sur un instrument ou sous\-jacent sous compétence de l’AMF__\.

Par ailleurs, afin de présenter un portrait type de l’investisseur français et d’y inclure une vision par sexe, __les transactions effectuées sur des comptes joints n’ont pas été incluses__15, puisqu’il n’est pas possible de distinguer l’activité de chacun des titulaires du compte\. Ainsi, les 5,1 millions de transactions réalisées à travers des comptes joints en 2022, soit 11,2% des transactions, ont été exclues de l’analyse\.

Concernant le sexe des investisseurs français, le code d’identification reçu au titre du règlement MiFIR ne permet pas de l’identifier\. C’est pourquoi le sexe de chaque investisseur a été déduit du prénom, à partir du référentiel des prénoms de l’INSEE16\. Pour cela, nous avons pris le parti de dresser une correspondance entre prénom et sexe, en excluant des données de l’étude les transactions pour lesquelles le sexe de l’investisseur ne pouvait être déduit du prénom avec un niveau de confiance de 98%\. Ainsi, pour un prénom donné, si la population se répartit entre hommes et femmes et qu’il existe plus de 2% de population pour chaque sexe et donc moins de 98% pour l’un des deux, les données relatives aux transactions des personnes portant ce prénom ont été exclues\. Au final, sur le périmètre, cette méthodologie aura permis de déterminer le sexe de 1,43 million d’investisseurs français \(seuls 30 000 investisseurs sont exclus faute de correspondance entre le prénom et le référentiel INSEE et 50 000 du fait de la mixité des prénoms, soit moins de 6% de la population des investisseurs\)\.

#### Cette étude porte ainsi sur 1,43 million d’investisseurs ayant réalisé 40,2 millions de transactions en 2022\.

15 Dans les études précédemment publiées par l’AMF, il a été choisi de considérer qu’un compte joint ouvert pour deux personnes correspondait à deux investisseurs\. En effet, les études relatives à la clientèle de détail sont à comparer avec des données statistiques fournies à l’échelle individuelle et non à l’échelle du compte joint, qui n’a d’existence qu’au niveau de l’établissement qui l’a ouvert\. Concernant les transactions, il a été considéré qu’une transaction déclarée correspondait à deux transactions \(dont le montant est divisé par deux\)\. Cette méthode a permis d’attribuer une transaction à chacun des détenteurs du compte et ainsi considérer individuellement les investisseurs dans leurs comportements\.

16 Fichier des prénoms basé sur l’état civil et publié le 20 juin 2022 : https://[www\.insee\.fr/fr/statistiques/2540004?sommaire=4767262&q=pr%C3%A9noms\#consulter\.](http://www.insee.fr/fr/statistiques/2540004?sommaire=4767262&q=pr%C3%A9noms&consulter)

Il convient également de souligner que la présente étude se base sur les transactions réalisées et donc sur les investisseurs ayant effectué au moins une transaction à l’achat ou à la vente au cours de la période \(les investisseurs « actifs »\)\. Le nombre d’investisseurs actifs ne tient donc pas compte des investisseurs n’ayant pas effectué de transaction mais détenant des actions\.

#### Pour ces raisons, le nombre d’investisseurs actifs diffère fortement des investisseurs pour lesquels le taux de détention d’actions en direct est estimé par l’institut Kantar à 6,7% des Français de 15 ans et plus en mars 2022 \(cf\. la Lettre de l’Observatoire de l’épargne de l’AMF n°4917\)\.

17 https://[www\.amf\-france\.org/sites/institutionnel/files/private/2022\-06/LOE%2049\.pdf](http://www.amf-france.org/sites/institutionnel/files/private/2022-06/LOE%2049.pdf)

