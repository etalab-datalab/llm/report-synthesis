import os
import mammoth
from html_parser import enlever_tableaux_et_balises, extraire_titres_sous_titres_texte_entier
import re
import pandas as pd
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings import HuggingFaceEmbeddings
from langchain_community.document_loaders import DataFrameLoader
from langchain.embeddings import HuggingFaceEmbeddings
from llama_index.embeddings.langchain import LangchainEmbedding
from RAG_preprocess import split_text_into_chunks, process_dataframe, process_docx_files, convert_docx_to_md
from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain.chains import (
    StuffDocumentsChain, LLMChain, ConversationalRetrievalChain
)
from langchain_community.vectorstores import FAISS
import platform

system_info = platform.system()

if system_info != "Darwin":
    print("Le système d'exploitation est linux.")
    # Ajoutez ici le code que vous souhaitez exécuter si le système est macOS
    from vllm import LLM, SamplingParams
    sampling_params = SamplingParams(temperature=0.2, top_p=0.95, max_tokens=1000)


from langchain_core.prompts import PromptTemplate
from summary_inference import prompt_generator, generate_summary , one_level_summary, iterative_summary_reduction
from dataframe_management import concate_chapter


prompt1 = """<|im_start|>system \n Tu es un agent de l'administration qui doit synthétiser l'information sur un sujet précis. <|im_end|> \n
<|im_start|>user \n Voici des éléments de contexte: """

prompt2 = """\n En utilisant les éléments de contexte et uniquement ça, fais un résumé sur le sujet suivant : \n"""  

prompt3 = """<|im_end|> \n <|im_start|>assistant"""



def dossier_docx_summary_RAG(chemin_dossier, llm, output_csv, sampling_params, max_summ_size = 2000, hf = False, suffixe = ''):
    # Vérifier si le dossier existe

    chemin_fichier_markdown = "input/sample.md"
    if not os.path.exists(chemin_dossier):
        print(f"Le dossier {chemin_dossier} n'existe pas.")
        return
    

    RAG_df = pd.DataFrame()

    # Itérer sur les fichiers du dossier d'origine
    for fichier in os.listdir(chemin_dossier):
        chemin_fichier = os.path.join(chemin_dossier, fichier)

        # Vérifier si le fichier est de type DOCX
        if fichier.lower().endswith('.docx'):

            # Convertir le fichier DOCX en md
            #try:

                with open(chemin_fichier, "rb") as docx_file:
                    result = mammoth.convert_to_markdown(docx_file)
                with open(chemin_fichier_markdown, "w") as markdown_file:
                    markdown_file.write(result.value)

                df = extraire_titres_sous_titres_texte_entier(chemin_fichier_markdown)
                max_level = df['niveau'].max()

                RAG_df = pd.concat([RAG_df, df])
                
                for i in range(max_level,1,-1):
                    df = one_level_summary(df, i, llm, sampling_params, hf=hf)
                    df = iterative_summary_reduction(df, i, llm, sampling_params, summ_size = max_summ_size, max_iteration=2)
                    df = concate_chapter(df)
                    RAG_df = pd.concat([RAG_df, df.loc[df['niveau']==i]])

                    
                df = one_level_summary(df, 1, llm, sampling_params,hf=hf) 
                df = iterative_summary_reduction(df, 1, llm, sampling_params, summ_size = max_summ_size, max_iteration=2)
                df['titre_doc'] = chemin_fichier
                
                RAG_df = pd.concat([RAG_df, df.loc[df['niveau']==1]])
                print(f"Résumé réussie : {fichier}")
            #except Exception as e:
            #    print(f"Erreur lors du résumé de {fichier} : {e}")


    # Enregistrer le DataFrame en format CSV
    RAG_df.to_csv(output_csv, index=False)
    return RAG_df

def RAG_prompt_generator(docs, query, prompt1 = prompt1, prompt2 = prompt2, prompt3 = prompt3):

    doc_prompt = ''
    for doc in docs:
        doc_prompt = doc_prompt + doc.page_content
    return prompt1 + doc_prompt + prompt2 + query + prompt3


def generate_RAG_linux(prompt, llm, sampling_params):

    summaries = []
    outputs = llm.generate(prompt,sampling_params)

    for output in outputs:
        generated_text = output.outputs[0].text
        summaries.append(generated_text)
    return summaries

def generate_RAG_Mac(prompt, llm):
    summaries = []
    for prom in prompt:
        sum = llm.invoke(prom)
        summaries.append(sum)
    return summaries


def generate_RAG(prompt, llm, sampling_params = None):

    system_info = platform.system()
    if system_info != "Darwin":
        return generate_RAG_linux(prompt, llm, sampling_params)  
    else : 
        return generate_RAG_Mac(prompt, llm)