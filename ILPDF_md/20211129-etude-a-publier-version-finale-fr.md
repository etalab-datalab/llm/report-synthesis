# NOVEMBRE 2021

LES INVESTISSEURS PARTICULIERS ET LEUR ACTIVITÉ DEPUIS LA CRISE COVID : PLUS JEUNES, PLUS NOMBREUX ET ATTIRÉS PAR DE NOUVEAUX ACTEURS

# amf\-france\.org



__EDOUARD CHATILLON, MICHEL DEGRYSE, STEPHEN FRENAY__

*Cette étude a été réalisée par la Direction de la supervision des intermédiaires et infrastructures de marché\. Elle repose sur l’utilisation de sources considérées comme fiables mais dont l’exhaustivité et l’exactitude ne peuvent être garanties\. Les données chiffrées ou autres informations ne sont valables qu'à la date de publication du rapport et sont sujettes à modification dans le temps\.*

*Toute copie, diffusion et reproduction de cette étude, en totalité ou partie, sont soumises à l’accord exprès, préalable et écrit de l’AMF\.*

# SYNTHÈSE

En avril 2020, l’Autorité des marchés financiers \(AMF\) publiait une étude sur le comportement des investisseurs particuliers pendant la crise COVID\-191\. Cette étude, effectuée sur la base du reporting des transactions issu de la directive MIF2 \(voir encadré ci\-après\), avait permis de constater l’importance des volumes traités par les particuliers pendant les six semaines de forte volatilité des marchés observée entre février et mars 2020\. Ces transactions émanaient d’investisseurs existants, mais aussi, pour une part significative, de nouveaux investisseurs\.

Cette nouvelle étude, assise uniquement sur les transactions sur les instruments financiers relevant de la compétence de l’AMF, permet de constater que ce regain d’intérêt des investisseurs particuliers pour les marchés financiers ne s’est pas essoufflé depuis, puisque l’activité de ces derniers est restée comparable à son niveau le plus élevé constaté en mars 2020\.

Cette activité, qui était essentiellement captée par les acteurs traditionnels \(banques et courtiers en ligne\), est désormais partagée avec un nouveau type d’intermédiaire, dont les offres se démarquent en matière de fonctionnalités nouvelles et de frais de courtage : les « neo\-brokers », à ce jour tous implantés hors de France mais servant des particuliers français dans le cadre de la libre prestation de services \(passeport européen\)\.

Cette étude permet d’observer la percée des neo\-brokers auprès des nouveaux investisseurs particuliers, en proportion de plus en plus importante par rapport aux autres catégories d’intermédiaires\. Elle révèle des profils et des comportements significativement différents entre les clientèles de ces différentes catégories d’intermédiaires\.

Parmi les principales tendances observées, il ressort :

- un rajeunissement substantiel des investisseurs actifs depuis le 1er trimestre 2020, les neo\-brokers concentrant une clientèle significativement plus jeune que les autres catégories d’intermédiaires ;
- des clients des neo\-brokers plus enclins à la négociation d’instruments complexes et, parmi les actions, de titres plus volatils que la clientèle des autres types d’intermédiaires ;
- une fréquence de transaction significativement plus élevée pour la clientèle des neo\-brokers, mais sur des ordres de plus petite taille que celles observées pour la clientèle des autres catégories d’intermédiaires\.

Ainsi, même sur des titres financiers correspondant à des investissements traditionnels, entrant dans le champ de compétence de l’AMF, l’activité des clients des neo\-brokers se distingue nettement de celles des autres catégories d'intermédiaires\.

Or, comme l’encadré de la page suivante le montre, seules les transactions des clients français portant sur des instruments financiers pour lesquels la France est le marché pertinent ou sur des instruments financiers étrangers négociés auprès d’intermédiaires établis en France sont portés à la connaissance de l’AMF de manière directe et automatisée\. Les transactions des investisseurs particuliers français sur des instruments financiers étrangers négociés auprès d’un intermédiaire étranger échappent ainsi au reporting systématique effectué à l’AMF et doivent être demandées à l’autorité nationale compétente\.

Il conviendrait pour l’AMF de bénéficier d’une vision exhaustive afin d’exercer pleinement son rôle de protection et d’éducation des investisseurs particuliers français\.

1 [https://www\.amf\-france\.org/sites/default/files/2020\-04/investisseurs\_particuliers\_actions\_mars\_2020\.pdf](https://www.amf-france.org/sites/default/files/2020-04/investisseurs_particuliers_actions_mars_2020.pdf)

## Encadré : Données utilisées dans le cadre de cette étude

Les régulateurs européens collectent au titre de l’article 26 du règlement MiFIR2 les déclarations sur transactions sur instruments financiers \(IF\) exécutées par les entreprises d’investissement européennes \(directement ou au travers d’un dispositif de déclaration agréé3\)\.

Entre les déclarations reçues par l’AMF directement de la part des entreprises d’investissement françaises et celles qui sont reçues via le système de routage entre autorités européennes \(TREM4\), l’AMF dispose de l’exhaustivité des transactions sur les deux périmètres suivants :

- transactions réalisées par une entreprise d’investissement établie sur le sol français \(y compris succursales françaises d’entreprises d’investissement étrangères\) ;
- transactions réalisées sur un instrument sous compétence de l’AMF ou dont le sous\-jacent est sous compétence de l’AMF \(ci\-après « IF FR »\)\.

## C’est ce périmètre, plus étendu que celui du Tableau de bord des investisseurs particuliers5 publié par ailleurs par l’AMF, qui a été choisi pour la présente étude\.

Le tableau ci\-dessous résume le périmètre des transactions pour lesquelles une déclaration est reçue par l’AMF :

Entreprises d’investissement établies en France

Entreprises d’investissement établies hors de France

IF FR

Autres IF\*

IF FR\*\*

Autres IF

Particuliers français

OUI

OUI

OUI

NON\*\*\*

Particuliers non français

OUI

OUI

OUI

NON

Ce tableau appelle les commentaires et compléments d’informations suivants :

\* Les transactions sur ces IF sont routées par l’AMF, via TREM, à l’autorité compétente du marché le plus pertinent en termes de liquidité\.

\*\* Les transactions sur ces IF sont routées par l’autorité du déclarant, via TREM, à l’AMF en tant qu’autorité compétente du marché le plus pertinent en termes de liquidité\.

\*\*\* Ces transactions, bien que concernant des investisseurs français, ne sont pas visibles de l’AMF\.

L’AMF ne dispose donc pas à ce jour, de manière directe et automatisée6, des transactions réalisées par les investisseurs particuliers français, sur instruments n’étant pas de compétence française au titre du règlement européen MAR sur les abus de marché, lorsque ces investisseurs utilisent les services d’entreprises d’investissement non établies en France \(ex\. un français qui traiterait une action américaine auprès d’un intermédiaire établi dans un autre pays européen\)\. Or, selon un de ces neo\-brokers étrangers, les actions les plus traitées par ses clients français au 3e trimestre 2021 étaient des actions étrangères, parmi lesquelles des titres très volatils, en dehors de la compétence de l’AMF\.

La présente étude porte sur 218 millions de transactions réalisées entre le 3e trimestre 2018 et le 3e trimestre 2021 par les investisseurs particuliers, français ou étrangers, __sur les instruments financiers dont l’AMF est l’autorité compétente du marché le plus pertinent en termes de liquidité__\.

2 Règlement \(UE\) N° 600/2014 du Parlement Européen et du Conseil du 15 mai 2014 concernant les marchés d’instruments financiers et modifiant le règlement \(UE\) no 648/2012\.

3 ARM – *Approved Reporting Mechanism*\.

4 TREM ou *Transaction Reporting Exchange Mechanism *est l’outil informatique mis en place par l’ESMA afin que les autorités compétentes se communiquent les transactions qui relèvent de leur compétence, lorsque ces transactions sont réalisées par des entreprises d’investissement que ces autorités compétentes ne supervisent pas\.

5 Le Tableau de bord des investisseurs particuliers porte sur le périmètre plus restreint des seules déclarations soumises par les entreprises d’investissement établies en France\.

6 Ces données peuvent être obtenues auprès de l’autorité nationale compétente sur demande\.

# ACTIVITÉ DES INVESTISSEURS PARTICULIERS

## ÉVOLUTION DE L’ACTIVITÉ DES INVESTISSEURS PARTICULIERS

Comme l’indique le graphique ci\-dessous, les investisseurs particuliers ont plus que doublé leur activité mensuelle en mars 2020 lors de la crise COVID\-19 et ont continué de négocier sur les instruments financiers à un rythme élevé avec un nombre de transactions qui n’est pas revenu au niveau pré\-COVID\-19\.

__Nombre de transactions effectuées par des investisseurs particuliers__



Si les instruments sous compétence de l’AMF traités par des investisseurs particuliers le sont majoritairement par des investisseurs de nationalité française \(la répartition des transactions en montant, pour les actions seulement, se situe à 72 % pour les investisseurs français et 28 % pour les investisseurs étrangers\), la part de transactions réalisées par des investisseurs non français croît dans le temps \(41 % au 3e trimestre 2021 contre 33 % au 3e trimestre 2018\)\.

Le graphique ci\-dessous, exclusivement sur le périmètre des actions et droits contrairement au reste de l’étude qui porte sur tous les instruments financiers sous compétence de l’AMF, montre que la forte chute des valeurs du CAC 40 en mars 2020 a été un point d’entrée pour les investisseurs particuliers, qui présentent un solde mensuel de transactions nettement acheteur entre mars 2020 et juin 2020\.

Exception notable, le constat d’un solde nettement vendeur sur le mois de novembre 2020 coïncide avec la forte hausse du marché actions survenue à la suite des annonces des résultats positifs des vaccins contre le COVID\-19\.

__Solde net mensuel \(achats – ventes\) des montants traités par les investisseurs particuliers sur actions françaises, en euros__



## ÉVOLUTION DU NOMBRE D’INVESTISSEURS PARTICULIERS

Le graphique ci\-dessous présente l’évolution du nombre d’investisseurs particuliers actifs par trimestre\. On entend par investisseur « actif » un particulier ayant réalisé au moins une transaction sur les instruments sous compétence française pendant le trimestre concerné\.

La notion de « nouvel investisseur » correspond quant à elle aux investisseurs qui n’avaient jusqu’à présent pas négocié de transactions rapportées à l’AMF au titre de MiFIR depuis janvier 2018, date de mise en œuvre de ce reporting\. Par conséquent, les investisseurs ayant déjà réalisé des transactions avant cette date seront dans le cadre de cette étude considérés comme de « nouveaux investisseurs »\.

__Nombre d’investisseurs particuliers actifs par trimestre, dont nouveaux investisseurs__



Plusieurs observations notables se dégagent de ce graphique :

- si le nombre d’investisseurs actifs par trimestre stagnait autour d’un million jusqu’au 3e trimestre 2019, un premier apport élevé de nouveaux investisseurs est observé au 4e trimestre 2019, lié à l’introduction en bourse du titre Française des jeux ;
- depuis la crise sanitaire, des apports tout aussi importants de nouveaux investisseurs particuliers ont ensuite accru le nombre d’investisseurs particuliers actifs sur les marchés, dans un contexte marqué par les confinements et l’augmentation du télétravail ;
- enfin, cet attrait pour les marchés financiers ne s’est pas interrompu avec la reprise du travail en présentiel dans une période de plus forte volatilité des marchés financiers, avec un nombre de particuliers actifs qui a doublé à partir de 2020 et qui s’est maintenu sur des niveaux élevés \(plus de 2,5 millions\)\.

Par ailleurs, l’évolution du nombre d’investisseurs historiques et nouveaux, comparée à celle du nombre de transactions de ces catégories d’investisseurs, permet de constater que les nouveaux investisseurs exécutent proportionnellement moins de transactions que les investisseurs historiques7\.

7 Comparaison du graphe ci\-dessus avec celui fourni en annexe 1\.

## INSTRUMENTS FINANCIERS TRAITÉS PAR LES INVESTISSEURS PARTICULIERS

Comme le montre le graphique suivant, les transactions sur actions représentent 75 % du nombre de transactions total\. Ce chiffre atteint 85,5 % en prenant en compte les transactions sur ETF \(*Exchange Traded Funds *ou fonds indiciels cotés\)\. L’ensemble d’instruments catégorisés comme « non\-complexes » dans le cadre de cette étude \(actions et droits, ETF et obligations\) représente 87,5 % des transactions réalisées par des investisseurs particuliers\. Il est toutefois intéressant de noter que les ETF les plus traités par les investisseurs particuliers sont ceux qui offrent un effet de levier sur indice, signe de l’utilisation par les investisseurs particuliers de produits à risque plus élevé8\. En revanche, les transactions réalisées sur les obligations et autres produits de dette sont en faible nombre\.

Sur les 12,5 % de transactions restantes, ce sont les transactions sur dérivés actions, majoritairement des contrats financiers pour différence – CFD ou *Contracts For Difference *\- et warrants qui dominent\.

Cette répartition par type d’instruments financiers ne présente pas d’évolution significative au cours du temps sur le périmètre du reporting reçu par l’AMF\.

__Répartition des transactions des investisseurs particuliers par type d’instrument financier__



Sur le périmètre des actions exclusivement, on constate que:

- les cinq actions les plus négociées \(en montant\) par les investisseurs particuliers sont les suivantes :

__Actions__

__Volume des transactions en M€__

__Poids des transactions des investisseurs particuliers dans le volume échangé__

Airbus

26 243

5,85 %

TotalEnergies

23 259

5,19 %

LVMH

16 229

3,62 %

Société générale

13 768

3,07 %

BNP Paribas

13 065

2,91 %

- la répartition des transactions par secteur d’activité fluctue de manière notable au cours du temps pour les investisseurs particuliers en comparaison avec l’ensemble des transactions effectuées sur Euronext, mais un secteur se démarque sur la période d’étude pour les investisseurs particuliers par rapport à l’ensemble des transactions, celui de la santé9\.

8 Sans que nous soyons en mesure de préciser la proportion de ces transactions réalisée à des fins de couverture\.

9 Cf\. annexe 2\.

# ÉVOLUTION DE L’INTERMEDIATION DES INVESTISSEURS PARTICULIERS : L’IMPORTANCE CROISSANTE DES NEO\-BROKERS

L’étude des transactions des investisseurs particuliers révèle également une évolution en ce qui concerne les intermédiaires auxquels ces derniers ont recours afin d’exécuter leurs transactions\.

L’étude de cette évolution a amené les services de l’AMF à distinguer trois catégories d’intermédiaires, établies à dire d’expert, et distinguant ainsi les entreprises d’investissement en trois catégories :

- Les banques dites classiques;
- Les banques en ligne, qui incluent également les courtiers en ligne traditionnels;
- Les neo\-brokers\.

La liste par ordre alphabétique des entités représentant plus de 0,5 % du total des transactions analysées sur l’ensemble de la période est fournie en annexe 3, ainsi que la catégorisation de chaque acteur\.

En ce qui concerne la dernière catégorie, elle se distingue avec des offres présentées comme « gratuites10 » ou supposées accessibles à tous comme le « *copy\-trading *»\. Il est par ailleurs utile de préciser que les banques dites classiques comme les banques en ligne sont situées en France ou au sein de l’Union Européenne, tandis que les neo\-brokers sont à ce jour tous situés en dehors de France, au sein de l’Union Européenne\.

## ÉVOLUTION DES TRANSACTIONS CAPTÉES PAR TYPE D’INTERMÉDIAIRE

Comme l’indique le graphique suivant, on note la percée des neo\-brokers en terme de nombre de transactions d’investisseurs particuliers passées par leur intermédiaire, doublant ainsi leur activité\. Cet accroissement d’activité auprès des neo\-brokers se fait, en termes de parts de marché, à un rythme différent des banques dites classiques et des banques en ligne\.

__Nombre de transactions par trimestre et par catégorie d’intermédiaire__



10 Les offres présentées comme « zéro frais » soulèvent généralement des problématiques d’information des clients sur les coûts et charges, et peuvent s’accompagner de pratiques dites de paiement pour flux d’ordres \(« *Payment for order flow *»\), qui ont récemment fait l’objet d’une communication par l’ESMA : [https://www\.esma\.europa\.eu/press\-news/esma\-news/esma\-](https://www.esma.europa.eu/press-news/esma-news/esma-warns-firms-and-investors-about-risks-arising-payment-order-flow) [warns\-firms\-and\-investors\-about\-risks\-arising\-payment\-order\-flow\.](https://www.esma.europa.eu/press-news/esma-news/esma-warns-firms-and-investors-about-risks-arising-payment-order-flow)

En complément, il est utile de noter que les transactions des investisseurs particuliers sur instruments financiers sous compétence AMF \(périmètre de cette étude\) sont réalisées par des prestataires qui ne sont pas établis sur le territoire français dans les proportions suivantes :

- 31 % en ce qui concerne les banques classiques, quasi\-exclusivement du fait d’investisseurs étrangers ;
- 15 % pour les banques en ligne, quasi\-exclusivement du fait d’investisseurs étrangers ;
- 100 % en ce qui concerne les neo\-brokers et ce, par des investisseurs français pour 43 % et 57 % par des investisseurs étrangers\.

## ÉVOLUTION DU NOMBRE DE CLIENTS ACTIFS PAR TYPE D’INTERMÉDIAIRE

En ce qui concerne les nouveaux investisseurs sur les instruments financiers de compétence AMF, le graphique suivant met en évidence qu’en deux ans, entre le 3e trimestre 2019 et le 3e trimestre 2021, la clientèle active de chaque catégorie d’établissement est passée : de 712 600 clients actifs à plus de 1 200 000 clients pour le banques classiques \(\+68 %\), de 214 600 clients actifs à plus de 520 500 clients pour les banques en ligne \(\+142 %\) et de 68 000 clients actifs à plus de 409 400 clients pour les neo\-brokers \(\+502 %\)\.

Il convient toutefois de rappeler que la forte hausse de la clientèle des banques classiques au 4e trimestre 2019 s’explique principalement11 par l’introduction en bourse du titre de la Française des Jeux, qui a attiré de nombreux nouveaux investisseurs exclusivement auprès des banques classiques et des banques en ligne, les neo\-brokers n’ayant pas proposé à leur clientèle de participer à cette introduction\.

__Nombre de clients actifs par trimestre et par catégorie de broker, avec comptages des nouveaux clients__



Une analyse plus détaillée montre que la proportion de la clientèle de neo\-brokers qui effectue des transactions auprès d’autres types d’intermédiaires se situait aux alentours de 10 % en 2018 et baisse légèrement pour atteindre 7,5 % en 2021\.

11 Près de 400 000 nouveaux clients ont souscrit à l’offre de la Française des jeux sur 508 600 nouveaux clients de banque classique et 70 700 nouveaux clients de banque en ligne\.

## SEGMENTATION PAR CLASSE D’ÂGE DES CLIENTS DES INTERMÉDIAIRES

Comme le montre le graphique ci\-après, la population des investisseurs actifs s’est significativement rajeunie depuis le 3e trimestre 2018 quelle que soit la catégorie d’établissement considérée\.

La baisse de l’âge moyen de la population générale d’investisseurs actifs \(ligne noire\) est encore plus prononcée du fait de l’augmentation dans le temps des effectifs de la clientèle des neo\-brokers, qui a toujours été significativement plus jeune que la clientèle des autres catégories d’établissements et qui s’élève aujourd’hui à 36,8 ans\.

__Âge moyen des investisseurs particuliers actifs, par catégorie d’établissement d’investissement et par trimestre__



__Distribution des âges des investisseurs particuliers actifs au 3e trimestre 2021, par catégorie d’établissement d’investissement__



# DES PROFILS D’INVESTISSEURS DIFFÉRENTS PAR TYPE D’INTERMÉDIAIRE

Comme indiqué précédemment, l’âge moyen des investisseurs actifs diffère sensiblement selon les types d’intermédiaires concernés\. Pour autant, ces investisseurs se distinguent\-ils en matière d’investissements ?

## TYPES DE PRODUITS

Dans cette section, nous nous intéressons à la nature des instruments traités par les clientèles des différentes catégories d’intermédiaires\. À cette fin, l’étude distingue deux catégories d’instruments financiers :

- 
	- 
		- une première catégorie d’instruments non complexes12, qui réunit pour les besoins de l’analyse les actions et droits, obligations et ETF ;
		- une seconde catégorie dite instruments complexes qui englobe tout le reste \(majoritairement des dérivés action tels que CFD et warrants\)\.

__Répartition des clients actifs en fonction de la nature complexe ou non des produits traités__



Le graphique ci\-dessus montre que les clients des neo\-brokers s’avèrent plus enclins à la négociation sur des instruments complexes, mais que cette proportion a décliné dans le temps\. Ce recul peut s’expliquer par deux tendances\. Après avoir été choisis par des investisseurs plus orientés vers les produits complexes, les neo\-brokers conquièrent désormais une clientèle nouvelle s’orientant vers des produits simples\.

12 Certains de ces instruments peuvent toutefois ne pas être considérés comme « non complexes » au sens de l’article 25\.4 de la directive MiFID qui dispense dans certaines circonstances les entreprises d’investissement d’évaluer le caractère approprié des services fournis au regard des connaissances, compétences et de l’expérience des clients\. Voir aussi les « Guidelines on complex debt instruments and structured deposits » publiées par l’ESMA\.

__Actions les plus traitées en montant par type d’intermédiaire__

Rang

Toutes entités

Ratio

Banques classiques

Ratio

Banques en ligne

Ratio

Neo\-brokers

Ratio

1

Airbus

5,9 %

TotalEnergies

5,3 %

Airbus

5,0 %

Airbus

__16,3 %__

2

TotalEnergies

5,2 %

Airbus

4,7 %

TotalEnergies

4,6 %

Air France KLM

6,1 %

3

LVMH

3,6 %

LVMH

3,6 %

Société générale

4,3 %

__Novacyt__

5,1 %

4

Société générale

3,1 %

BNP Paribas

2,9 %

Renault

3,9 %

TotalEnergies

3,3 %

5

BNP Paribas

2,9 %

Société générale

2,7 %

Air France KLM

3,5 %

Renault

2,6 %

6

Renault

2,8 %

Air Liquide

2,5 %

BNP Paribas

3,3 %

__Valneva__

2,5 %

7

Air France KLM

2,5 %

Crédit Agricole

2,2 %

LVMH

2,4 %

LVMH

2,4 %

8

AXA

2,0 %

Renault

2,1 %

CGG

2,0 %

Société générale

1,8 %

9

Sanofi

1,9 %

Sanofi

2,1 %

Technip

1,7 %

BNP Paribas

1,4 %

10

Air Liquide

1,9 %

AXA

2,0 %

Crédit Agricole

1,7 %

__Mcphy Energy__

1,4 %

32 %

30 %

32 %

43 %

Le classement des dix actions les plus traitées par type d‘intermédiaire montre ainsi qu’à l’intérieur même des actions, les investisseurs des neo\-brokers favorisent des titres moins liquides, en dehors des indices majeurs\. L’importance des négociations sur le titre Airbus, appartenant au DAX et au CAC, par les clients des neo\-brokers s’explique entre autres par la forte proportion d’investisseurs particuliers étrangers, notamment allemands, ayant traité sur ce titre \(72 % de clients étrangers contre 28 % de clients français\)\.

## FRÉQUENCE DES TRANSACTIONS ET MONTANT MOYEN SUR ACTIONS

L’étude de la fréquence des transactions des investisseurs par type d’intermédiaire montre que les investisseurs passant par les neo\-brokers traitent toujours significativement plus que les investisseurs des autres catégories d’intermédiaires, même si la tendance au rajeunissement des nouveaux investisseurs a significativement pesé sur la fréquence moyenne de transactions ces deux dernières années\.

__Nombre moyen de transactions effectuées par des clients actifs, par trimestre, par catégorie d’entreprise d’investissement et selon la nationalité du client__



Cette fréquence de transaction plus élevée chez les neo\-brokers par des investisseurs plus jeunes s’accompagne d’un montant moyen de transaction beaucoup plus faible, témoignant ainsi d’une approche différente de l’investissement\.

__Montant des transactions sur actions__

Banques classiques

Banques en ligne

Neo\-brokers

Moyenne des montants de transaction

2 689€

2 651 €

689 €

Médiane des montants de transaction

1 319 €

942 €

61 €

## RÉPARTITION DES NÉGOCIATIONS AU COURS DE LA JOURNÉE

Enfin, le graphe suivant permet d’observer une répartition des négociations sur la journée éloignée de la répartition des volumes généralement observée sur les marchés13, les flux étant très concentrés lors de l’ouverture des marchés en Europe\. On constate que l’ouverture des marchés outre\-Atlantique \(15h30\) a un impact plus important sur le nombre de transactions réalisées par la clientèle des neo\-brokers que sur celle des autres entreprises d’investissement et, inversement, que les clients des neo\-brokers participent encore moins aux enchères de clôture\.

__Nombre de transactions réalisées par dizaine de minutes__



\*\*\*

13 Voir « Importance croissante du fixing de clôture dans les volumes négociés sur actions » AMF, octobre 2019 [https://www\.amf\-france\.org/fr/actualites\-publications/publications/rapports\-etudes\-et\-analyses/importance\-croissante\-du\-](https://www.amf-france.org/fr/actualites-publications/publications/rapports-etudes-et-analyses/importance-croissante-du-fixing-de-cloture-dans-les-volumes-negocies-sur-actions) [fixing\-de\-cloture\-dans\-les\-volumes\-negocies\-sur\-actions](https://www.amf-france.org/fr/actualites-publications/publications/rapports-etudes-et-analyses/importance-croissante-du-fixing-de-cloture-dans-les-volumes-negocies-sur-actions)

## ANNEXE 1 : NOMBRE DE TRANSACTION PAR TRIMESTRE ET PAR TYPE D'INVESTISSEUR

__Nombre de transactions des investisseurs particuliers, dont nouveaux investisseurs__



Pour rappel, la notion de « nouvel investisseur » correspond aux investisseurs qui n’avaient jusqu’à présent pas négocié de transactions rapportées à l’AMF au titre de MiFIR depuis janvier 2018, date de mise en œuvre de ce reporting\. Une fois réalisée une première transaction, un investisseur n’est donc plus considéré comme

« nouveau » sur les trimestres suivants\. Par opposition, la notion d’ «investisseur historique» couvre les investisseurs ayant réalisé au moins une transaction lors des trimestres précédents\. Un investisseur est considéré comme historique au 1er trimestre 2020 s’il a effectué au moins une transaction depuis janvier 2018\. Un investisseur est considéré comme nouveau s’il n’a effectué aucune transaction depuis janvier 2018\.

## ANNEXE 2 : RÉPARTITION DES TRANSACTIONS PAR SECTEUR D’ACTIVITÉ

Aux fins de cette étude, la classification ICB \(*Industry Classification Benchmark*\) a été utilisée afin d’identifier le secteur de chaque titre et comparer ainsi la répartition des transactions des investisseurs particuliers \(quels que soient les lieux d’exécution\) à celles effectuées sur Euronext, en considérant que les transactions sur Euronext représentent de manière appropriée la répartition des transactions par titre du marché dans son ensemble quels que soient les intervenants\.

__Répartition du volume des transactions des investisseurs particuliers en euros par secteur d’activité__



__Répartition du volume des transactions sur Euronext en euros par secteur d’activité, tous intervenants confondus \(donc y compris investisseurs institutionnels et intervenants pour compte propre\)__



On constate notamment sur ces graphiques la différence d’importance entre les transactions des investisseurs particuliers et celles conclues sur Euronext sur le secteur santé \(« Healthcare », en bleu clair\) au 1er et 2e trimestre 2020, puis au 4e trimestre 2020 et 1er trimestre 2021, traité plus abondamment par les investisseurs particuliers\.

## ANNEXE 3 : LISTE PAR ORDRE ALPHABÉTIQUE DES ENTITÉS REPRÉSENTANT PLUS DE 0,5 % DU TOTAL DES TRANSACTIONS ANALYSÉES

__Banques classiques__

__Banques en ligne__

__Neo\-brokers__

BNP Paribas

Arkea Direct

Activtrades

BPE

BinckBank N\.V\.

De Giro

Comdirect Bank

Bourse Direct

eToro

Commerzbank

Boursorama

Trade Republic

Credito Emiliano

Easybourse

Trading 212

DekaBank Deutsche Girozentrale

IG Markets

Groupe BPCE

ING

Groupe Crédit Agricole

Interactive Brokers

Groupe Crédit du Nord

Saxo Bank

Groupe Crédit Mutuel / CIC

KBC Securities

La Banque Postale

Max Heinr\. Sutor OHG

Nationale\-Nederlanden Bank N\.V\.

Rabobank

Société Générale

