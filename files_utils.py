import os
import aspose.words as aw
import mammoth
from html_parser import extraire_titres_sous_titres_texte_entier
from summary_inference import one_level_summary
from dataframe_management import concate_chapter
from export_summary import save_docx
import pandas as pd
from vllm import LLM, SamplingParams
from html_parser import extraire_titres_sous_titres_texte_entier
from summary_inference import one_level_summary, long_chapter_summary, iterative_summary_reduction
from docx import Document

sampling_params = SamplingParams(temperature=0.2, top_p=0.95, max_tokens=2000)

def convertir_dossier_docx_en_pdf(chemin_dossier):
    # Vérifier si le dossier existe
    if not os.path.exists(chemin_dossier):
        print(f"Le dossier {chemin_dossier} n'existe pas.")
        return

    # Créer un nouveau dossier avec _pdf à la fin du nom
    nouveau_dossier = chemin_dossier + '_pdf'
    os.makedirs(nouveau_dossier, exist_ok=True)

    # Itérer sur les fichiers du dossier d'origine
    for fichier in os.listdir(chemin_dossier):
        chemin_fichier = os.path.join(chemin_dossier, fichier)

        # Vérifier si le fichier est de type DOCX
        if fichier.lower().endswith('.docx'):

            # Convertir le fichier DOCX en PDF
            try:
                doc = aw.Document(chemin_fichier)
                doc.save(os.path.join(nouveau_dossier, f"{os.path.splitext(fichier)[0]}.pdf"))
                print(f"Conversion réussie : {fichier}")
            except Exception as e:
                print(f"Erreur lors de la conversion de {fichier} : {e}")




def dossier_docx_summary(chemin_dossier, llm, max_summ_size = 2000, sampling_params=sampling_params, hf = False, suffixe = ''):
    # Vérifier si le dossier existe

    chemin_fichier_markdown = "input/sample.md"
    if not os.path.exists(chemin_dossier):
        print(f"Le dossier {chemin_dossier} n'existe pas.")
        return
    
    # Créer un nouveau dossier avec _pdf à la fin du nom
    nouveau_dossier = chemin_dossier + '_summary'+suffixe
    os.makedirs(nouveau_dossier, exist_ok=True)

    # Itérer sur les fichiers du dossier d'origine
    for fichier in os.listdir(chemin_dossier):
        chemin_fichier = os.path.join(chemin_dossier, fichier)

        # Vérifier si le fichier est de type DOCX
        if fichier.lower().endswith('.docx'):

            # Convertir le fichier DOCX en md
            try:

                with open(chemin_fichier, "rb") as docx_file:
                    result = mammoth.convert_to_markdown(docx_file)
                with open(chemin_fichier_markdown, "w") as markdown_file:
                    markdown_file.write(result.value)

                df = extraire_titres_sous_titres_texte_entier(chemin_fichier_markdown)
                max_level = df['niveau'].max()

                for i in range(max_level,1,-1):
                    df = one_level_summary(df, i, llm, sampling_params, hf=hf)
                    df = iterative_summary_reduction(df, i, llm, sampling_params, summ_size = max_summ_size)
                    df = concate_chapter(df)

                df = one_level_summary(df, 1, llm, sampling_params,hf=hf) 
                df = iterative_summary_reduction(df, 1, llm, sampling_params, summ_size = max_summ_size)

                save_docx(df, os.path.join(nouveau_dossier, f"{os.path.splitext(fichier)[0]}.docx"))
                print(f"Résumé réussie : {fichier}")
            except Exception as e:
                print(f"Erreur lors du résumé de {fichier} : {e}")

    return


def dossier_docx_summary_RAG(chemin_dossier, llm, max_summ_size = 2000, sampling_params=sampling_params, hf = False, suffixe = ''):
    # Vérifier si le dossier existe

    chemin_fichier_markdown = "input/sample.md"
    if not os.path.exists(chemin_dossier):
        print(f"Le dossier {chemin_dossier} n'existe pas.")
        return
    
    # Créer un nouveau dossier avec _pdf à la fin du nom
    nouveau_dossier = chemin_dossier + '_summary'+suffixe
    os.makedirs(nouveau_dossier, exist_ok=True)

    RAG_df = pd.DataFrame()

    # Itérer sur les fichiers du dossier d'origine
    for fichier in os.listdir(chemin_dossier):
        chemin_fichier = os.path.join(chemin_dossier, fichier)

        # Vérifier si le fichier est de type DOCX
        if fichier.lower().endswith('.docx'):

            # Convertir le fichier DOCX en md
            try:

                with open(chemin_fichier, "rb") as docx_file:
                    result = mammoth.convert_to_markdown(docx_file)
                with open(chemin_fichier_markdown, "w") as markdown_file:
                    markdown_file.write(result.value)

                df = extraire_titres_sous_titres_texte_entier(chemin_fichier_markdown)
                max_level = df['niveau'].max()

                RAG_df = pd.concat([RAG_df, df])
                
                for i in range(max_level,1,-1):
                    df = one_level_summary(df, i, llm, sampling_params, hf=hf)
                    df = iterative_summary_reduction(df, i, llm, sampling_params, summ_size = max_summ_size)
                    df = concate_chapter(df)
                    RAG_df = pd.concat([RAG_df, df[df.loc['niveau']==i]])

                    
                df = one_level_summary(df, 1, llm, sampling_params,hf=hf) 
                df = iterative_summary_reduction(df, 1, llm, sampling_params, summ_size = max_summ_size)
                RAG_df = pd.concat([RAG_df, df[df.loc['niveau']==1]])
                save_docx(df, os.path.join(nouveau_dossier, f"{os.path.splitext(fichier)[0]}.docx"))
                print(f"Résumé réussie : {fichier}")
            except Exception as e:
                print(f"Erreur lors du résumé de {fichier} : {e}")

    return

def read_docx_files(directory_path):
    all_text = ""

    # Traverse the directory
    for filename in os.listdir(directory_path):
        if filename.endswith(".docx"):
            file_path = os.path.join(directory_path, filename)
            doc_text = read_docx(file_path)
            all_text += doc_text + "\n"

    return all_text

def read_docx(docx_path):
    doc = Document(docx_path)
    text = ""

    for paragraph in doc.paragraphs:
        text += paragraph.text + "\n"

    return text